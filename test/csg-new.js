import test from 'ava'
import {CSG, CAG} from '../csg'

//
// Test suite for CSG initialization (new)
// - verify that the CSG is "empty" in all ways
// - verify that CSG functions do / return nothing
// - verify that the CSG converts to/from properly
//
test('New CSG should contain nothing', t => {
  let csg = new CSG()

  // conversion functions
  t.is(csg.toString(), 'CSG: geom3 (0 basePolygons):\n')

  t.true(Array.isArray(csg.toPolygons()))
  t.is(csg.toPolygons().length, 0)

  let feature = csg.getFeatures('volume')
  t.is(feature, 0)
  let feature2 = csg.getFeatures('area')
  t.is(feature2, 0)

  let bounds = csg.getBounds()
  t.true(Array.isArray(bounds))
  t.is(bounds.length, 2)
  t.is(typeof bounds[0], 'object')
  t.is(typeof bounds[1], 'object')

  let vector0 = bounds[0]
  t.is(vector0[0], 0)
  t.is(vector0[1], 0)
  t.is(vector0[2], 0)
  let vector1 = bounds[1]
  t.is(vector1[0], 0)
  t.is(vector1[1], 0)
  t.is(vector1[2], 0)

  let triangles = csg.toTriangles()
  t.is(triangles.length, 0)

  let binary = csg.toCompactBinary()
  t.is(binary.class, 'CSG')
  t.is(binary.numPolygons, 0)
  t.is(binary.numVerticesPerPolygon.length, 0)
  t.is(binary.polygonPlaneIndexes.length, 0)
  t.is(binary.polygonSharedIndexes.length, 0)
  t.is(binary.polygonVertices.length, 0)
})

test('New CSG should do nothing', t => {
  let csg = new CSG()

  // tests for basic transforms
  let shared = new CSG.Shared([0.1, 0.2, 0.3, 0.4])
  let acsg = csg.setShared(shared)
  let polygons = acsg.toPolygons() // trigger canonicalize
  t.deepEqual(csg, acsg)

  acsg = csg.setColor(0.1, 0.2, 0.3, 0.4)
  polygons = acsg.toPolygons() // trigger canonicalize
  t.deepEqual(csg, acsg)

  let matrix = CSG.Matrix4x4.rotationX(45)
  acsg = csg.transform(matrix)

  // tests for common transforms
  let plane = new CSG.Plane(new CSG.Vector3D(0, 0, 1), 0)
  acsg = csg.mirrored(plane)
  acsg = csg.mirroredX()
  acsg = csg.mirroredY()
  acsg = csg.mirroredZ()

  acsg = csg.translate([10, 10, 10])

  acsg = csg.scale([2.0, 2.0, 2.0])

  acsg = csg.rotate([0, 0, 0], [1, 1, 1], 45)
  acsg = csg.rotateX()
  acsg = csg.rotateY()
  acsg = csg.rotateZ()
  acsg = csg.rotateEulerAngles(45, 45, 45, [0, 0, 0])

  acsg = csg.center([true, true, true])

  acsg = csg.cutByPlane(plane)

  acsg = csg.expand(2.0, 36)
  polygons = acsg.toPolygons() // trigger canonicalize
  t.deepEqual(polygons, [])

  acsg = csg.contract(2.0, 36)
  polygons = acsg.toPolygons() // trigger canonicalize
  t.deepEqual(polygons, []);

  acsg = csg.invert()
  polygons = acsg.toPolygons() // trigger canonicalize
  t.deepEqual(csg,acsg);

  acsg = csg.stretchAtPlane([1, 0, 0], [0, 0, 0], 2.0)
  polygons = acsg.toPolygons() // trigger canonicalize
  t.deepEqual(polygons, []);
})

test('New CSG should return empty values', t => {
  let csg = new CSG()

  let plane = new CSG.Plane(new CSG.Vector3D(0, 0, 1), 0)
  let onb = new CSG.OrthoNormalBasis(plane)

  let cag = new CAG()
  let ucag = cag.union(new CAG())

  let acag = csg.projectToOrthoNormalBasis(onb)
  // NOTE: CAG.union() is being called internally so compare accordingly
  t.deepEqual(acag, ucag)

  acag = csg.sectionCut(onb)
  // NOTE: CAG.union() is being called internally so compare accordingly
  t.deepEqual(acag, ucag)

  let acsg = CSG.toPointCloud(csg)
  let polygons = acsg.toPolygons() // trigger canonicalize
  t.deepEqual(polygons, [])
})

test('New CSG should convert properly', t => {
  let csg = new CSG()

  let acb = csg.toCompactBinary()
  let acsg = CSG.fromCompactBinary(acb)
  let polygons = acsg.toPolygons() // trigger canonicalize
  t.deepEqual(csg, acsg)

  // TODO use toObject() when available
  let aobj = {geometry: {basePolygons: []}}
  acsg = CSG.fromObject(aobj)
  polygons = acsg.toPolygons() // trigger canonicalize
  t.deepEqual(acsg, csg)

  let triangles = csg.toTriangles()
  t.is(triangles.length, 0)
  acsg = CSG.fromPolygons(triangles)
  polygons = acsg.toPolygons()
  t.deepEqual(polygons, triangles)
})
