const test = require('ava')

const Vertex = require('../src/math/Vertex3')
const Vector3D = require('../src/math/Vector3')
const Polygon = require('../src/math/Polygon3')

const {fromPolygons, fromObject} = require('../src/core/CSGFactories')

// failing due to compare 0 vs -0
test.failing('CSG can be created from polygons', t => {
  const points = [[0, 0, 0], [0, 10, 0], [0, 0, 10]]
  let polygon = Polygon.fromPoints(points)
  const obsCSG = fromPolygons([polygon])
  t.deepEqual(obsCSG.toPolygons(), [polygon])
})

test('CSG can be created from objects', t => {
  const input = {
    geometry : {
      basePolygons: [{
        vertices: [
          {pos: {x: 0, y: 0, z: 0}},
          {pos: {x: 0, y: 10, z: 0}},
          {pos: {x: 0, y: 10, z: 10}}
        ],
        shared: {
          color: [1, 0, 1, 1]
        },
        plane: {
          normal: [10, 1, 1],
          w: 1
        }
      }]
    }
  }
  const obsCSG = fromObject(input)
  const polygons = obsCSG.toPolygons()
  t.deepEqual(polygons.length, 1)
})
