import test from 'ava'
import {CSG, CAG} from '../csg'

//
// Test suite for CAG initialization (new)
// - verify that the CAG is "empty" in all ways
// - verify that CAG functions do / return nothing
// - verify that the CAG converts to/from properly
//
test('New CAG should contain nothing', t => {
  const cag = new CAG()

  // conversion functions
  t.is(cag.toString(), 'CAG: geom2 (0 baseSides):\n')
  t.is(cag.isSelfIntersecting(), false)

  var area = cag.area()
  t.is(area, 0)

  var bounds = cag.getBounds()
  t.true(Array.isArray(bounds))
  t.is(bounds.length, 2)
  t.is(bounds[0][0], 0)
  t.is(bounds[0][1], 0)
  t.is(bounds[1][0], 0)
  t.is(bounds[1][1], 0)
})

test('New CAG should do nothing', t => {
  var cag = new CAG()

  // test for basic transforms
  var cagB = CAG.rectangle()

  var cag1 = cag.union(cagB)
  t.deepEqual(cag1.toPoints(), cagB.toPoints())
  var cag2 = cag.subtract(cagB)
  // FIXME : t.deepEqual(cag2.toPoints(), cag.toPoints());
  var cag3 = cag.intersect(cagB)
  // FIXME : t.deepEqual(cag3.toPoints(), cag.toPoints());

  // tests for basic functionality
  var matrixB = CSG.Matrix4x4.translation([10, 10, 0])
  var cag4 = cag.transform(matrixB)
  t.deepEqual(cag4.toPoints(), cag.toPoints())

  var cag5 = cag.flipped()
  t.deepEqual(cag5.toPoints(), cag.toPoints())
})

test('New CAG should return empty values', t => {
  var cag = new CAG()

  // test externals
  var paths = cag.getOutlinePaths()
  t.true(Array.isArray(paths))
  t.is(paths.length, 0)

  var cag2 = cag.expand(4, 8)
  t.deepEqual(cag.toPoints(), cag2.toPoints())
  var cag3 = cag.contract(4, 8)
  t.deepEqual(cag.toPoints(), cag3.toPoints())
})

test('New CAG should convert properly', t => {
  var cag = new CAG()

  // to from binary
  var binary = cag.toCompactBinary()
  t.is(binary.class, 'CAG')
  t.is(binary.sideVertexIndices.length, 0)
  t.is(binary.vertexData.length, 0)

  // to 3D objects
  var csgNew = new CSG()
  var csg1 = cag.extrude() // use defaults
  t.deepEqual(csg1.toPolygons(), csgNew.toPolygons())
  var csg2 = cag.extrudeInPlane('X', 'Y', 1.0, {symmetrical: true})
  t.deepEqual(csg2.toPolygons(), csgNew.toPolygons())
  var ortho1 = CSG.OrthoNormalBasis.GetCartesian('-X', '-Y')
  var csg3 = cag.extrudeInOrthonormalBasis(ortho1, 5)
  t.deepEqual(csg3.toPolygons(), csgNew.toPolygons())

  // FIXME : var csg4 = cag.rotateExtrude()
  // FIXME : t.deepEqual(csg4,csgNew);
})
