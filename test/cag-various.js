const test = require('ava')
const {CAG, CSG} = require('../csg')

// helper function, gives a much more compact variant of the the side data
// hint : this could also be used to streamline the cag implementation in the future
function compareCagSide (cagSide) {
  return {pos: [
    [cagSide.vertex0.pos._x, cagSide.vertex0.pos._y],
    [cagSide.vertex1.pos._x, cagSide.vertex1.pos._y]
  ]}
}

// FIXME change this to NOT use expand() as that function is full of bugs
// FIXME this test needs additional tests for multiple paths, i.e. holes, donuts, etc.
test('CAG getOutlinePaths should work correctly', t => {
  const radius = 10
  const cag = CAG.fromPoints([
    [-radius, -radius, 0],
    [radius, -radius, 0],
    [radius, radius, 0]
  ])

  const paths = cag.getOutlinePaths()
  const result = CAG.fromPath2(paths[0])

  t.deepEqual(result.toPoints(), cag.toPoints())
})
