import test from 'ava'
import {CSG, CAG} from '../csg'

// returns positions and tags for simplicity
const flatPolygons = polygon => {
  return polygon.vertices.map(p => [p.pos._x, p.pos._y, p.pos.z])
}

// FAILING due to added tags on each point of each polygon
test.failing('CSG.fixTJunctions fixes ...tjunctions', t => {
  const csg = CSG.cube()
  const csgpolygons = csg.toPolygons()

  const fix = csg.fixTJunctions()
  const fixpolygons = fix.toPolygons()
  t.deepEqual(csgpolygons, fixpolygons)
})

// yikes, horrible name
// FIXME this is test is not reproducable
test.failing('CSG.fixTJunctions should work correctly even in corner cases', t => {
  const csgImage = CAG.fromPoints([
    [ 25.183085182520657, -16.31346279512401 ],
    [ 25.399997999999997, -14.111109999999998 ],
    [ 25.18308518252066, -11.908757204875993 ],
    [ 24.540682568012272, -9.791039592574926 ],
    [ 23.497477328686852, -7.839339863307808 ],
    [ 22.09355925685544, -6.128660743144558 ],
    [ 20.38288013669219, -4.724742671313145 ],
    [ 18.43118040742507, -3.6815374319877243 ],
    [ 16.313462795124003, -3.0391348174793364 ],
    [ 14.111109999999998, -2.822222 ],
    [ 11.908757204875993, -3.0391348174793364 ],
    [ 9.791039592574926, -3.6815374319877243 ],
    [ 7.839339863307811, -4.724742671313143 ],
    [ 6.128660743144558, -6.1286607431445566 ],
    [ 4.724742671313143, -7.839339863307808 ],
    [ 3.6815374319877243, -9.791039592574924 ],
    [ 3.0391348174793364, -11.90875720487599 ],
    [ 2.822222, -14.111109999999996 ],
    [ 3.0391348174793364, -16.313462795124003 ],
    [ 3.6815374319877243, -18.43118040742507 ],
    [ 4.724742671313141, -20.382880136692187 ],
    [ 6.128660743144556, -22.09355925685544 ],
    [ 7.839339863307808, -23.497477328686852 ],
    [ 9.791039592574919, -24.54068256801227 ],
    [ 11.908757204875988, -25.183085182520657 ],
    [ 14.111109999999996, -25.399997999999997 ],
    [ 16.313462795124003, -25.18308518252066 ],
    [ 18.431180407425074, -24.540682568012272 ],
    [ 20.382880136692183, -23.497477328686855 ],
    [ 22.093559256855436, -22.093559256855443 ],
    [ 23.497477328686852, -20.382880136692187 ],
    [ 24.54068256801227, -18.431180407425078 ] ])

  const block = CSG.cube({center: [5, 5, 5], radius: 20})
  const engraving = csgImage.extrude([0, 0, 5])
  const input = block.subtract(engraving)
  const afterFix = input.fixTJunctions()

  const afterPolygons = afterFix.toPolygons()
  const expectedPolygons = []
  t.deepEqual(afterPolygons, expectedPolygons)
})
