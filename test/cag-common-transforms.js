import test from 'ava'
import { CSG, CAG } from '../csg'
const {nearlyEqual} = require('./helpers/nearlyEqual')

// helper function, gives a much more compact variant of the the side data
// hint : this could also be used to streamline the cag implementation in the future
function compactCagSide (cagSide) {
  return {pos: [
    [cagSide.vertex0.pos._x, cagSide.vertex0.pos._y],
    [cagSide.vertex1.pos._x, cagSide.vertex1.pos._y]
  ]}
}

function compareCagSides (t, expected, observed, precision) {
  nearlyEqual(t, expected.pos[0][0], observed.pos[0][0], precision)
  nearlyEqual(t, expected.pos[0][1], observed.pos[0][1], precision)
}

function getSimplifiedVertexPosition (vertex) {
  return '_z' in vertex.pos ? [vertex.pos._x, vertex.pos._y, vertex.pos._z] : [vertex.pos._x, vertex.pos._y]
}

test('CAG should translate properly', t => {
  const shape = CAG.rectangle({radius: 3})
  const shape2 = shape.translate([2, 0, 0])
  const points = shape2.toPoints()
  t.deepEqual(points[0], [-1, -3])
})

test('CAG should union properly', t => {
  const op1 = CAG.circle({resolution: 5})
  const op2 = CAG.rectangle({center: [1, 1]})

  const result = op1.union(op2)
  const sides = result.toSides()
  const firstSide = sides[0]
  const lastSide = sides[sides.length - 1]

  t.deepEqual(sides.length, 8)
  t.deepEqual(compactCagSide(firstSide), {pos: [[0.30902, -0.95106], [1, 0]]})
  t.deepEqual(compactCagSide(lastSide), {pos: [[1, 0], [2, 0]]})
})

test('CAG should subtract properly', t => {
  const op1 = CAG.circle({resolution: 5})
  const op2 = CAG.rectangle({radius: 3})

  const result = op2.subtract(op1)
  const sides = result.toSides()
  const firstSide = sides[0]
  const lastSide = sides[sides.length - 1]

  t.deepEqual(sides.length, 9)
  t.deepEqual(compactCagSide(firstSide), {pos: [[-3, 3], [-3, -3]]})
  t.deepEqual(compactCagSide(lastSide), {pos: [[0.30902, -0.95106], [-0.80902, -0.58779]]})
})

test('CAG should intersect properly', t => {
  const op1 = CAG.circle({resolution: 5})
  const op2 = CAG.rectangle({radius: 3})

  const result = op2.intersect(op1)
  const sides = result.toSides()
  const firstSide = sides[0]
  const lastSide = sides[sides.length - 1]

  t.deepEqual(sides.length, 5)
  t.deepEqual(compactCagSide(firstSide), {pos: [[0.30902, -0.95106], [1, 0]]})
  t.deepEqual(compactCagSide(lastSide), {pos: [[-0.80902, -0.58779], [0.30902, -0.95106]]})
})

test('CAG should transform properly', t => {
  const cag1 = CAG.rectangle({radius: 3})
  const matrixA = CSG.Matrix4x4.translation([10, 10, 0])

  let result = cag1.transform(matrixA)
  const sides = result.toSides()
  t.deepEqual(compactCagSide(sides[0]), {pos: [[7, 13], [7, 7]]})
  t.deepEqual(compactCagSide(sides[1]), {pos: [[7, 7], [13, 7]]})
  t.deepEqual(compactCagSide(sides[2]), {pos: [[13, 7], [13, 13]]})
  t.deepEqual(compactCagSide(sides[3]), {pos: [[13, 13], [7, 13]]})
})

test('CAG should flip properly', t => {
  const cag1 = CAG.rectangle({center: [5,-5],radius: [3,2]})
  let result = cag1.flipped()
  const sides = result.toSides()
  t.deepEqual(compactCagSide(sides[0]), {pos: [[2, -3], [8, -3]]})
  t.deepEqual(compactCagSide(sides[1]), {pos: [[8, -3], [8, -7]]})
  t.deepEqual(compactCagSide(sides[2]), {pos: [[8, -7], [2, -7]]})
  t.deepEqual(compactCagSide(sides[3]), {pos: [[2, -7], [2, -3]]})
})

test('CAG should expand properly', t => {
  const cag1 = CAG.rectangle({center: [5,-5],radius: [5,3]})

  const result = cag1.expand(1, 4)
  const sides = result.toSides()

  const firstSide = sides[0]
  const lastSide = sides[sides.length - 1]

  t.deepEqual(sides.length, 8)
  t.deepEqual(compactCagSide(firstSide), {pos: [[-1, -2], [-1, -8]]})
  t.deepEqual(compactCagSide(lastSide), {pos: [[11, -2], [10, -1]]})
})

test('CAG should contract properly', t => {
  const cag1 = CAG.rectangle({center: [5,-5],radius: [5,3]})

  const result = cag1.contract(1, 8)
  const sides = result.toSides()

  const firstSide = sides[0]
  const lastSide = sides[sides.length - 1]

  t.deepEqual(sides.length, 4)
  t.deepEqual(compactCagSide(firstSide), {pos: [[1, -3], [1, -7]]})
  t.deepEqual(compactCagSide(lastSide), {pos: [[9, -3], [1, -3]]})
})
