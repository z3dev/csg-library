const Side = require('../math/Side')

//const {contains} = require('./utils/cagValidation')

const {union, difference} = require('../api/ops-booleans')

const geom2 = require('../geom2')

const fromBinaryDataBuffers = require('../ops-geom2/fromBinaryDataBuffers')
const fromFakePolygons = require('../ops-geom2/fromFakePolygons')

/** Construct a CAG from a list of `Side` instances.
 * @param {Side[]} sides - list of sides
 * @returns {CAG} new CAG object
 */
const fromSides = function (sides) {
  const geometry = geom2.create(sides)
  const CAG = require('../api/CAG') // circular dependency
  return new CAG(geometry)
}

// Converts a CSG to a  The CSG must consist of polygons with only z coordinates +1 and -1
// as constructed by _toCSGWall(-1, 1). This is so we can use the 3D union(), intersect() etc
const fromFakeCSG = function (csg) {
  const geometry = fromFakePolygons(csg.toPolygons())
  const CAG = require('../api/CAG') // circular dependency
  return new CAG(geometry)
}

/** Construct a CAG from a list of points (a polygon) or an nested array of points.
 * The rotation direction of the points is not relevant.
 * The points can define a convex or a concave polygon.
 * The polygon must not self intersect.
 * Hole detection follows the even/odd rule,
 * which means that the order of the paths is not important.
 * @param {points[]|Array.<points[]>} points - (nested) list of points in 2D space
 * @returns {CAG} new CAG object
 */
const fromPoints = function (points) {
  if (!points) {
    throw new Error('points parameter must be defined')
  }
  if (!Array.isArray(points)) {
    throw new Error('points parameter must be an array')
  }
  // BUG FIXME
  if ((points[0].x !== undefined) || (typeof points[0][0] === 'number')) {
    return fromPointArray(points)
  }
  //if (typeof points[0][0] === 'object') {
  //  return fromNestedPointsArray(points)
  //}
  throw new Error('Unsupported points list format')
}

const fromPointArray = function (points) {
  const geometry = geom2.fromPoints(points)
  const CAG = require('../api/CAG') // circular dependency
  return new CAG(geometry)
}

const fromNestedPointsArray = function (points) {
  if (points.length === 1) {
    return fromPoints(points[0])
  }
  // First pass: create a collection of CAG paths
  let paths = points.map(path => {
    fromPointArray(path)
  })
  // Second pass: make a tree of paths
  let tree = {}
  // for each polygon extract parents and childs polygons
  paths.forEach((p1, i) => {
    // check for intersection
    paths.forEach((p2, y) => {
      if (p1 !== p2) {
        // create default node
        tree[i] || (tree[i] = { parents: [], isHole: false })
        tree[y] || (tree[y] = { parents: [], isHole: false })
        // check if polygon2 stay in poylgon1
        if (contains(p2, p1)) {
          // push parent and child; odd parents number ==> hole
          tree[i].parents.push(y)
          tree[i].isHole = !! (tree[i].parents.length % 2)
          tree[y].isHole = !! (tree[y].parents.length % 2)
        }
      }
    })
  })
  // Third pass: subtract holes
  let path = null
  for (let key in tree) {
    path = tree[key]
    if (path.isHole) {
      delete tree[key] // remove holes for final pass
      path.parents.forEach(parentKey => {
        paths[parentKey] = difference(paths[parentKey], paths[key])
      })
    }
  }
  // Fourth and last pass: create final CAG object
  let cag = fromSides([])
  for (let key in tree) {
    cag = union(cag, paths[key])
  }
  return cag
}

/** Reconstruct a CAG from an object with identical property names.
 * @param {Object} obj - anonymous object, typically from JSON
 * @returns {CAG} new CAG object
 */
const fromObject = function (obj) {
  const geometry = geom2.fromObject(obj.geometry)
  const CAG = require('../api/CAG')
  return new CAG(geometry)
}

/** Construct a CAG from a list of points (a polygon).
 * Like fromPoints() but does not check if the result is a valid polygon.
 * The points MUST rotate counter clockwise.
 * The points can define a convex or a concave polygon.
 * The polygon must not self intersect.
 * @param {points[]} points - list of points in 2D space
 * @returns {CAG} new CAG object
 */
const fromPointsNoCheck = function (points) {
  const geometry = geom2.fromPoints(points)
  const CAG = require('../api/CAG') // circular dependency
  return new CAG(geometry)
}

/** Construct a CAG from a closed 2D path.
 * @param {Path2} path - a closed path
 * @returns {CAG} new CAG object
 */
const fromPath2 = function (path) {
  if (!path.isClosed()) throw new Error('The path should be closed!')
  let points = path.toPoints()
  return fromPoints(points)
}

/** Reconstruct a CAG from the output of toCompactBinary().
 * @param {CompactBinary} bin - see toCompactBinary()
 * @returns {CAG} new CAG object
 */
const fromCompactBinary = function (bin) {
  const geometry = fromBinaryDataBuffers({'class': 'CAG'}, bin)
  const CAG = require('../api/CAG') // circular dependency
  return new CAG(geometry)
}

module.exports = {
  fromSides,
  fromObject,
  fromPointArray,
  fromPoints,
  fromPointsNoCheck,
  fromPath2,
  fromFakeCSG,
  fromCompactBinary
}
