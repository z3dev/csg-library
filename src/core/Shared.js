const {getTag} = require('../constants')

/** Class Shared
 * Holds the shared properties for each polygon (Currently only color).
 * @constructor
 * @param {Array[]} color - array containing RGBA values, or null
 *
 * @example
 *   let shared = new Shared([0, 0, 0, 1])
 */
Shared = function (color) {
  if (color) {
    if (Array.isArray(color) && color.length === 4) {
      this.color = color
    } else {
      throw new Error('expecting color, 4 element array')
    }
  }
}

Shared.prototype = {
  getTag: function () {
    let result = this.tag
    if (!result) {
      result = getTag()
      this.tag = result
    }
    return result
  },

  // get a string uniquely identifying this object
  getHash: function () {
    if (!this.color) return 'null'
    return this.color.join('/')
  }
}

Shared.fromObject = function (obj) {
  return new Shared(obj.color)
}

/** Create Shared from color values.
 * @param {number} r - value of RED component
 * @param {number} g - value of GREEN component
 * @param {number} b - value of BLUE component
 * @param {number} [a] - value of ALPHA component
 * @param {Array[]} [color] - OR array containing RGB values (optional Alpha)
 *
 * @example
 * let s1 = Shared.fromColor(0,0,0)
 * let s2 = Shared.fromColor([0,0,0,1])
 */
Shared.fromColor = function (args) {
  let color
  if (arguments.length === 1) {
    color = arguments[0].slice() // make deep copy
  } else {
    color = []
    for (let i = 0; i < arguments.length; i++) {
      color.push(arguments[i])
    }
  }
  if (color.length === 3) {
    color.push(1)
  } else if (color.length !== 4) {
    throw new Error('setColor expects either an array with 3 or 4 elements, or 3 or 4 parameters.')
  }
  return new Shared(color)
}

Shared.defaultShared = new Shared()

module.exports = Shared
