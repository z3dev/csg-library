function isCSG (object) {
  // objects[i] instanceof CSG => NOT RELIABLE
  // 'instanceof' causes huge issues when using objects from
  // two different versions of CSG.js as they are not reckonized as one and the same
  // so DO NOT use instanceof to detect matching types for CSG/CAG
  if (!('geometry' in object)) {
    return false
  }
  if (!('basePolygons' in object.geometry)) {
    return false
  }
  return true
}

module.exports = isCSG
