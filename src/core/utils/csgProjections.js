const {EPS, areaEPS} = require('../../constants')

const CAG = require('../../api/CAG')

const {fromPointsNoCheck} = require('../CAGFactories')

// project the 3D CSG onto a plane
// This returns a 2D CAG with the 'shadow' shape of the 3D solid when projected onto the
// plane represented by the orthonormal basis
const projectToOrthoNormalBasis = function (csg, orthobasis) {
  let cags = []
  const polygons = csg.toPolygons()
  polygons.filter(function (p) {
    // only return polys in plane, others may disturb result
    // FIXME comparing NORMALS is NOT the same as comparing points
    return p.plane.normal.minus(orthobasis.plane.normal).lengthSquared() < (EPS * EPS)
  }).map(function (polygon) {
    // project the polygon to the basis
    let points2d = polygon.vertices.map(function (vertex) {
      return orthobasis.to2D(vertex.pos)
    })
    let cag = fromPointsNoCheck(points2d)
    if (cag.area() > areaEPS) cags.push(cag)
  })
  let result = new CAG().union(cags)
  return result
}

module.exports = {projectToOrthoNormalBasis}
