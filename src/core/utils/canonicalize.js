const FuzzyCSGFactory = require('../FuzzyFactory3d')
const {fromPolygons} = require('../CSGFactories')

const isCSG = require('./isCSG')

/**
   * Returns a cannoicalized version of the input csg/cag : ie every very close
   * points get deduplicated
   * @returns {CSG|CAG}
   * @example
   * let rawInput = someCSGORCAGMakingFunction()
   * let canonicalized= canonicalize(rawInput)
   */
const canonicalize = function (csgOrCAG, options) {
  if (isCSG(csgOrCAG)) {
    return canonicalizeCSG(csgOrCAG, options)
  }
  return csgOrCAG
}

/**
   * Returns a cannoicalized version of the input csg : ie every very close
   * points get deduplicated
   * @returns {CSG}
   * @example
   * let rawCSG = someCSGMakingFunction()
   * let canonicalizedCSG = canonicalize(rawCSG)
   */
const canonicalizeCSG = function (csg) {
  if (csg.geometry.isCanonicalized) {
    return csg
  } else {
    const factory = new FuzzyCSGFactory()
    let result = CSGFromCSGFuzzyFactory(factory, csg)
    result.isRetesselated = csg.isRetesselated
    result.properties = csg.properties // keep original properties
    return result
  }
}

const CSGFromCSGFuzzyFactory = function (factory, sourcecsg) {
  const polygons = sourcecsg.toPolygons()
  let newpolygons = []
  polygons.forEach(function (polygon) {
    let newpolygon = factory.getPolygon(polygon)
    // see getPolygon above: we may get a polygon with no vertices, discard it:
    if (newpolygon.vertices.length >= 3) {
      newpolygons.push(newpolygon)
    }
  })
  return fromPolygons(newpolygons)
}

module.exports = canonicalize
