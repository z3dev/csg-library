const canonicalize = require('./canonicalize')

/**
 * Calls a function for each point in the given path.
 * @param {function} thunk - the function to call
 * @param {path} path - the path to operate upon
 * @example
 * eachPoint(path, accumulate)
 */
const eachPoint = (options, thunk, path) => {
  canonicalize(path).points.forEach(thunk)
}

module.exports = eachPoint
