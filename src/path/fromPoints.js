const Vector3 = require('../math/Vector3')

const close = require('./close')
const create = require('./create')

/**
 * Create a new path from the given points.
 * The points must be provided an array of points,
 * where each point is an array of two numbers.
 * @param {Array} points - array of points from which to create the path
 * @param {boolean} [options.closed] - if the path should be open or closed
 * @returns {path} new path
 * @example:
 * my newpath = fromPoints({closed: true}, [[10, 10], [-10, 10]])
 */
const fromPoints = (options, points) => {
  const defaults = {closed: false}
  let {closed} = Object.assign({}, defaults, options)

  let created = create()
  created.basePoints = points.map((point) => Vector3.fromVarious(point))

  // check if first and last points are equal
  if (created.basePoints.length > 1) {
    if (created.basePoints[0].equals(created.basePoints[created.basePoints.length - 1])) {
    // and close automatically
      closed = true
    }
  }
  if (closed === true) created = close(created)

  return created
}

module.exports = fromPoints
