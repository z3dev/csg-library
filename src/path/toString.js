const canonicalize = require('./canonicalize')

/**
 * Create a string representing the contents of the given path.
 * @returns {String} a representive string
 * @example
 * console.out(toString(path))
 */
const toString = (geometry) => {
  if (geometry.isCanonicalized) {
    let result = 'path (' + geometry.points.length + ' points, '+geometry.isClosed+'):\n'
    geometry.points.forEach(function (point) {
      result += '  ' + point + '\n'
    })
    return result
  }

  let result = 'path (' + geometry.basePoints.length + ' basePoints, '+geometry.isClosed+'):\n'
  geometry.basePoints.forEach(function (point) {
    result += '  ' + point + '\n'
  })
  return result
}

module.exports = toString
