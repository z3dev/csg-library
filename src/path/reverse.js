const clone = require('./clone')

/**
 * Reverses the given path so that the points are in the opposite order.
 * This swaps the left (interior) and right (exterior) edges.
 * @param {path} path - the path to reverse
 * @returns {path} the new reversed path
 * @example
 * let newpath = reverse(path)
 */
const reverse = (path) => {
  const cloned = clone(path)
  // The points don't move, so we can keep the transforms.
  // Just reverse the base points, and we're good to go.
  cloned.basePoints = path.basePoints.slice().reverse()
  cloned.points = []
  cloned.isCanonicalized = false
  return cloned
}

module.exports = reverse
