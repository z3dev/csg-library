const test = require('ava')

const Vector3 = require('../math/Vector3')

const eachPoint = require('./eachPoint')
const fromPoints = require('./fromPoints')

test('eachPoint: Each point is emitted', t => {
  const collector = []
  eachPoint({},
            point => collector.push(point),
            fromPoints({}, [[1, 1, 0], [2, 2, 0]]))
  t.deepEqual(collector, [Vector3.fromVarious(1, 1, 0), Vector3.fromVarious(2, 2, 0)])
})
