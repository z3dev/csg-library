module.exports = {
  canonicalize: require('./canonicalize'),
  close: require('./close'),
  concat: require('./concat'),
  create: require('./create'),
  direction: require('./direction'),
  eachPoint: require('./eachPoint'),
  equals: require('./equals'),
  fromPoints: require('./fromPoints'),
  isA: require('./isA'),
  reverse: require('./reverse'),
  toPoints: require('./toPoints'),
  toString: require('./toString'),
  transform: require('./transform')
}
