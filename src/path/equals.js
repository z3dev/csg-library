const Vector3 = require('../math/Vector3')

const canonicalize = require('./canonicalize')

/**
  * Determine if the given paths are equal.
  * For closed paths this includes equality under point order rotation.
  * @param {path} a - the first path to compare
  * @param {path} b - the second path to compare
  * @returns {boolean}
  */
const equals = (a, b) => {
  if (a.isClosed !== b.isClosed) {
    return false
  }
  if (a.basePoints.length !== b.basePoints.length) {
    return false
  }
  a = canonicalize(a)
  b = canonicalize(b)
  let length = a.points.length
  let offset = 0
  do {
    let unequal = false
    for (let i = 0; i < length; i++) {
      let pointa = a.points[i]
      let pointb = b.points[(i + offset) % length]
      if (!pointa.equals(pointb)) {
        unequal = true
        break
      }
    }
    if (unequal === false) {
      return true
    }
    if (!a.isClosed) {
      return false
    }
    // Circular paths might be equal under graph rotation.
    // Try effectively rotating b one step.
  } while (++offset < length)
  return false
}

module.exports = equals
