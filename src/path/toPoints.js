const canonicalize = require('./canonicalize')

/**
 * Produces an array of points from the path.
 * The returned array should not be modified as the points are shared with the path.
 * @param {path} path - the path
 * @returns {Array} an array of points, each point contains an array of two numbers
 * @example
 * let sharedpoints = toPoints(path)
 */
const toPoints = (path) => {
  return canonicalize(path).points
}

module.exports = toPoints
