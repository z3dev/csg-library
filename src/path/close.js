const {EPS} = require('../constants')

const clone = require('./clone')

/**
 * Close the given path.
 * @params {path} path - the path to close
 * @returns {path} the closed path
 */
const close = (path) => {
  if (path.isClosed) return path

  const cloned = clone(path)
  cloned.points = undefined
  cloned.isCanonicalized = false
  cloned.isClosed = true
 
  if (cloned.basePoints.length > 1) {
    // make sure the paths are formed properly
    let basePoints = cloned.basePoints
    let p0 = basePoints[0]
    let pn = basePoints[basePoints.length - 1]
    while (p0.distanceToSquared(pn) < (EPS*EPS)) {
      basePoints.pop()
      if (basePoints.length === 1) break
      pn = basePoints[basePoints.length - 1]
    }
  }
  return cloned
}

module.exports = close
