/**
 * Determine the overall direction of rotation of the given path.
 * See: http://mathworld.wolfram.com/PolygonArea.html
 * @params {path} path - the path to examine
 * @returns {String} direction of rotation, 'cw', 'ccw' or 'unknown'
 */
const direction = (path) => {
  const points = path.basePoints
  let twice_area = 0
  let last = points.length - 1
  for (let current = 0; current < points.length; last = current++) {
    twice_area += points[last].x * points[current].y - points[last].y * points[current].x
  }
  if (twice_area > 0) {
    return 'cw'
  } else if (twice_area < 0) {
    return 'ccw'
  }
  return 'unknown'
}

module.exports = direction
