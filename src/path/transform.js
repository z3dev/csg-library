const Matrix4x4 = require('../math/Matrix4x4')

const clone = require('./clone')

/**
 * Transform the given path using the give matrix.
 * This is a lazy transform of the points, as this function only adjusts the transforms.
 * See canonicalize() for the actual application of the transfroms to the points.
 * @param {Matrix4x4} matrix - the transform matrix
 * @param {path} path - the path to transform
 * @returns {path} new path
 * @example
 * let newpath = transform(fromZRotation(degToRad(90)), path)
 */
const transform = (matrix, path) => {
  let cloned = clone(path)
  cloned.transforms = cloned.transforms.multiply(matrix)
  cloned.points = []
  cloned.isCanonicalized = false
  return cloned
}

module.exports = transform
