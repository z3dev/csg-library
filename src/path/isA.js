/**
 * Determin if the given object is a path geometry.
 * @params {path} object - the object to interogate
 * @returns {true} if the object matches a path geometry
 */
const isA = (object) => {
  if ('basePoints' in object && Array.isArray(object.basePoints)) {
    if ('isClosed' in object) {
      return true
    }
  }
  return false
}

module.exports = isA
