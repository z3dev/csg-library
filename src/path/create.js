const Matrix4x4 = require('../math/Matrix4x4')

/**
 * Produces an empty, open path.
 * @returns {path} a new empty, open path
 * @example
 * let newpath = create()
 */
const create = () => {
  return {
    basePoints: [],  // Contains canonical, untransformed points.
    points: [],  // Contains canonical, transformed points.
    isClosed: false,
    isCanonicalized: false,
    transforms: new Matrix4x4()
  }
}

module.exports = create
