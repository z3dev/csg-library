const test = require('ava')

const Vector3 = require('../math/Vector3')

const fromPoints = require('./fromPoints')
const toPoints = require('./toPoints')

test('fromPoints: Creating a path from no points produces an empty non-canonical path', t => {
  const created = fromPoints({}, [])
  t.false(created.isCanonicalized)
  t.deepEqual(toPoints(created), [])
})

test('fromPoints: Creating a path from one point produces a non-canonical path with that element', t => {
  const created = fromPoints({}, [[1, 1, 0]])
  t.false(created.isCanonicalized)
  t.deepEqual(toPoints(created), [Vector3.fromVarious(1, 1, 0)])
})

test('fromPoints: Creating a closed path from one point produces a closed non-canonical path with that element', t => {
  const created = fromPoints({ closed: true }, [[1, 1, 0]])
  t.true(created.isClosed)
  t.false(created.isCanonicalized)
  t.deepEqual(toPoints(created), [Vector3.fromVarious(1, 1, 0)])
})
