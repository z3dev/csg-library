const test = require('ava')

const Vector3 = require('../math/Vector3')
const Matrix4x4 = require('../math/Matrix4x4')

const toPoints = require('./toPoints')
const fromPoints = require('./fromPoints')
const transform = require('./transform')

const line = fromPoints({}, [[0, 0, 0], [1, 0, 0]])

test('transform: Transform of a path produces a matching array', t => {
  t.deepEqual(toPoints(transform(Matrix4x4.rotationZ(90), line)),
              [Vector3.fromVarious(0, 0, 0), Vector3.fromVarious(0, 1, 0)])
})
