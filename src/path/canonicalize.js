/**
 * Produces a canonicalized path by canonicalizing the base points.
 * Must be called before exposing any point data.
 * @param {path} path - the path to canonicalize
 * @returns {path} new path
 * @example
 * let newpath = canonicalize(path)
 */
const canonicalize = (path) => {
  if (path.isCanonicalized) {
    return path
  }
  // canonicalize in-place.
  path.points = path.basePoints.map((point) => {
    return point.transform(path.transforms).canonicalize()
  })
  path.isCanonicalized = true
  return path
}

module.exports = canonicalize
