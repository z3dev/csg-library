const {quantizeForSpace} = require('./utils')

const IsFloat = require('../utils/IsFloat')

/** Class Vector2
 * Represents a 2D vector with X, Y coordinates
 * @constructor
 *
 * @example
 * new CSG.Vector2(1, 2);
 */
const Vector2 = function (x, y) {
  this._x = x
  this._y = y
}

Vector2.prototype = {
  get x () {
    return this._x
  },
  get y () {
    return this._y
  },

  set x (v) {
    throw new Error('Vector2 is immutable')
  },
  set y (v) {
    throw new Error('Vector2 is immutable')
  },

  canonicalize: function () {
    return new Vector2(quantizeForSpace(this._x), quantizeForSpace(this._y))
  },

  // extend to a 3D vector by adding a z coordinate:
  toVector3D: function (z) {
    const Vector3D = require('./Vector3') // FIXME: circular dependencies Vector2 => Vector3 => Vector2
    return new Vector3D(this._x, this._y, z)
  },

  equals: function (a) {
    return (this._x === a._x) && (this._y === a._y)
  },

  clone: function () {
    return new Vector2(this._x, this._y)
  },

  negated: function () {
    return new Vector2(-this._x, -this._y)
  },

  plus: function (a) {
    return new Vector2(this._x + a._x, this._y + a._y)
  },

  minus: function (a) {
    return new Vector2(this._x - a._x, this._y - a._y)
  },

  times: function (a) {
    return new Vector2(this._x * a, this._y * a)
  },

  dividedBy: function (a) {
    return new Vector2(this._x / a, this._y / a)
  },

  dot: function (a) {
    return this._x * a._x + this._y * a._y
  },

  lerp: function (a, t) {
    return this.plus(a.minus(this).times(t))
  },

  length: function () {
    return Math.sqrt(this.dot(this))
  },

  distanceTo: function (a) {
    return this.minus(a).length()
  },

  distanceToSquared: function (a) {
    return this.minus(a).lengthSquared()
  },

  lengthSquared: function () {
    return this.dot(this)
  },

  unit: function () {
    return this.dividedBy(this.length())
  },

  cross: function (a) {
    return this._x * a._y - this._y * a._x
  },

  // returns the vector rotated by 90 degrees clockwise
  normal: function () {
    return new Vector2(this._y, -this._x)
  },

  // Right multiply by a 4x4 matrix (the vector is interpreted as a row vector)
  // Returns a new Vector2
  multiply4x4: function (matrix4x4) {
    return matrix4x4.leftMultiply1x2Vector(this)
  },

  transform: function (matrix4x4) {
    return matrix4x4.leftMultiply1x2Vector(this)
  },

  angle: function () {
    return this.angleRadians()
  },

  angleDegrees: function () {
    var radians = this.angleRadians()
    return 180 * radians / Math.PI
  },

  angleRadians: function () {
    // y=sin, x=cos
    return Math.atan2(this._y, this._x)
  },

  min: function (p) {
    return new Vector2(Math.min(this._x, p._x), Math.min(this._y, p._y))
  },

  max: function (p) {
    return new Vector2(Math.max(this._x, p._x), Math.max(this._y, p._y))
  },

  toString: function () {
    return '[' + this._x.toFixed(5) + ', ' + this._y.toFixed(5) + ']'
  },

  abs: function () {
    return new Vector2(Math.abs(this._x), Math.abs(this._y))
  }
}

Vector2.fromAngle = function (radians) {
  return Vector2.fromAngleRadians(radians)
}

Vector2.fromAngleDegrees = function (degrees) {
  var radians = Math.PI * degrees / 180
  return Vector2.fromAngleRadians(radians)
}

Vector2.fromAngleRadians = function (radians) {
  return new Vector2(Math.cos(radians), Math.sin(radians))
}

Vector2.fromVarious = function (x, y) {
  if (arguments.length === 2) {
    y = parseFloat(y)
    x = parseFloat(x)
  } else {
    var ok = true
    if (arguments.length === 1) {
      if (typeof (x) === 'object') {
        if (x instanceof Vector2) {
          y = x._y
          x = x._x
        } else if (Array.isArray(x)) {
          y = parseFloat(x[1])
          x = parseFloat(x[0])
        } else if (('x' in x) && ('y' in x)) {
          y = parseFloat(x.y)
          x = parseFloat(x.x)
        } else ok = false
      } else {
        x = parseFloat(x)
        y = x
      }
    } else ok = false
    if (ok) {
      if ((!IsFloat(x)) || (!IsFloat(y))) ok = false
    }
    if (!ok) {
      throw new Error('wrong arguments')
    }
  }
  return new Vector2(x, y)
}

module.exports = Vector2
