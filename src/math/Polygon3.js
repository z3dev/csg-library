const {EPS} = require('../constants')

const Shared = require('../core/Shared')

const Matrix4x4 = require('./Matrix4x4')
const Plane = require('./Plane')
const Vector3D = require('./Vector3')
const Vertex3 = require('./Vertex3')

/** Class Polygon3
 * Represents a convex polygon. The vertices used to initialize a polygon must
 *   be coplanar and form a convex loop. They do not have to be `Vertex`
 *   instances but they must behave similarly (duck typing can be used for
 *   customization).
 * <br>
 * Each convex polygon has a `shared` property, which is shared between all
 *   polygons that are clones of each other or were split from the same polygon.
 *   This can be used to define per-polygon properties (such as surface color).
 * <br>
 * The plane of the polygon is calculated from the vertex coordinates if not provided.
 *   The plane can alternatively be passed as the third argument to avoid calculations.
 *
 * @constructor
 * @param {Vertex[]} vertices - list of vertices
 * @param {Shared} [shared=Shared.defaultShared] - shared property to apply
 * @param {Plane} [plane] - plane of the polygon
 *
 * @example
 * const vertices = [
 *   new CSG.Vertex(new CSG.Vector3D([0, 0, 0])),
 *   new CSG.Vertex(new CSG.Vector3D([0, 10, 0])),
 *   new CSG.Vertex(new CSG.Vector3D([0, 10, 10]))
 * ]
 * let observed = new Polygon3(vertices)
 */
let Polygon3 = function (vertices, shared, plane) {
  if (arguments.length !== 3) throw new Error('wrong arguments') // TBD remove later

  this.vertices = vertices
  this.shared = shared
  this.plane = plane
}

Polygon3.prototype = {
  /** Check whether the polygon is convex. (it should be, otherwise we will get unexpected results)
   * @returns {boolean}
   */
  checkIfConvex: function () {
    return Polygon3.verticesConvex(this.vertices, this.plane.normal)
  },

  // FIXME what? why does this return this, and not a new polygon?
  // FIXME is this used?
  setColor: function (args) {
    this.shared = Shared.fromColor(args)
    return this
  },

  getSignedVolume: function () {
    let signedVolume = 0
    for (let i = 0; i < this.vertices.length - 2; i++) {
      signedVolume += this.vertices[0].pos.dot(this.vertices[i + 1].pos.cross(this.vertices[i + 2].pos))
    }
    signedVolume /= 6
    return signedVolume
  },

  getArea: function () {
    let polygonArea = 0
    for (let i = 0; i < this.vertices.length - 2; i++) {
      polygonArea += this.vertices[i + 1].pos.minus(this.vertices[0].pos).cross(this.vertices[i + 2].pos.minus(this.vertices[i + 1].pos)).length()
    }
    polygonArea /= 2
    return polygonArea
  },

  translate: function (offset) {
    return this.transform(Matrix4x4.translation(offset))
  },

  // returns an array with a Vector3D (center point) and a radius
  boundingSphere: function () {
    if (!this.cachedBoundingSphere) {
      let box = this.boundingBox()
      let middle = box[0].plus(box[1]).times(0.5)
      let radius3 = box[1].minus(middle)
      let radius = radius3.length()
      this.cachedBoundingSphere = [middle, radius]
    }
    return this.cachedBoundingSphere
  },

  // returns an array of two Vector3Ds (minimum coordinates and maximum coordinates)
  boundingBox: function () {
    if (!this.cachedBoundingBox) {
      let minpoint
      if (this.vertices.length === 0) {
        minpoint = new Vector3D(0, 0, 0)
      } else {
        minpoint = this.vertices[0].pos
      }
      let maxpoint = minpoint
      this.vertices.forEach(function (vertice) {
        minpoint = minpoint.min(vertice.pos)
        maxpoint = maxpoint.max(vertice.pos)
      })
      this.cachedBoundingBox = [minpoint, maxpoint]
    }
    return this.cachedBoundingBox
  },

  flipped: function () {
    let newvertices = this.vertices.map(function (v) {
      return v.flipped()
    })
    newvertices.reverse()
    let newplane = this.plane.flipped()
    return new Polygon3(newvertices, this.shared, newplane)
  },

  // Affine transformation of polygon. Returns a new Polygon3
  transform: function (matrix4x4) {
    let newvertices = this.vertices.map(function (v) {
      return v.transform(matrix4x4)
    })
    let newplane = this.plane.transform(matrix4x4)
    if (matrix4x4.isMirroring()) {
      // need to reverse the vertex order
      // in order to preserve the inside/outside orientation:
      newvertices.reverse()
    }
    return new Polygon3(newvertices, this.shared, newplane)
  },

  toString: function () {
    let result = 'Polygon3 plane: ' + this.plane.toString() + '\n'
    this.vertices.forEach(function (vertex) {
      result += '  ' + vertex.toString() + '\n'
    })
    return result
  }
}

// create from an untyped object with identical property names:
Polygon3.fromObject = function (obj) {
  let vertices = obj.vertices.map(function (v) {
    return Vertex3.fromObject(v)
  })
  let shared = Shared.fromObject(obj.shared)
  let plane = Plane.fromObject(obj.plane)
  return new Polygon3(vertices, shared, plane)
}

/** Create a polygon from the given points.
 *
 * @param {Array[]} points - list of points
 * @param {Shared} [shared=Shared.defaultShared] - shared property to apply
 * @param {Plane} [plane] - plane of the polygon
 *
 * @example
 * const points = [
 *   [0,  0, 0],
 *   [0, 10, 0],
 *   [0, 10, 10]
 * ]
 * let observed = Polygon3.fromPoints(points)
 */
Polygon3.fromPoints = function (points, shared, plane) {
  let vertices = points.map(function (p) {
    let vec = Vector3D.fromVarious(p)
    return new Vertex3(vec)
  })

  if (!shared) shared = Shared.defaultShared

  if (!plane) plane = Plane.fromVector3Ds(vertices[0].pos, vertices[1].pos, vertices[2].pos)

  return new Polygon3(vertices, shared, plane)
}

Polygon3.verticesConvex = function (vertices, planenormal) {
  let numvertices = vertices.length
  if (numvertices > 2) {
    let prevprevpos = vertices[numvertices - 2].pos
    let prevpos = vertices[numvertices - 1].pos
    for (let i = 0; i < numvertices; i++) {
      let pos = vertices[i].pos
      if (!Polygon3.isConvexPoint(prevprevpos, prevpos, pos, planenormal)) {
        return false
      }
      prevprevpos = prevpos
      prevpos = pos
    }
  }
  return true
}

// calculate whether three points form a convex corner
//  prevpoint, point, nextpoint: the 3 coordinates (Vector3D instances)
//  normal: the normal vector of the plane
Polygon3.isConvexPoint = function (prevpoint, point, nextpoint, normal) {
  let crossproduct = point.minus(prevpoint).cross(nextpoint.minus(point))
  let crossdotnormal = crossproduct.dot(normal)
  return (crossdotnormal >= 0)
}

Polygon3.isStrictlyConvexPoint = function (prevpoint, point, nextpoint, normal) {
  let crossproduct = point.minus(prevpoint).cross(nextpoint.minus(point))
  let crossdotnormal = crossproduct.dot(normal)
  return (crossdotnormal >= EPS)
}

module.exports = Polygon3
