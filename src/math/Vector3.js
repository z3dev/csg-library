const {quantizeForSpace} = require('./utils')

const IsFloat = require('../utils/IsFloat')

const Vector2 = require('./Vector2')

/** Class Vector3
 * Represents a 3D vector with X, Y, Z coordinates.
 * @constructor
 *
 * @example
 * new CSG.Vector3(1, 2, 3);
 */

const Vector3 = function (x, y, z) {
  this._x = x
  this._y = y
  this._z = z
}

Vector3.fromVarious = function (x, y, z) {
  if (arguments.length === 3) {
    x = parseFloat(x)
    y = parseFloat(y)
    z = parseFloat(z)
  } else if (arguments.length === 2) {
    x = parseFloat(x)
    y = parseFloat(y)
    z = 0
  } else {
    var ok = true
    if (arguments.length === 1) {
      if (typeof (x) === 'object') {
        if (x instanceof Vector3) {
          z = x._z
          y = x._y
          x = x._x
        } else if (x instanceof Vector2) {
          z = 0
          y = x._y
          x = x._x
        } else if (Array.isArray(x)) {
          if ((x.length < 2) || (x.length > 3)) {
            ok = false
          } else {
            if (x.length === 3) {
              z = parseFloat(x[2])
            } else {
              z = 0
            }
            y = parseFloat(x[1])
            x = parseFloat(x[0])
          }
        } else if (('x' in x) && ('y' in x)) {
          if ('z' in x) {
            z = parseFloat(x.z)
          } else {
            z = 0
          }
          y = parseFloat(x.y)
          x = parseFloat(x.x)
        } else if (('_x' in x) && ('_y' in x)) {
          if ('_z' in x) {
            z = parseFloat(x._z)
          } else {
            z = 0
          }
          y = parseFloat(x._y)
          x = parseFloat(x._x)
        } else ok = false
      } else {
        x = parseFloat(x)
        y = x
        z = x
      }
    } else ok = false
    if (ok) {
      if ((!IsFloat(x)) || (!IsFloat(y)) || (!IsFloat(z))) ok = false
    }
    if (!ok) {
      throw new Error('wrong arguments')
    }
  }
  return new Vector3(x, y, z)
}

Vector3.prototype = {
  get x () {
    return this._x
  },
  get y () {
    return this._y
  },
  get z () {
    return this._z
  },

  set x (v) {
    throw new Error('Vector3 is immutable')
  },
  set y (v) {
    throw new Error('Vector3 is immutable')
  },
  set z (v) {
    throw new Error('Vector3 is immutable')
  },

  canonicalize: function () {
    return new Vector3(quantizeForSpace(this._x), quantizeForSpace(this._y), quantizeForSpace(this._z))
  },

  clone: function () {
    return new Vector3(this._x, this._y, this._z)
  },

  negated: function () {
    return new Vector3(-this._x, -this._y, -this._z)
  },

  abs: function () {
    return new Vector3(Math.abs(this._x), Math.abs(this._y), Math.abs(this._z))
  },

  plus: function (a) {
    return new Vector3(this._x + a._x, this._y + a._y, this._z + a._z)
  },

  minus: function (a) {
    return new Vector3(this._x - a._x, this._y - a._y, this._z - a._z)
  },

  times: function (a) {
    return new Vector3(this._x * a, this._y * a, this._z * a)
  },

  dividedBy: function (a) {
    return new Vector3(this._x / a, this._y / a, this._z / a)
  },

  dot: function (a) {
    return this._x * a._x + this._y * a._y + this._z * a._z
  },

  lerp: function (a, t) {
    return this.plus(a.minus(this).times(t))
  },

  lengthSquared: function () {
    return this.dot(this)
  },

  length: function () {
    return Math.sqrt(this.lengthSquared())
  },

  unit: function () {
    return this.dividedBy(this.length())
  },

  cross: function (a) {
    return new Vector3(
      this._y * a._z - this._z * a._y, this._z * a._x - this._x * a._z, this._x * a._y - this._y * a._x)
  },

  distanceTo: function (a) {
    return this.minus(a).length()
  },

  distanceToSquared: function (a) {
    return this.minus(a).lengthSquared()
  },

  equals: function (a) {
    return (this._x === a._x) && (this._y === a._y) && (this._z === a._z)
  },

  // Right multiply by a 4x4 matrix (the vector is interpreted as a row vector)
  // Returns a new Vector3
  multiply4x4: function (matrix4x4) {
    return matrix4x4.leftMultiply1x3Vector(this)
  },

  transform: function (matrix4x4) {
    return matrix4x4.leftMultiply1x3Vector(this)
  },

  toString: function () {
    return '[' + this._x + ', ' + this._y + ', ' + this._z + ']'
  },

  // find a vector that is somewhat perpendicular to this one
  randomNonParallelVector: function () {
    var abs = this.abs()
    if ((abs._x <= abs._y) && (abs._x <= abs._z)) {
      return new Vector3(1, 0, 0)
    } else if ((abs._y <= abs._x) && (abs._y <= abs._z)) {
      return new Vector3(0, 1, 0)
    } else {
      return new Vector3(0, 0, 1)
    }
  },

  min: function (p) {
    return new Vector3(
      Math.min(this._x, p._x), Math.min(this._y, p._y), Math.min(this._z, p._z))
  },

  max: function (p) {
    return new Vector3(
      Math.max(this._x, p._x), Math.max(this._y, p._y), Math.max(this._z, p._z))
  }
}

module.exports = Vector3
