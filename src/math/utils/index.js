module.exports = {
//  degToRad: require('./degToRad'),
//  radToDeg: require('./radToDeg'),
  interpolateBetween2DPointsForY: require('./interpolateBetween2DPointsForY'),
  quantizeForSpace: require('./quantizeForSpace'),
  solve2Linear: require('./solve2Linear')
}
