const {getTag} = require('../constants')

const Vector2D = require('./Vector2')

const Vertex2 = function (pos) {
  this.pos = pos
}

Vertex2.fromObject = function (obj) {
  return new Vertex2(new Vector2D(obj.pos._x, obj.pos._y))
}

Vertex2.prototype = {
  toString: function () {
    return this.pos.toString()
  },

  getTag: function () {
    var result = this.tag
    if (!result) {
      result = getTag()
      this.tag = result
    }
    return result
  }
}

module.exports = Vertex2
