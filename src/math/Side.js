const {getTag} = require('../constants')

const Polygon = require('./Polygon3')
const Vector2 = require('./Vector2')
const Vertex2 = require('./Vertex2')
const Vertex3 = require('./Vertex3')

const Side = function (vertex0, vertex1) {
  if (!(vertex0 instanceof Vertex2)) throw new Error('Assertion failed') // TBD remove later
  if (!(vertex1 instanceof Vertex2)) throw new Error('Assertion failed') // TBD remove later
  this.vertex0 = vertex0
  this.vertex1 = vertex1
}

Side.fromObject = function (obj) {
  var vertex0 = Vertex2.fromObject(obj.vertex0)
  var vertex1 = Vertex2.fromObject(obj.vertex1)
  return new Side(vertex0, vertex1)
}

Side.fromPoints = function (point1, point2) {
  let vector1 = Vector2.fromVarious(point1)
  let vector2 = Vector2.fromVarious(point2)
  return new Side(new Vertex2(vector1), new Vertex2(vector2))
}

Side._fromFakePolygon = function (polygon) {
  // this can happen based on union, seems to be residuals -
  // return null and handle in caller
  if (polygon.vertices.length < 4) {
    return null
  }
  var vert1Indices = []
  var pts2d = polygon.vertices.filter(function (v, i) {
    if (v.pos.z > 0) {
      vert1Indices.push(i)
      return true
    }
    return false
  })
    .map(function (v) {
      return new Vector2(v.pos.x, v.pos.y)
    })
  if (pts2d.length !== 2) {
    throw new Error('Assertion failed: _fromFakePolygon: not enough points found') // TBD remove later
  }
  var d = vert1Indices[1] - vert1Indices[0]
  if (d === 1 || d === 3) {
    if (d === 1) {
      pts2d.reverse()
    }
  } else {
    throw new Error('Assertion failed: _fromFakePolygon: unknown index ordering')
  }
  var result = new Side(new Vertex2(pts2d[0]), new Vertex2(pts2d[1]))
  return result
}

Side.prototype = {
  toString: function () {
    return this.vertex0.toString() + ' -> ' + this.vertex1.toString()
  },

  toPolygon3D: function (z0, z1) {
    const points = [
      this.vertex0.pos.toVector3D(z0),
      this.vertex1.pos.toVector3D(z0),
      this.vertex1.pos.toVector3D(z1),
      this.vertex0.pos.toVector3D(z1)
    ]
    return Polygon.fromPoints(points)
  },

  canonicalize: function () {
    var newp1 = this.vertex0.pos.canonicalize()
    var newp2 = this.vertex1.pos.canonicalize()
    return new Side(new Vertex2(newp1), new Vertex2(newp2))
  },

  transform: function (matrix4x4) {
    var newp1 = this.vertex0.pos.transform(matrix4x4)
    var newp2 = this.vertex1.pos.transform(matrix4x4)
    return new Side(new Vertex2(newp1), new Vertex2(newp2))
  },

  flipped: function () {
    return new Side(this.vertex1, this.vertex0)
  },

  direction: function () {
    return this.vertex1.pos.minus(this.vertex0.pos)
  },

  getTag: function () {
    var result = this.tag
    if (!result) {
      result = getTag()
      this.tag = result
    }
    return result
  },

  lengthSquared: function () {
    let x = this.vertex1.pos.x - this.vertex0.pos.x
    let y = this.vertex1.pos.y - this.vertex0.pos.y
    return x * x + y * y
  },

  length: function () {
    return Math.sqrt(this.lengthSquared())
  }
}

module.exports = Side
