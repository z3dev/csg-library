const flatten = require('../utils/flatten')

const geom3 = require('../geom3')
const geom3union = require('../ops-geom3/union')

const fromFakePolygons = require('./fromFakePolygons')
const to3DWalls = require('./to3DWalls')

/*
 * @returns {geom2} new geometry
 */
const union = (...geometries) => {
  geometries = flatten(geometries)
  const newgeometries = geometries.map((geometry) => {
    return to3DWalls({z0: -1, z1: 1}, geometry)
  })

  let newgeom3 = geom3union(newgeometries)

  return fromFakePolygons(geom3.toPolygons(newgeom3))
}

module.exports = union
