const Side = require('../math/Side')

const geom2 = require('../geom2')

// create a list of edges (sides) with unique vertexes
// this creates ordered edges which is used to find outlines
const toEdges = (sides) => {
  let vertices = [] // list of unique vertices
  let edges = []

  const getUniqueVertex = (vertex) => {
    let i = vertices.findIndex((v) => {
      return vertex.pos.equals(v.pos)
    })
    if (i < 0) {
      vertices.push(vertex)
      return vertex
    }
    return vertices[i]
  }

  sides.forEach((side) => {
    edges.push(new Side(getUniqueVertex(side.vertex0), getUniqueVertex(side.vertex1)))
  })
  return edges
}

/*
 * Create a list of outlines (ordered points) from the given geometry.
 */
const toOutlines = function (options, geometry) {
  let sides = toEdges(geom2.toSides(geometry))

  let sideTagToSideMap = {}
  let startVertexTagToSideTagMap = {}

  sides.forEach(function (side) {
    let sidetag = side.getTag()
    sideTagToSideMap[sidetag] = side
    let startvertextag = side.vertex0.getTag()
    if (!(startvertextag in startVertexTagToSideTagMap)) {
      startVertexTagToSideTagMap[startvertextag] = []
    }
    startVertexTagToSideTagMap[startvertextag].push(sidetag)
  })

  let outlines = []
  while (true) {
    let startsidetag = null
    for (let aVertexTag in startVertexTagToSideTagMap) {
      let sidesForVertex = startVertexTagToSideTagMap[aVertexTag]
      startsidetag = sidesForVertex[0]
      sidesForVertex.splice(0, 1)
      if (sidesForVertex.length === 0) {
        delete startVertexTagToSideTagMap[aVertexTag]
      }
      break
    }

    if (startsidetag === null) break // we've had all sides

    let connectedVertexPoints = []
    let sidetag = startsidetag
    let currentside = sideTagToSideMap[sidetag]
    let startvertextag = currentside.vertex0.getTag()
    while (true) {
      connectedVertexPoints.push(currentside.vertex0.pos)
      let nextvertextag = currentside.vertex1.getTag()
      if (nextvertextag === startvertextag) break // we've closed the polygon
      if (!(nextvertextag in startVertexTagToSideTagMap)) {
        throw new Error('Area is not closed!')
      }
      let nextpossiblesidetags = startVertexTagToSideTagMap[nextvertextag]
      let nextsideindex = -1
      if (nextpossiblesidetags.length === 1) {
        nextsideindex = 0
      } else {
        // more than one side starting at the same vertex, which means we have
        // two shapes touching at the same corner
        let bestangle = null
        let sideangle = currentside.direction().angleDegrees()
        for (let sideindex = 0; sideindex < nextpossiblesidetags.length; sideindex++) {
          let nextpossiblesidetag = nextpossiblesidetags[sideindex]
          let possibleside = sideTagToSideMap[nextpossiblesidetag]
          let angle = possibleside.direction().angleDegrees()
          let angledif = angle - sideangle
          if (angledif < -180) angledif += 360
          if (angledif >= 180) angledif -= 360
          if ((nextsideindex < 0) || (angledif > bestangle)) {
            nextsideindex = sideindex
            bestangle = angledif
          }
        }
      }
      let nextsidetag = nextpossiblesidetags[nextsideindex]
      nextpossiblesidetags.splice(nextsideindex, 1)
      if (nextpossiblesidetags.length === 0) {
        delete startVertexTagToSideTagMap[nextvertextag]
      }
      currentside = sideTagToSideMap[nextsidetag]
    } // inner loop
    // due to the logic of fromPoints()
    // move the first point to the last
    if (connectedVertexPoints.length > 0) {
      connectedVertexPoints.push(connectedVertexPoints.shift())
    }
    outlines.push(connectedVertexPoints)
  } // outer loop
  return outlines
}

module.exports = toOutlines
