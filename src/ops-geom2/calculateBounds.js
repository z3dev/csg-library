const geom2 = require('../geom2')

/*
 * Calculate the min and max bounds of the given geometry.
 * @return {[[x,y,z], [x,y,z]]} the bounds (min and max points) for the geometry
 */
const calculateBounds = function (geometry) {
  const points = geom2.toPoints(geometry)

  let minpoint
  if (points.length === 0) {
    minpoint = [0, 0, 0]
  } else {
    minpoint = [points[0][0], points[0][1], 0]
  }
  let maxpoint = minpoint
  points.forEach(function (point) {
    minpoint = [Math.min(minpoint[0], point[0]), Math.min(minpoint[1], point[1]), 0]
    maxpoint = [Math.max(maxpoint[0], point[0]), Math.max(maxpoint[1], point[1]), 0]
  })
  return [minpoint, maxpoint]
}

module.exports = calculateBounds
