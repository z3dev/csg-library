const {canonicalize} = require('../geom2')

const Vector3 = require('../math/Vector3')

const toVector3DPairs = function (options, geometry) {
  geometry = canonicalize(geometry)

  const pairs = geometry.sides.map(function (side) {
    let p0 = side.vertex0.pos
    let p1 = side.vertex1.pos
    let v0 = new Vector3(p0.x, p0.y, 0)
    let v1 = new Vector3(p1.x, p1.y, 0)
    let t0 = v0.transform(options.matrix)
    let t1 = v1.transform(options.matrix)
    return [t0, t1]
  })
  return pairs
}

module.exports = toVector3DPairs
