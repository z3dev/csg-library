const Side = require('../math/Side')

const {create} = require('../geom2')

/*
 * Create a new geometry from the given object which contains platform specific binary data buffers.
 * See https://www.ecma-international.org/ecma-262/6.0/#sec-typedarray-objects
 */
const fromBinaryDataBuffers = (options, obj) => {
  if (obj['class'] !== options.class) throw new Error('invalid object')

  let vertexData = obj.vertexData
  let vertexIndices = obj.sideVertexIndices

  let vertices = []
  let numvertices = vertexData.length / 2
  let arrayindex = 0
  for (let vertexindex = 0; vertexindex < numvertices; vertexindex++) {
    let x = vertexData[arrayindex++]
    let y = vertexData[arrayindex++]
    vertices.push([x, y])
  }

  let sides = []
  let numsides = vertexIndices.length / 2
  arrayindex = 0
  for (let sideindex = 0; sideindex < numsides; sideindex++) {
    let vertexindex0 = vertexIndices[arrayindex++]
    let vertexindex1 = vertexIndices[arrayindex++]
    let side = Side.fromPoints(vertices[vertexindex0], vertices[vertexindex1])
    sides.push(side)
  }
  return create(sides)
}

module.exports = fromBinaryDataBuffers
