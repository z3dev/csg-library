const expandShell = require('./expandShell')
const subtract = require('./subtract')

/*
 */
const contract = (options, geometry) => {
  // FIXME this doesn't contract... it produces non-sensical shapes
  let expanded = expandShell(options, geometry)
  let result = subtract(geometry, expanded)
  return result
}

module.exports = contract
