const geom2 = require('../geom2')

const geom3 = require('../geom3')

const to3DWalls = function (options, geometry) {
  let sides = geom2.toSides(geometry)

  let polygons = sides.map((side) => {
    return side.toPolygon3D(options.z0, options.z1)
  })
  return geom3.create(polygons)
}

module.exports = to3DWalls
