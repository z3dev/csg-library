const flatten = require('../utils/flatten')

const toCSGWall = require('./toCSGWall')
const fromFakePolygons = require('./fromFakePolygons')

/*
 * @returns {geom2} new geometry
 */
const intersect = (...geometries) => {
  geometries = flatten(geometries)
  const csgs = geometries.map((geometry) => {
    return toCSGWall({z0: -1, z1: 1}, geometry)
  })

  let result = csgs.shift()
  result = result.intersect(csgs)

  return fromFakePolygons(result.toPolygons())
}

module.exports = intersect
