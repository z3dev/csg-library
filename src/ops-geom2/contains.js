const geom2 = require('../geom2')

const hasPointInside = require('./hasPointInside')

/**
 * Determine if one geometry constains another geometry.
 * @param {geom2} geometry1 - the inner geometry
 * @param {geom3} geometry2 - the outer geometry
 * @returns {Boolean}
 */
const constains = (geometry1, geometry2) {
  const sides1 = geom2.toSides(geometry1)

  if (sides1.length === 0) return false

  // test that each point (of each side) lies inside geometry2
  sides1.forEach((side) => {
    if (!hasPointInside(side.vertex0.pos, geometry2)) {
      return false
    }
  })
  return true
}

module.exports = contains
