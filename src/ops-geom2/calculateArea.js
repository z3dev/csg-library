const geom2 = require('../geom2')

// see http://local.wasp.uwa.edu.au/~pbourke/geometry/polyarea/ :
// Area of the polygon. For a counter clockwise rotating polygon the area is positive, otherwise negative
const calculateArea = function (geometry) {
  let area = 0
  let sides = geom2.toSides(geometry)
  sides.forEach(function (side) {
    area += side.vertex0.pos.cross(side.vertex1.pos)
  })
  area *= 0.5
  return area
}

module.exports = calculateArea
