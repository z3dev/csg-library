const Polygon3 = require('../math/Polygon3')
const Vector2 = require('../math/Vector2')

const {Connector} = require('../core/connectors')
const {fromPolygons} = require('../core/CSGFactories')

const calculateBounds = require('./calculateBounds')
const toCSGWall = require('./toCSGWall')

/*
 * transform a cag into the polygons of a corresponding 3d plane, positioned per options
 * Accepts a connector for plane positioning, or optionally
 * single translation, axisVector, normalVector arguments
 * (toConnector has precedence over single arguments if provided)
 */
const toPlanePolygons = function (options, geometry) {
  // santity check
  let bounds = calculateBounds(geometry)
  if (bounds[0][0] === bounds[1][0] && bounds[0][1] === bounds[1][1]) return []

  // reference connector for transformation
  let origin = [0, 0, 0]
  let defaultAxis = [0, 0, 1]
  let defaultNormal = [0, 1, 0]
  let thisConnector = new Connector(origin, defaultAxis, defaultNormal)

  // translated connector per options
  let flipped = options.flipped || false
  let translation = options.translation || origin
  let axisVector = options.axisVector || defaultAxis
  let normalVector = options.normalVector || defaultNormal

  // will override above if options has toConnector
  let toConnector = options.toConnector || new Connector(translation, axisVector, normalVector)

  // resulting transform
  let m = thisConnector.getTransformationTo(toConnector, false, 0)

  // create plane as a (partial non-closed) CSG in XY plane
  let point0 = new Vector2(bounds[0][0], bounds[0][1])
  let point1 = new Vector2(bounds[1][0], bounds[1][1])
  point0 = point0.minus(new Vector2(1, 1))
  point1 = point1.plus(new Vector2(1, 1))
  let csgshell = toCSGWall({z0: -1, z1: 1}, geometry)
  let csgplane = fromPolygons([
    Polygon3.fromPoints([[point0.x, point0.y, 0],
                         [point1.x, point0.y, 0],
                         [point1.x, point1.y, 0],
                         [point0.x, point1.y, 0]])
  ])
  if (flipped) {
    csgplane = csgplane.invert()
  }
  // intersectSub -> prevent premature retesselate/canonicalize
  csgplane = csgplane.intersectSub(csgshell, false, false)
  let polys = csgplane.toPolygons()
  // only keep the polygons in the positive Z plane
  polys = polys.filter(function (polygon) {
    return Math.abs(polygon.plane.normal.z) > 0.99
  })
  // finally, position the plane per passed transformations
  return polys.map(function (poly) {
    return poly.transform(m)
  })
}

module.exports = toPlanePolygons
