const {defaultResolution3D, EPS} = require('../constants')

const clamp = require('../utils/clamp')
const rightMultiply1x3VectorToArray = require('../utils/rightMultiply1x3VectorToArray')

const Matrix4x4 = require('../math/Matrix4x4')
const Polygon3 = require('../math/Polygon3')

const geom2 = require('../geom2')
const geom3 = require('../geom3')

const toPlanePolygons = require('./toPlanePolygons')

/**
 * Rotate extrusion the given geometry using the given options.
 * @param {Object} [options] - options for extrusion
 * @param {Float} [options.angle=1] - angle of the extrusion, in degrees
 * @param {Float} [options.startAngle=1] - start angle of the extrusion, in degrees
 * @param {Float} [options.overflow='cap'] - what to do with points outside of bounds (+ / - x) :
 * defaults to capping those points to 0 (only supported behaviour for now)
 * @param {Integer} [options.resolution=defaultResolution3D] - resolution/number of segments of the extrusion
 * @param {geom2} geometry the 2D geometry to extrude
 * @returns {geom3} new extruded 3D geometry
 */
const extrudeRotate = (options, geometry) => {
  const defaults = {
    resolution: defaultResolution3D,
    startAngle: 0,
    angle: 360,
    overflow: 'cap'
  }
  options = Object.assign({}, defaults, options)
  let {resolution, startAngle, angle, overflow} = options

  // FIXME BIG NOTE... This code creates a rotation in the wrong direction (left-hand rule).
  // FIXME BIG NOTE... The whole library needs to follow RIGHT-HAND RULE.
  angle = -angle // reverse the rotation for now

  // are we dealing with a positive or negative angle (for normals flipping)
  const positive = angle > 0
  // limit actual angle between 0 & 360, regardless of direction
  const totalAngle = positive ? clamp((startAngle + angle), 0, 360) : clamp((startAngle + angle), -360, 0)
  // adapt to the totalAngle : 1 extra segment per 45 degs if not 360 deg extrusion
  // needs to be at least one and higher then the input resolution
  const segments = Math.max(
    Math.floor(Math.abs(totalAngle) / 45),
    1,
    resolution
  )
  // maximum distance per axis between two points before considering them to be the same
  const overlapTolerance = EPS
  // convert baseshape to just an array of points, easier to deal with
  let shapePoints = geom2.toPoints(geometry)

  // determine if the rotate_extrude can be computed in the first place
  // ie all the points have to be either x > 0 or x < 0

  // generic solution to always have a valid solid, even if points go beyond x/ -x
  // 1. split points up between all those on the 'left' side of the axis (x<0) & those on the 'righ' (x>0)
  // 2. for each set of points do the extrusion operation IN OPOSITE DIRECTIONS
  // 3. union the two resulting solids

  // 1. alt : OR : just cap of points at the axis ?

  const pointsWithNegativeX = shapePoints.filter(x => x[0] < 0)
  const pointsWithPositiveX = shapePoints.filter(x => x[0] >= 0)
  const arePointsWithNegAndPosX = pointsWithNegativeX.length > 0 && pointsWithPositiveX.length > 0

  if (arePointsWithNegAndPosX && overflow === 'cap') {
    if (pointsWithNegativeX.length > pointsWithPositiveX.length) {
      shapePoints = shapePoints.map(function (point) {
        return [Math.min(point[0], 0), point[1]]
      })
    } else if (pointsWithPositiveX.length >= pointsWithNegativeX.length) {
      shapePoints = shapePoints.map(function (point) {
        return [Math.max(point[0], 0), point[1]]
      })
    }
  }

  // for each of the intermediary steps in the extrusion
  let polygons = []
  for (let i = 1; i < segments + 1; i++) {
    // for each side of the 2d shape
    for (let j = 0; j < shapePoints.length - 1; j++) {
      // 2 points of a side
      const curPoint = shapePoints[j]
      const nextPoint = shapePoints[j + 1]

      // compute matrix for current and next segment angle
      let prevMatrix = Matrix4x4.rotationZ((i - 1) / segments * angle + startAngle)
      let curMatrix = Matrix4x4.rotationZ(i / segments * angle + startAngle)

      const pointA = rightMultiply1x3VectorToArray(prevMatrix, [curPoint[0], 0, curPoint[1]])
      const pointAP = rightMultiply1x3VectorToArray(curMatrix, [curPoint[0], 0, curPoint[1]])
      const pointB = rightMultiply1x3VectorToArray(prevMatrix, [nextPoint[0], 0, nextPoint[1]])
      const pointBP = rightMultiply1x3VectorToArray(curMatrix, [nextPoint[0], 0, nextPoint[1]])

      let overlappingPoints = false
      if (Math.abs(pointA[0] - pointAP[0]) < overlapTolerance && Math.abs(pointB[1] - pointBP[1]) < overlapTolerance) {
        overlappingPoints = true
      }

      // we do not generate a single quad because:
      // 1. it does not allow eliminating unneeded triangles in case of overlapping points
      // 2. the current cleanup routines of csg.js create degenerate shapes from those quads

      if (positive) {
        // CW
        polygons.push(Polygon3.fromPoints([pointA, pointB, pointBP]))
        if (!overlappingPoints) {
          polygons.push(Polygon3.fromPoints([pointBP, pointAP, pointA]))
        }
      } else {
        // CCW
        if (!overlappingPoints) {
          polygons.push(Polygon3.fromPoints([pointA, pointAP, pointBP]))
        }
        polygons.push(Polygon3.fromPoints([pointBP, pointB, pointA]))
      }
    }
  }
  // if we do not do a full extrusion, we want caps at both ends (closed volume)
  if (Math.abs(totalAngle) < 360) {
    // we need to recreate the side with capped points where applicable
    const sideShape = geom2.fromPoints(shapePoints)
    const endMatrix = Matrix4x4.rotationX(90).multiply(
      Matrix4x4.rotationZ(-startAngle)
    )
    const endCap = toPlanePolygons({flipped: positive}, sideShape)
      .map(x => x.transform(endMatrix))

    const startMatrix = Matrix4x4.rotationX(90).multiply(
      Matrix4x4.rotationZ(-angle - startAngle)
    )
    const startCap = toPlanePolygons({flipped: !positive}, sideShape)
      .map(x => x.transform(startMatrix))
    polygons = polygons.concat(endCap).concat(startCap)
  }
  let newgeometry = geom3.create(polygons)
  return newgeometry
}

module.exports = extrudeRotate
