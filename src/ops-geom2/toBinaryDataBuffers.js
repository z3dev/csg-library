const {canonicalize} = require('../geom2')

/**
 * Convert the given geometry to platform specific binary data buffers.
 * See fromCompactBinary.
 * @return {CompactBinary}
 */
const toBinaryDataBuffers = function (options, geometry) {
  geometry = canonicalize(geometry)

  let numsides = geometry.sides.length
  let vertexmap = {}
  let vertices = []
  let numvertices = 0
  let sideVertexIndices = new Uint32Array(2 * numsides)
  let sidevertexindicesindex = 0
  geometry.sides.forEach(function (side) {
    [side.vertex0, side.vertex1].forEach(function (v) {
      let vertextag = v.getTag()
      let vertexindex
      if (!(vertextag in vertexmap)) {
        vertexindex = numvertices++
        vertexmap[vertextag] = vertexindex
        vertices.push(v)
      } else {
        vertexindex = vertexmap[vertextag]
      }
      sideVertexIndices[sidevertexindicesindex++] = vertexindex
    })
  })
  let vertexData = new Float64Array(numvertices * 2)
  let verticesArrayIndex = 0
  vertices.forEach(function (v) {
    let pos = v.pos
    vertexData[verticesArrayIndex++] = pos._x
    vertexData[verticesArrayIndex++] = pos._y
  })
  let result = {
    'class': options.class,
    sideVertexIndices: sideVertexIndices,
    vertexData: vertexData
  }
  return result
}

module.exports = toBinaryDataBuffers
