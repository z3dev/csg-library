const {canonicalize} = require('../geom2')

const {fromPolygons} = require('../core/CSGFactories')

const toCSGWall = function (options, geometry) {
  geometry = canonicalize(geometry)

  let polygons = geometry.sides.map(function (side) {
    return side.toPolygon3D(options.z0, options.z1)
  })
  return fromPolygons(polygons)
}

module.exports = toCSGWall
