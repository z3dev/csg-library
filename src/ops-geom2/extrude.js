const {defaultResolution3D} = require('../constants')

const Vector3 = require('../math/Vector3')
const Matrix4x4 = require('../math/Matrix4x4')

const {Connector} = require('../core/connectors')

const toPlanePolygons = require('./toPlanePolygons')
const toWallPolygons = require('./toWallPolygons')

const geom3 = require('../geom3')

/**
 * Extrude the given geometry using the given options.
 * @param {Object} options - options for extrude
 * @param {Array} options.offset - the direction of the extrusion as a 3D vector
 * @param {Number} options.twistangle - the final rotatation (degrees) about the origin of the shape
 * @param {Integer} options.twiststeps - the resolution of the twist (if any) about the axis
 * @param {geom2} geometry - the geometry to extrude
 * @returns {geom3} the extruded 3D geometry
*/
const extrude = function (options, geometry) {
  const defaults = {
    offset: [0, 0, 1],
    twistangle: 0,
    twiststeps: defaultResolution3D
  }
  let {offset, twistangle, twiststeps} = Object.assign({}, defaults, options)

  // convert to vector in order to perform math
  let offsetv = new Vector3(offset[0], offset[1], offset[2])

  if (twistangle === 0 || twiststeps < 1) {
    twiststeps = 1
  }

  let flipped = (offsetv.z < 0)
  let normalVector = new Vector3(0, 1, 0) // normal of Z axis

  // bottom
  let polygons = []
  polygons = polygons.concat(
    toPlanePolygons(
      {translation: [0, 0, 0], normalVector: normalVector, flipped: !flipped},
      geometry
    )
  )

  // top
  polygons = polygons.concat(
    toPlanePolygons(
      {translation: offsetv, normalVector: normalVector.transform(Matrix4x4.rotationZ(twistangle)), flipped: flipped},
      geometry
    )
  )

  // walls
  for (let i = 0; i < twiststeps; i++) {
    let c1 = new Connector(offsetv.times(i / twiststeps), [0, 0, offsetv.z],
      normalVector.transform(Matrix4x4.rotationZ((i * twistangle / twiststeps))))
    let c2 = new Connector(offsetv.times((i + 1) / twiststeps), [0, 0, offsetv.z],
      normalVector.transform(Matrix4x4.rotationZ(((i + 1) * twistangle / twiststeps))))
    polygons = polygons.concat(toWallPolygons({toConnector1: c1, toConnector2: c2}, geometry))
  }
  return geom3.create(polygons)
}

module.exports = extrude
