const Side = require('../math/Side')

const geom2 = require('../geom2')

// Converts the given polygons to a new geometry.
// The polygons must have only z coordinates +1 and -1, as constructed by toCSGWall(-1, 1).

const fromFakePolygons = function (polygons) {
  let sides = polygons.map(function (polygon) {
    return Side._fromFakePolygon(polygon)
  }).filter(function (s) {
    return s !== null
  })
  return geom2.create(sides)
}

module.exports = fromFakePolygons
