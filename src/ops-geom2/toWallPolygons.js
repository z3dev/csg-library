const {canonicalize, toString} = require('../geom2')

const Polygon3 = require('../math/Polygon3')

const {Connector} = require('../core/connectors')

const toVector3DPairs = require('./toVector3DPairs')

/*
 * given 2 connectors, this returns all polygons of a "wall" between 2
 * copies of this cag, positioned in 3d space as "bottom" and
 * "top" plane per connectors toConnector1, and toConnector2, respectively
 * options:
 *   toConnector1
 *   toConnector2
 *     walls go from toConnector1 to toConnector2
 */
const toWallPolygons = function (options, geometry) {
  geometry = canonicalize(geometry)

  // normals are going to be correct as long as toConn2.point - toConn1.point
  // points into cag normal direction (check in caller)

  // options
  let toConnector1 = options.toConnector1
  let toConnector2 = options.toConnector2
  if (!(toConnector1 instanceof Connector && toConnector2 instanceof Connector)) {
    throw new Error('could not parse Connector arguments toConnector1 or toConnector2')
  }
  let toCag = geometry

  let origin = [0, 0, 0]
  let defaultAxis = [0, 0, 1]
  let defaultNormal = [0, 1, 0]
  let thisConnector = new Connector(origin, defaultAxis, defaultNormal)

  let m1 = thisConnector.getTransformationTo(toConnector1, false, 0)
  let m2 = thisConnector.getTransformationTo(toConnector2, false, 0)
  let vps1 = toVector3DPairs({matrix: m1}, geometry)
  let vps2 = toVector3DPairs({matrix: m2}, toCag)

  let polygons = []
  vps1.forEach(function (vp1, i) {
    polygons.push(Polygon3.fromPoints([vps2[i][1], vps2[i][0], vp1[0]]))
    polygons.push(Polygon3.fromPoints([vps2[i][1], vp1[0], vp1[1]]))
  })
  return polygons
}

module.exports = toWallPolygons
