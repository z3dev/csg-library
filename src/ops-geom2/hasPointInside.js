const geom2 = require('../geom2')

/**
 * Determine if the given point lies inside the given 2D geometry.
 * Ray-casting algorithm based on :
 * https://github.com/substack/point-in-polygon/blob/master/index.js
 * http://www.ecse.rp1.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
 * originaly writed for https://github.com/lautr3k/SLAcer.js/blob/dev/js/slacer/slicer.js#L82
 * @param {geom2} geometry - the given geometry to inspect
 * @param {Array} p0 - 2D point represented as an array of two numbers
 * @returns {Boolean}
 */
const hasPointInside = (point, geometry) => {
  const sides = geom2.toSides(geometry)

  let inside = false
  sides.forEach((side) => {
    let p1 = side.vertex0.pos
    let p2 = side.vertex1.pos
    if (hasPointInside.c1(point, p1, p2) && hasPointInside.c2(point, p1, p2)) {
      inside = !inside
    }
  })
  return inside
}

hasPointInside.c1 = (p0, p1, p2) => (p1.y > p0.y) !== (p2.y > p0.y)
hasPointInside.c2 = (p0, p1, p2) => (p0.x < (p2.x - p1.x) * (p0.y - p1.y) / (p2.y - p1.y) + p1.x)

module.exports = hasPointInside
