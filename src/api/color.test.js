const test = require('ava')
const { color, rgb2hsl, hsl2rgb, rgb2hsv, hsv2rgb, html2rgb, rgb2html, css2rgb } = require('./color')

test('css2rgb', t => {
  const c1 = css2rgb('black')
  const e1 = [0 / 255, 0 / 255, 0 / 255]
  t.deepEqual(c1, e1)

  const c2 = css2rgb('yellowgreen')
  const e2 = [154 / 255, 205 / 255, 50 / 255]
  t.deepEqual(c2, e2)
})

test('rgb2hsl', t => {
  const obs = rgb2hsl(1, 0, 0)
  const expColor = [0, 1, 0.5]

  t.deepEqual(obs, expColor)
})

test('hsl2rgb', t => {
  const obs = hsl2rgb(0, 1, 0)
  const expColor = [0, 0, 0]

  t.deepEqual(obs, expColor)
})

test('rgb2hsv', t => {
  const obs = rgb2hsv(1, 0, 0.5)
  const expColor = [0.9166666666666666, 1, 1]

  t.deepEqual(obs, expColor)
})

test('hsv2rgb', t => {
  const obs = hsv2rgb(0, 0.2, 0)
  const expColor = [0, 0, 0]

  t.deepEqual(obs, expColor)
})

test('html2rgb', t => {
  const obs = html2rgb('#000000')
  const expColor = [0, 0, 0]

  t.deepEqual(obs, expColor)
})

test('rgb2html', t => {
  const html = rgb2html(1, 0, 0.5)
  const expHtml = '#ff007f'

  t.deepEqual(html, expHtml)
})
