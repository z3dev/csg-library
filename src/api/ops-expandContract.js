const {EPS, angleEPS, defaultResolution2D} = require('../constants')

const Polygon = require('../math/Polygon3')
const Vertex3 = require('../math/Vertex3')
const Side = require('../math/Side')
const Vertex2 = require('../math/Vertex2')
const Vector2 = require('../math/Vector2')

const {fnNumberSort} = require('../core/utils')
const isCSG = require('../core/utils/isCSG')
const {fromPoints} = require('../core/CAGFactories')

const extrudePolygon = require('../api/extrudePolygon')
const {parseOptionAsFloat, parseOptionAsInt} = require('../api/optionParsers')

const expand = function (shape, radius, resolution) {
  let result
  if (isCSG(shape)) {
    result = shape.union(expandedShellOfCCSG(shape, radius, resolution))
    result = result.reTesselated()
    result.properties = shape.properties // keep original properties
  } else {
    result = shape.union(expandedShellOfCAG(shape, radius, resolution))
  }
  return result
}

const contract = function (shape, radius, resolution) {
  let result
  if (isCSG(shape)) {
    result = shape.subtract(expandedShellOfCCSG(shape, radius, resolution))
    result = result.reTesselated()
    result.properties = shape.properties // keep original properties
  } else {
    result = shape.subtract(expandedShellOfCAG(shape, radius, resolution))
  }
  return result
}

const expandedShellOfCAG = function (cag, radius, resolution) {
  const CAG = require('../api/CAG') // FIXME, circular dependency !!
  resolution = resolution || 8
  if (resolution < 4) resolution = 4
  let cags = []
  let pointmap = {}
  let sides = cag.toSides()
  sides.forEach(function (side) {
    let d = side.vertex1.pos.minus(side.vertex0.pos)
    let dl = d.length()
    if (dl > EPS) {
      d = d.times(1.0 / dl)
      let normal = d.normal().times(radius)
      let shellpoints = [
        side.vertex1.pos.plus(normal),
        side.vertex1.pos.minus(normal),
        side.vertex0.pos.minus(normal),
        side.vertex0.pos.plus(normal)
      ]
      let newcag = fromPoints(shellpoints)
      cags.push(newcag)
      for (let step = 0; step < 2; step++) {
        let p1 = (step === 0) ? side.vertex0.pos : side.vertex1.pos
        let p2 = (step === 0) ? side.vertex1.pos : side.vertex0.pos
        let tag = p1.x + ' ' + p1.y
        if (!(tag in pointmap)) {
          pointmap[tag] = []
        }
        pointmap[tag].push({
          'p1': p1,
          'p2': p2
        })
      }
    }
  })
  for (let tag in pointmap) {
    let m = pointmap[tag]
    let angle1, angle2
    let pcenter = m[0].p1
    if (m.length === 2) {
      let end1 = m[0].p2
      let end2 = m[1].p2
      angle1 = end1.minus(pcenter).angleDegrees()
      angle2 = end2.minus(pcenter).angleDegrees()
      if (angle2 < angle1) angle2 += 360
      if (angle2 >= (angle1 + 360)) angle2 -= 360
      if (angle2 < angle1 + 180) {
        let t = angle2
        angle2 = angle1 + 360
        angle1 = t
      }
      angle1 += 90
      angle2 -= 90
    } else {
      angle1 = 0
      angle2 = 360
    }
    let fullcircle = (angle2 > angle1 + 359.999)
    if (fullcircle) {
      angle1 = 0
      angle2 = 360
    }
    if (angle2 > (angle1 + angleEPS)) {
      let points = []
      if (!fullcircle) {
        points.push(pcenter)
      }
      let numsteps = Math.round(resolution * (angle2 - angle1) / 360)
      if (numsteps < 1) numsteps = 1
      for (let step = 0; step <= numsteps; step++) {
        let angle = angle1 + step / numsteps * (angle2 - angle1)
        if (step === numsteps) angle = angle2 // prevent rounding errors
        let point = pcenter.plus(Vector2.fromAngleDegrees(angle).times(radius))
        if ((!fullcircle) || (step > 0)) {
          points.push(point)
        }
      }
      let newcag = fromPoints(points)
      cags.push(newcag)
    }
  }
  let result = new CAG()
  result = result.union(cags)
  return result
}

/**
 * Create the expanded shell of the solid:
 * All faces are extruded to get a thickness of 2*radius
 * Cylinders are constructed around every side
 * Spheres are placed on every vertex
 * unionWithThis: if true, the resulting solid will be united with 'this' solid;
 * the result is a true expansion of the solid
 * If false, returns only the shell
 * @param  {Float} radius
 * @param  {Integer} resolution
 * @param  {Boolean} unionWithThis
 */
const expandedShellOfCCSG = function (_csg, radius, resolution, unionWithThis) {
  const CSG = require('../api/CSG') // FIXME: circular dependency ! CSG => this => CSG
  const {fromPolygons} = require('../core/CSGFactories') // FIXME: circular dependency !
  let csg = _csg.reTesselated()
  let result
  if (unionWithThis) {
    result = csg
  } else {
    result = new CSG()
  }

  // first extrude all polygons:
  const polygons = csg.toPolygons()
  polygons.forEach(function (polygon) {
    let extrudevector = polygon.plane.normal.unit().times(2 * radius)
    let translatedpolygon = polygon.translate(extrudevector.times(-0.5))
    let extrudedface = extrudePolygon(extrudevector, translatedpolygon)
    result = result.unionSub(extrudedface, false, false)
  })

  // Make a list of all unique vertex pairs (i.e. all sides of the solid)
  // For each vertex pair we collect the following:
  //   v1: first coordinate
  //   v2: second coordinate
  //   planenormals: array of normal vectors of all planes touching this side
  let vertexpairs = {} // map of 'vertex pair tag' to {v1, v2, planenormals}
  polygons.forEach(function (polygon) {
    let numvertices = polygon.vertices.length
    let prevvertex = polygon.vertices[numvertices - 1]
    let prevvertextag = prevvertex.getTag()
    for (let i = 0; i < numvertices; i++) {
      let vertex = polygon.vertices[i]
      let vertextag = vertex.getTag()
      let vertextagpair
      if (vertextag < prevvertextag) {
        vertextagpair = vertextag + '-' + prevvertextag
      } else {
        vertextagpair = prevvertextag + '-' + vertextag
      }
      let obj
      if (vertextagpair in vertexpairs) {
        obj = vertexpairs[vertextagpair]
      } else {
        obj = {
          v1: prevvertex,
          v2: vertex,
          planenormals: []
        }
        vertexpairs[vertextagpair] = obj
      }
      obj.planenormals.push(polygon.plane.normal)

      prevvertextag = vertextag
      prevvertex = vertex
    }
  })

  // now construct a cylinder on every side
  // The cylinder is always an approximation of a true cylinder: it will have <resolution> polygons
  // around the sides. We will make sure though that the cylinder will have an edge at every
  // face that touches this side. This ensures that we will get a smooth fill even
  // if two edges are at, say, 10 degrees and the resolution is low.
  // Note: the result is not retesselated yet but it really should be!
  for (let vertextagpair in vertexpairs) {
    let vertexpair = vertexpairs[vertextagpair]
    let startpoint = vertexpair.v1.pos
    let endpoint = vertexpair.v2.pos
    // our x,y and z vectors:
    let zbase = endpoint.minus(startpoint).unit()
    let xbase = vertexpair.planenormals[0].unit()
    let ybase = xbase.cross(zbase)

    // make a list of angles that the cylinder should traverse:
    let angles = []

    // first of all equally spaced around the cylinder:
    for (let i = 0; i < resolution; i++) {
      angles.push(i * Math.PI * 2 / resolution)
    }

    // and also at every normal of all touching planes:
    for (let i = 0, iMax = vertexpair.planenormals.length; i < iMax; i++) {
      let planenormal = vertexpair.planenormals[i]
      let si = ybase.dot(planenormal)
      let co = xbase.dot(planenormal)
      let angle = Math.atan2(si, co)

      if (angle < 0) angle += Math.PI * 2
      angles.push(angle)
      angle = Math.atan2(-si, -co)
      if (angle < 0) angle += Math.PI * 2
      angles.push(angle)
    }

    // this will result in some duplicate angles but we will get rid of those later.
    angles = angles.sort(fnNumberSort)

    // Now construct the cylinder by traversing all angles:
    let numangles = angles.length
    let prevp1
    let prevp2
    let startfacevertices = []
    let endfacevertices = []
    let polygons = []
    for (let i = -1; i < numangles; i++) {
      let angle = angles[(i < 0) ? (i + numangles) : i]
      let si = Math.sin(angle)
      let co = Math.cos(angle)
      let p = xbase.times(co * radius).plus(ybase.times(si * radius))
      let p1 = startpoint.plus(p)
      let p2 = endpoint.plus(p)
      let skip = false
      if (i >= 0) {
        if (p1.distanceTo(prevp1) < EPS) {
          skip = true
        }
      }
      if (!skip) {
        if (i >= 0) {
          startfacevertices.push(p1)
          endfacevertices.push(p2)
          let points = [prevp2, p2, p1, prevp1]
          let polygon = Polygon.fromPoints(points)
          polygons.push(polygon)
        }
        prevp1 = p1
        prevp2 = p2
      }
    }
    endfacevertices.reverse()
    polygons.push(Polygon.fromPoints(startfacevertices))
    polygons.push(Polygon.fromPoints(endfacevertices))
    let cylinder = fromPolygons(polygons)
    result = result.unionSub(cylinder, false, false)
  }

  // make a list of all unique vertices
  // For each vertex we also collect the list of normals of the planes touching the vertices
  let vertexmap = {}
  polygons.forEach(function (polygon) {
    polygon.vertices.forEach(function (vertex) {
      let vertextag = vertex.getTag()
      let obj
      if (vertextag in vertexmap) {
        obj = vertexmap[vertextag]
      } else {
        obj = {
          pos: vertex.pos,
          normals: []
        }
        vertexmap[vertextag] = obj
      }
      obj.normals.push(polygon.plane.normal)
    })
  })

  // and build spheres at each vertex
  // We will try to set the x and z axis to the normals of 2 planes
  // This will ensure that our sphere tesselation somewhat matches 2 planes
  for (let vertextag in vertexmap) {
    let vertexobj = vertexmap[vertextag]
    // use the first normal to be the x axis of our sphere:
    let xaxis = vertexobj.normals[0].unit()
    // and find a suitable z axis. We will use the normal which is most perpendicular to the x axis:
    let bestzaxis = null
    let bestzaxisorthogonality = 0
    for (let i = 1; i < vertexobj.normals.length; i++) {
      let normal = vertexobj.normals[i].unit()
      let cross = xaxis.cross(normal)
      let crosslength = cross.length()
      if (crosslength > 0.05) {
        if (crosslength > bestzaxisorthogonality) {
          bestzaxisorthogonality = crosslength
          bestzaxis = normal
        }
      }
    }
    if (!bestzaxis) {
      bestzaxis = xaxis.randomNonParallelVector()
    }
    let yaxis = xaxis.cross(bestzaxis).unit()
    let zaxis = yaxis.cross(xaxis)
    let _sphere = CSG.sphere({
      center: vertexobj.pos,
      radius: radius,
      resolution: resolution,
      axes: [xaxis, yaxis, zaxis]
    })
    result = result.unionSub(_sphere, false, false)
  }

  return result
}


// Expand the path to a CAG, then expand the CAG shell (with a circle)
const expandPath2 = function (options, path) {
  const CAG = require('../api/CAG') // FIXME, circular dependency !!

  let width = parseOptionAsFloat(options, 'width', 1.0)
  let resolution = parseOptionAsInt(options, 'resolution', defaultResolution2D)

  let radius = width / 2;

  const oldpoints = path.toPoints() // Vector3[]
  let numpoints = oldpoints.length
  let startindex = 0
  if (path.isClosed() && (numpoints > 2)) startindex = -1

  let sides = []
  let prevvertex
  for (let i = startindex; i < numpoints; i++) {
    let pointindex = i
    if (pointindex < 0) pointindex = numpoints - 1
    let point = oldpoints[pointindex] // Vector3
    let vertex = new Vertex2(new Vector2(point.x, point.y))
    if (i > startindex) {
      sides.push(new Side(prevvertex, vertex))
    }
    prevvertex = vertex
  }
  let newcag = CAG.fromSides(sides)
  newcag = newcag.expand(radius, resolution)
  return newcag
}

module.exports = {
  expand,
  contract,
  expandPath2,
  expandedShellOfCAG,
  expandedShellOfCCSG
}
