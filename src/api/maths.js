// -- Math functions (360 deg based vs 2pi)
function sin (a) {
  return Math.sin(a / 360 * Math.PI * 2)
}

function cos (a) {
  return Math.cos(a / 360 * Math.PI * 2)
}

function asin (a) {
  return Math.asin(a) / (Math.PI * 2) * 360
}

function acos (a) {
  return Math.acos(a) / (Math.PI * 2) * 360
}

function tan (a) {
  return Math.tan(a / 360 * Math.PI * 2)
}

function atan (a) {
  return Math.atan(a) / (Math.PI * 2) * 360
}

function atan2 (a, b) {
  return Math.atan2(a, b) / (Math.PI * 2) * 360
}

function ceil (a) {
  return Math.ceil(a)
}

function floor (a) {
  return Math.floor(a)
}

function abs (a) {
  return Math.abs(a)
}

function min (a, b) {
  return a < b ? a : b
}

function max (a, b) {
  return a > b ? a : b
}

function rands (min, max, vn, seed) {
  // -- seed is ignored for now, FIX IT (requires reimplementation of random())
  //    see http://stackoverflow.com/questions/424292/how-to-create-my-own-javascript-random-number-generator-that-i-can-also-set-the
  var v = new Array(vn)
  for (var i = 0; i < vn; i++) {
    v[i] = Math.random() * (max - min) + min
  }
}

function log (a) {
  return Math.log(a)
}

function pow (a, b) {
  return Math.pow(a, b)
}

function sign (a) {
  return a < 0 ? -1 : (a > 1 ? 1 : 0)
}

function sqrt (a) {
  return Math.sqrt(a)
}

function round (a) {
  return floor(a + 0.5)
}

module.exports = {
  sin,
  cos,
  asin,
  acos,
  tan,
  atan,
  atan2,
  ceil,
  floor,
  abs,
  min,
  max,
  rands,
  log,
  pow,
  sign,
  sqrt,
  round
}
