const Matrix4x4 = require('../math/Matrix4x4')
const OrthoNormalBasis = require('../math/OrthoNormalBasis')
const Plane = require('../math/Plane')

const flatten = require('../utils/flatten')

// the underlying geometry
const geom2 = require('../geom2')

// the extended operations of this object, beyond those of the geometry
const toBinaryDataBuffers = require('../ops-geom2/toBinaryDataBuffers')
const toOutlines = require('../ops-geom2/toOutlines')
const calculateArea = require('../ops-geom2/calculateArea')
const calculateBounds = require('../ops-geom2/calculateBounds')
const intersect = require('../ops-geom2/intersect')
const subtract = require('../ops-geom2/subtract')
const union = require('../ops-geom2/union')

const jscad = require('./jscad')

/**
 * Class CAG
 * Holds a solid 2D geometry consisting of a number of sides.
 * Where each side is a line segment as defined by two points.
 * @constructor
 */
let CAG = function (geometry) {
  if (geometry === undefined) {
    this.geometry = geom2.create()
  } else {
    this.geometry = geometry
  }
}

CAG.prototype = {

  union: function (...cags) {
    cags = flatten(cags)
    let geometries = [this.geometry]
    cags.forEach((cag) => {
      geometries.push(cag.geometry)
    })
    const result = union(geometries)
    return new CAG(result)
  },

  subtract: function (...cags) {
    cags = flatten(cags)
    let geometries = [this.geometry]
    cags.forEach((cag) => {
      geometries.push(cag.geometry)
    })
    const result = subtract(geometries)
    return new CAG(result)
  },

  intersect: function (...cags) {
    cags = flatten(cags)
    let geometries = [this.geometry]
    cags.forEach((cag) => {
      geometries.push(cag.geometry)
    })
    const result = intersect(geometries)
    return new CAG(result)
  },

  // ALIAS !
  transform: function (matrix) {
    let newgeometry = geom2.transform(matrix, this.geometry)
    return new CAG(newgeometry)
  },

  // ALIAS !
  flipped: function () {
    let newgeometry = geom2.reverse(this.geometry)
    return new CAG(newgeometry)
  },

  // Use the JSCAD API in order to process options
  center: function (axes) {
    let newgeometry = jscad.center({axes: axes}, this.geometry)
    return new CAG(newgeometry)
  },

  // Use the JSCAD API in order to process options
  expand: function (radius, resolution) {
    let newgeometry = jscad.expand({radius: radius, resolution: resolution}, this.geometry)
    return new CAG(newgeometry)
  },

  // Use the JSCAD API in order to process options
  contract: function (radius, resolution) {
    let newgeometry = jscad.contract({radius: radius, resolution: resolution}, this.geometry)
    return new CAG(newgeometry)
  },

  // ALIAS !
  area: function () {
    return calculateArea(this.geometry)
  },

  // ALIAS !
  getBounds: function () {
    return calculateBounds(this.geometry)
  },

  // ALIAS !
  isSelfIntersecting: function () {
    return geom2.isSelfIntersecting(this.geometry)
  },

  // ALIAS !
  toPoints: function () {
    return geom2.toPoints(this.geometry)
  },

  // ALIAS !
  toSides: function () {
    return geom2.toSides(this.geometry)
  },

  // ALIAS !
  toCompactBinary: function () {
    return toBinaryDataBuffers({'class': 'CAG'}, this.geometry)
  },

  // ALIAS !
  toString: function () {
    let result = 'CAG: ' + geom2.toString(this.geometry)
    return result
  },

  // Use the JSCAD API in order to process options
  extrudeInOrthonormalBasis: function (orthonormalbasis, depth, options) {
    options = options || {}
    options.offset = [0,0,depth]
    options.orthonormalbasis = orthonormalbasis

    let newgeometry = jscad.extrudeInOrthonormalBasis(options, this.geometry)
    const CSG = require('../api/CSG')
    return new CSG(newgeometry)
  },

  // ALIAS !
  extrudeInPlane: function (axis1, axis2, depth, options) {
    let basis = OrthoNormalBasis.GetCartesian(axis1, axis2)
    return this.extrudeInOrthonormalBasis(basis, depth, options)
  },

  // Use the JSCAD API in order to process options
  extrude: function (options) {
    let newgeometry = jscad.extrude(options, this.geometry)
    const CSG = require('../api/CSG')
    return new CSG(newgeometry)
  },

  // Use the JSCAD API in order to process options
  rotateExtrude: function (options) {
    let newgeometry = jscad.extrudeRotate(options, this.geometry)
    const CSG = require('../api/CSG')
    return new CSG(newgeometry)
  },

  // ALIAS !
  getOutlinePaths: function () {
    let outlines = toOutlines({}, this.geometry)
    const Path2 = require('../api/Path2')
    return outlines.map((outline) => {
      return new Path2.fromPoints(outline, true) // outlines are closed paths
    })
  },

  mirrored: function (plane) {
    return this.transform(Matrix4x4.mirroring(plane))
  },

  mirroredX: function () {
    let plane = Plane.fromNormalAndPoint([1, 0, 0], [0, 0, 0])
    return this.mirrored(plane)
  },

  mirroredY: function () {
    let plane = Plane.fromNormalAndPoint([0, 1, 0], [0, 0, 0])
    return this.mirrored(plane)
  },

  mirroredZ: function () {
    let plane = Plane.fromNormalAndPoint([0, 0, 1], [0, 0, 0])
    return this.mirrored(plane)
  },

  translate: function (v) {
    return this.transform(Matrix4x4.translation(v))
  },

  scale: function (f) {
    return this.transform(Matrix4x4.scaling(f))
  },

  rotateX: function (deg) {
    return this.transform(Matrix4x4.rotationX(deg))
  },

  rotateY: function (deg) {
    return this.transform(Matrix4x4.rotationY(deg))
  },

  rotateZ: function (deg) {
    return this.transform(Matrix4x4.rotationZ(deg))
  },

  rotate: function (rotationCenter, rotationAxis, degrees) {
    return this.transform(Matrix4x4.rotationSCAD(rotationCenter, rotationAxis, degrees))
  },

  rotateEulerAngles: function (alpha, beta, gamma, position) {
    return this.transform(Matrix4x4.rotateEulerAngles(alpha, beta, gamma, position))
  }
}

module.exports = CAG
