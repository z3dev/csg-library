const test = require('ava')

const circle = require('./circle')

const geom2 = require('../../geom2')

test('circle (defaults)', t => {
  // NOTE: first point in geometry becomes last point in point array
  const exp = [
    [ 1, 0 ],
    [ 0.30902, 0.95106 ],
    [ -0.80902, 0.58779 ],
    [ -0.80902, -0.58779 ],
    [ 0.30902, -0.95106 ]
  ]
  const geometry = circle()
  const obs = geom2.toPoints(geometry)

  t.deepEqual(obs.length, 5)
  t.deepEqual(obs, exp)
})

test('circle (scalar radius)', t => {
  // NOTE: first point in geometry becomes last point in point array
  const exp = [
    [ 10, 0 ],
    [ 3.09017, 9.51057 ],
    [ -8.09017, 5.87785 ],
    [ -8.09017, -5.87785 ],
    [ 3.09017, -9.51057 ]
  ]
  const geometry = circle(10)
  const obs = geom2.toPoints(geometry)

  t.deepEqual(obs.length, 5)
  t.deepEqual(obs, exp)
})

test('circle (custom radius, object as parameter)', t => {
  // NOTE: first point in geometry becomes last point in point array
  const exp = [
    [ 10, 0 ],
    [ 3.09017, 9.51057 ],
    [ -8.09017, 5.87785 ],
    [ -8.09017, -5.87785 ],
    [ 3.09017, -9.51057 ]
  ]
  const geometry = circle({r: 10})
  const obs = geom2.toPoints(geometry)

  t.deepEqual(obs.length, 5)
  t.deepEqual(obs, exp)
})

test('circle (custom radius, custom resolution, object as parameter)', t => {
  // NOTE: first point in geometry becomes last point in point array
  const exp = [
    [ 10, 0 ],
    [ 8.66025, 5 ],
    [ 5, 8.66025 ],
    [ 0, 10 ],
    [ -5, 8.66025 ],
    [ -8.66025, 5 ],
    [ -10, 0 ],
    [ -8.66025, -5 ],
    [ -5, -8.66025 ],
    [ -0, -10 ],
    [ 5, -8.66025 ],
    [ 8.66025, -5 ]
  ]
  const geometry = circle({r: 10, fn: 12})
  const obs = geom2.toPoints(geometry)

  t.deepEqual(obs.length, 12)
  t.deepEqual(obs, exp)
})
