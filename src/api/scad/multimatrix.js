const flatten = require('../../utils/flatten')

const Matrix4x4 = require('../../math/Matrix4x4')

const jscad = require('../jscad')

// FIXME OPENSCAD INTERFACE... See api/jscad/transform.js for the implementation
// FIXME OPENSCAD INTERFACE... wow... that's some name

/**
 * Transform the given object(s) using the given matrix
 * @param {Array} matrix - the 4x4 matrix to apply as a single transform
 * @param {Object|Array} objects - the objects(s) to transform
 * @return {Object|Array} the transformed object(s)
 *
 * @example
 * let elements = [[1, 0, 0, 0], [0 ,1 , 0.7, 0], [0, 0, 1, 0], [0 , 0, 0, 1] ]
 * let newsphere = multimatrix(elements, sphere())
 */
const multimatrix = function (elements, ...objects) {
  if (!Array.isArray(elements)) throw new Error('wrong arguments for matrix elements')
  elements = flatten(elements)
  if (elements.length !== 16) throw new Error('matrix elements must contain exactly 16 values')

  let matrix = new Matrix4x4(elements)

  return jscad.transform(matrix, objects)
}

module.exports = multimatrix
