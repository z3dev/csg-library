const {parseOptionAs3DVector} = require('../optionParsers')

const jscad = require('../jscad')

// FIXME OPENSCAD INTERFACE... See api/jscad/translate.js for the implementation

/**
 * Translate the given object(s) using the given options (if any)
 * @param {Object} options - options for translate
 * @param {Array} options.v=[0,0,0] - offsets of which to translate the object
 * @param {Object|Array} objects - the objects(s) to translate
 * @return {Object|Array} the translated object(s)
 *
 * @example
 * let newsphere = translate([5,0,10], sphere())
 * let newsphere = translate({v: [5,0,10]}, sphere())
 */
const translate = function (options, ...objects) {
  options = options || {}

  let offsets = [0, 0, 0]
  if (Array.isArray(options)) {
    offsets = options
  } else {
    let v = parseOptionAs3DVector(options, 'v', [0, 0, 0])
    offsets = v
  }

  // convert OPENSCAD to JSCAD options
  options = {
    offsets: offsets
  }

  return jscad.translate(options, objects)
}

module.exports = translate
