// create a postive or a negative offset from a 2D geometry
// - create a set of 2D lines from the sides
// - transpose each line by offset
// - create a set of points from the intersection between lines
// - return a new 2D geometry
