const flatten = require('../../utils/flatten')

const jscad = require('../jscad')

// FIXME OPENSCAD INTERFACE... See api/jscad/extrude.js for the implementation
// FIXME OPENSCAD INTERFACE... SCAD uses LEFT HAND RULE for rotation of the twist. LAME!!!

/** 
 * Linear extrude the given geometry using the given options (if any).
 * @param {Object} [options] - options for construction
 * @param {Float} [options.height=100] - height of the extruded shape
 * @param {Integer} [options.twist=0] - twist (angle in degrees) of the extrusion (LEFT HAND RULE)
 * @param {Integer} [options.slices=7] - number of intermediary steps/slices
 * @param {Boolean} [options.center=false] - whether to center extrusion or not
 * @param {Float} [options.scale=1] - scale of the shape during extrusion
 * @param {Object|Array} objects - the objects(s) to extrude
 * @returns {CSG} new extruded shape
 *
 * @example
 * let revolved = linear_extrude({height: 10}, square())
 */
const linear_extrude = (options, ...objects) => {
  const defaults = {
    height: 100,
    twist: 0,
    slices: 7,
    center: false,
    scale: 1
  }
  const {height, twist, slices, center, scale} = Object.assign({}, defaults, options)

  if (scale !== 1) {
    throw new Error('scaling an object during extrude is not implemented')
  }

  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  // convert OPENSCAD to JSCAD options
  options = {
    // for extrude
    offset: [0, 0, height], // extrusion about Z only
    twistangle: -twist, // LEFT HAND RULE
    twiststeps: slices,
    // for center
    axes: [false, false, true], // center along Z axis only
    center: [0, 0, 0]
  }

  const results = objects.map((object) => {
    let extruded = jscad.extrude(options, object)
    if (center === true) extruded = jscad.center(options, extruded)
    return extruded
  })
  return results.length === 1 ? results[0] : results
}

module.exports = linear_extrude
