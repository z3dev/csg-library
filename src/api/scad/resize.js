const flatten = require('../../utils/flatten')

const {parseOptionAs3DVector} = require('../optionParsers')

const jscad = require('../jscad')

// FIXME OPENSCAD INTERFACE... See api/jscad/scale.js for the implementation
// FIXME OPENSCAD INTERFACE... REFACTORING AT IT"S BEST

/**
 * Resize the given object(s) using the given options (if any)
 * @param {Object} options - options for resize
 * @param {Array} options.newsize=[0,0,0] - sizes of the new object
 * @param {Array} options.auto=[true,true,true] - auto-resize X, Y, Z dimensions
 * @param {Object|Array} objects - the objects(s) to resize
 * @return {Object|Array} the resized object(s)
 *
 * @example
 * let newsphere = resize([5,0,10], sphere())
 * let newsphere = resize({newsize: [5,0,10]}, sphere())
 */
const resize = function (options, ...objects) {
  options = options || {}

  let newsizes = [0, 0, 0]
  let auto = [false, false, false]
  if (Array.isArray(options)) {
    newsizes = options
  } else {
    let option = parseOptionAs3DVector(options, 'newsizes', [0, 0, 0])
    newsizes[0] = option.x
    newsizes[1] = option.y
    newsizes[2] = option.z
    if ('auto' in options) {
      if (Array.isArray(options.auto)) {
        auto = options.auto
      } else {
      // UG.. SCAD supports a single boolean option
        auto[0] = options.auto
        auto[1] = options.auto
        auto[2] = options.auto
      }
    }
  }

  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  const results = objects.map(function (object) {
    let bounds = jscad.calculateBounds(object)
    let sizeX = bounds[1][0] - bounds[0][0]
    let sizeY = bounds[1][1] - bounds[0][1]
    let sizeZ = bounds[1][2] - bounds[0][2]
    let currentsizes = [sizeX, sizeY, sizeZ]
 
    // calculate initial X/Y/Z factors (if size is provided)
    let autofactor = 1
    let factors = newsizes.map((size, i) => {
      if (currentsizes[i] === 0) return 0
      let factor = newsizes[i] / currentsizes[i]
      if (newsizes[i] !== 0) autofactor = factor // the LAST factor is also the autofactor
      return factor
    })
    // fill in missing factors
    factors = factors.map((factor, i) => {
      if (factor !== 0) return factor
      if (auto[i]) {
        return autofactor
      }
      return 1
    })
    // scale accordingly to the factors
    options = {
      factors: factors
    }
    return jscad.scale(options, object)
  })
  return results.length === 1 ? results[0] : results
}

module.exports = resize
