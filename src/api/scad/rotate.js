const Matrix4x4 = require('../../math/Matrix4x4')

const {parseOptionAs3DVector} = require('../optionParsers')

const jscad = require('../jscad')

// FIXME OPENSCAD INTERFACE... See api/jscad/circle.js for the implementation
// FIXME OPENSCAD INTERFACE... can anyone say "OVERLOAD"? horrible interface

/**
 * Rotate the given object(s) using OPENSCAD based options.
 * If a single number provided for options is the same as a=[0,0,angle].
 * @param {Object} [options] - options for rotation
 * @param {Number} [options.a=[0,0,0]] - angle of rotations about X, Y, Z
 * @param {Number} [options.v=[0,0,1]] - axis of rotation
 * @param {Object|Array} objects - the objects(s) to rotate
 * @return {Object|Array} the rotated object(s)
 *
 * @example
 * let rotatedSphere = rotate([0.2,15,1], sphere())
 */
const rotate = (options, ...objects) => {
  options = options || {}

  let angles = [0, 0, 0]
  let axis = [0, 0, 0]
  let mode = 1 // XYZ-axis rotation, 2 - Arbitrary axis rotation
  if (typeof options === 'number') {
    angles[2] = options // Z-axis rotation
  } else {
    if (Array.isArray(options)) {
      if (options.length > 0) angles[0] = options[0]
      if (options.length > 1) angles[1] = options[1]
      if (options.length > 2) angles[2] = options[2]
    } else {
      a = parseOptionAs3DVector(options, 'a', [0, 0, 0])
      angles[0] = a.x
      angles[1] = a.y
      angles[2] = a.z
      if ('v' in options) {
        axis = parseOptionAs3DVector(options, 'v', [0, 0, 0])
        mode = 2 // 2 - Arbitrary axis rotation
      }
    }
  }

  if (mode === 1) {
  // create XYZ transform as per OPENSCAD
    let transformX = Matrix4x4.rotationX(angles[0])
    let transformY = Matrix4x4.rotationY(angles[1])
    let transformZ = Matrix4x4.rotationZ(angles[2])
    let transform = transformX.multiply(transformY).multiply(transformZ)
    return jscad.transform(transform, objects)
  }
  if (mode === 2) {
    throw new Error('PLEASE IMPLEMENT THIS MODE')
    return jscad.rotate(options, objects)
  }
}

module.exports = rotate
