/** create a minkowski sum of the given shapes
 * @param {Object(s)|Array} objects either a single or multiple CSG/CAG objects to create a hull around
 * @returns {CSG} new CSG object , mirrored
 *
 * @example
 * let hulled = hull(rect(), circle())
 */
function minkowski () {
  throw new Error('minkowski is not implemented')
}

module.exports = minkowski
