const {parseOptionAs3DVector} = require('../optionParsers')

const jscad = require('../jscad')

// FIXME OPENSCAD INTERFACE... See api/jscad/scale.js for the implementation

/**
 * Translate the given object(s) using the given options (if any)
 * @param {Object} options - options for scale
 * @param {Array} options.v=[0,0,0] - factors of which to scale the object
 * @param {Object|Array} objects - the objects(s) to scale
 * @return {Object|Array} the scaled object(s)
 *
 * @example
 * let newsphere = scale([5,0,10], sphere())
 * let newsphere = scale({v: [5,0,10]}, sphere())
 */
const scale = function (options, ...objects) {
  options = options || {}

  let factors = [1, 1, 1]
  if (Array.isArray(options)) {
    factors = options
  } else {
    let v = parseOptionAs3DVector(options, 'v', [1, 1, 1])
    factors = v
  }

  // convert OPENSCAD to JSCAD options
  options = {
    factors: factors
  }

  return jscad.scale(options, objects)
}

module.exports = scale
