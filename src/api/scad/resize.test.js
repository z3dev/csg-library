const test = require('ava')

const path = require('../../path')
const geom2 = require('../../geom2')

const resize = require('./resize')

test('resize (defaults): scaling of a path produces expected changes to points', t => {
  let line = path.fromPoints({}, [[0, 1, 0], [1, 0, 0]])

  line = resize({}, line)
  let obs = path.toPoints(line)
  let exp = [ { _x: 0, _y: 1, _z: 0 }, { _x: 1, _y: 0, _z: 0 } ]

  t.deepEqual(obs, exp)
})

test('resize: resizing of a path produces expected changes to points', t => {
  let line = path.fromPoints({}, [[0, 1, 0], [1, 0, 0]])

  line = resize([2,3,5], line)
  let obs = path.toPoints(line)
  let exp = [ { _x: 0, _y: 3, _z: 0 }, { _x: 2, _y: 0, _z: 0 } ]

  t.deepEqual(obs, exp)
})

test('resize: resizing of a geom2 produces expected changes to points', t => {
  let geometry = geom2.fromPoints([[0, 0], [1, 0], [0, 1]])

  geometry = resize({newsizes: [0,3,6], auto: true}, geometry)
  let obs = geom2.toPoints(geometry)
  let exp = [[0, 0], [3, 0], [0, 3]  ]

  t.deepEqual(obs, exp)
})
