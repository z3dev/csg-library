const test = require('ava')

const path = require('../../path')
const geom2 = require('../../geom2')

const mirror = require('./mirror')

test('mirror: mirroring of a path produces expected changes to points', t => {
  let line = path.fromPoints({}, [[0, 1, 0], [1, 0, 0]])

  line = mirror([1,0,0], line) // about the X axis
  let obs = path.toPoints(line)
  let exp = [ { _x: 0, _y: 1, _z: 0 }, { _x: -1, _y: 0, _z: 0 } ]

  t.deepEqual(obs, exp)
})

test('mirror: mirroring of a geom2 produces expected changes to points', t => {
  let geometry = geom2.fromPoints([[0, 0], [1, 0], [0, 1]])

  geometry = mirror({v: [0,1,0]}, geometry) // about the Y axis
  let obs = geom2.toPoints(geometry)
  let exp = [[0, 0], [1, 0], [0, -1]  ]

  t.deepEqual(obs, exp)
})
