const {parseOptionAs3DVector} = require('../optionParsers')

const jscad = require('../jscad')

// FIXME OPENSCAD INTERFACE... See api/jscad/mirror.js for the implementation
// FIXME OPENSCAD INTERFACE... mirror is always about the point [0,0,0]

/**
 * Translate the given object(s) using the given options (if any)
 * @param {Object} options - options for mirror
 * @param {Array} options.v=[0,0,0] - factors of which to mirror the object
 * @param {Object|Array} objects - the objects(s) to mirror
 * @return {Object|Array} the mirrored object(s)
 *
 * @example
 * let newsphere = mirror([5,0,10], sphere())
 */
const mirror = function (options, ...objects) {
  options = options || {}

  let normal = [0, 0, 0]
  if (Array.isArray(options)) {
    normal = options
  } else {
    let v = parseOptionAs3DVector(options, 'v', [1, 1, 1])
    normal[0] = v.x
    normal[1] = v.y
    normal[2] = v.z
  }

  // convert OPENSCAD to JSCAD options
  options = {
    normal: normal,
    origin: [0, 0, 0] // always about the point [0,0,0]
  }

  return jscad.mirror(options, objects)
}

module.exports = mirror
