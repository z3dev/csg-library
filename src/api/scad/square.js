const {parseOptionAsBool} = require('../optionParsers')

const jscad = require('../jscad')

// FIXME OPENSCAD INTERFACE... See api/jscad/rectangle.js for the implementation
// FIXME OPENSCAD INTERFACE... squares are NOT centered by default

/** Construct a square.
 * @param {Object} [options] - options for construction
 * @param {Vector2} [options.size=[0,0]] - size of square
 * @param {Vector2} [options.center=false] - center or not
 * @returns {geom2} new 2D geometry
 */
const square = function (options) {
  options = options || {}

  let size = [1,1] // default

  if ('number' === typeof(options)) options = {size: [options,options]}

  if (Array.isArray(options)) {
    size = options
  } else {
    if ('size' in options) {
      size = options.size
      if (!Array.isArray(size)) {
        size = [size, size]
      }
    }
  }
  let isCentered = parseOptionAsBool(options, 'center', false)

  // convert OPENSCAD to JSCAD options
  let radius = [size[0]/2, size[1]/2]
  let center = [0, 0]
  if (isCentered === false) {
    center = radius
  }

  options = {
    center: center,
    radius: radius
  }

  return jscad.rectangle(options)
}

module.exports = square
