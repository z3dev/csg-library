const {parseOptionAsFloat, parseOptionAsInt} = require('../optionParsers')

const jscad = require('../jscad')

// FIXME OPENSCAD INTERFACE... See api/jscad/circle.js for the implementation
// FIXME OPENSCAD INTERFACE... so 'center' is not supported

/**
 * Construct a circle using OPENSCAD based options.
 * @param {Object} [options] - options for construction
 * @param {Number} [options.r=1] - radius of circle
 * @param {Number} [options.d=2] - diameter of circle
 * @param {Number} [options.fn=5] - number of sides per 360 rotation
 * @returns {geom2} new 2D geometry
 */
const circle = (options) => {
  options = options || {}

  if ('number' === typeof(options)) options = {r: options}

  if ('r' in options && 'd' in options) {
    throw new Error('invalid parameters: use r or d, not both')
  }

  let radius = 0
  if ('d' in options) {
    let diameter = parseOptionAsFloat(options, 'd', 2)
    radius = diameter / 2
  } else {
    radius = parseOptionAsFloat(options, 'r', 1)
  }
  let resolution = parseOptionAsInt(options, 'fn', 5)

  // convert OPENSCAD to JSCAD options
  options = {
    center: [0, 0],
    radius: radius,
    resolution: resolution
  }

  return jscad.circle(options)
}

module.exports = circle
