const test = require('ava')

const path = require('../../path')
const geom2 = require('../../geom2')

const translate = require('./translate')

test('translate: translating of a path produces expected changes to points', t => {
  let line = path.fromPoints({}, [[0, 0, 0], [1, 0, 0]])

  line = translate([1,1], line)
  let obs = path.toPoints(line)
  let exp = [ { _x: 1, _y: 1, _z: 0 }, { _x: 2, _y: 1, _z: 0 } ]

  t.deepEqual(obs, exp)
})

test('translate: translating of a geom2 produces expected changes to points', t => {
  let geometry = geom2.fromPoints([[0, 0], [1, 0], [0, 1]])

  geometry = translate({v: [1,1]}, geometry)
  let obs = geom2.toPoints(geometry)
  let exp = [[1, 1], [2, 1], [1, 2]  ]

  t.deepEqual(obs, exp)
})
