const jscad = require('../jscad')

// FIXME OPENSCAD INTERFACE... See api/jscad/extrudeRotate.js for the implementation

/** 
 * Rotate extrude the given geometry using the given options (if any).
 * @param {Object} [options] - options for construction
 * @param {Float} [options.angle=360] - angle of the extrusion, in degrees
 * @param {Integer} [options.fn=10] - number of fragments used by the extrusion
 * @param {Object|Array} objects - the objects(s) to extrude
 * @returns {geom3} new extruded shape
 *
 * @example
 * let revolved = rotate_extrude({angle: 270}, square())
 */
const rotate_extrude = (options, ...objects) => {
  const defaults = {
    angle: 360,
    fn: 10
  }
  const {angle, fn} = Object.assign({}, defaults, options)

  if (fn < 3) fn = 3

  // convert OPENSCAD to JSCAD options
  let resolution = Math.floor(Math.max((angle / (360 / fn)), 1))
  options = {
    angle: angle,
    startangle: 0,
    overflow: 'cap',
    resolution: resolution
  }
  return jscad.extrudeRotate(options, objects)
}

module.exports = rotate_extrude
