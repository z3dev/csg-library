module.exports = {
  circle: require('circle'),
  lookup: require('lookup'),
  minkowski: require('./minkowski')
  mirror: require('mirror'),
  multimatrix: require('multimatrix'),
  //offset: require('offset'),
  polygon: require('polygon'),
  resize: require('resize'),
  rotate: require('rotate'),
  scale: require('scale'),
  square: require('square'),
  translate: require('translate')
}
