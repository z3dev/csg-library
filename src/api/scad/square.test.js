const test = require('ava')

const square = require('./square')

const geom2 = require('../../geom2')

test('square (defaults)', t => {
  // NOTE: first point in geometry becomes last point in point array
  const exp = [[0.00000, 0.00000], [1.00000, 0.00000], [1.00000, 1.00000], [0.00000, 1.00000]]
  const geometry = square()
  const obs = geom2.toPoints(geometry)

  t.deepEqual(obs.length, 4)
  t.deepEqual(obs, exp)
})

test('square (scalar radius)', t => {
  // NOTE: first point in geometry becomes last point in point array
  const exp = [[0, 0], [10, 0], [10, 10], [0, 10]]
  const geometry = square(10)
  const obs = geom2.toPoints(geometry)

  t.deepEqual(obs.length, 4)
  t.deepEqual(obs, exp)
})

test('square (custom size, 2d array parameter)', t => {
  // NOTE: first point in geometry becomes last point in point array
  const exp = [[0, 0], [2, 0], [2, 3], [0, 3]]
  const geometry = square([2, 3])
  const obs = geom2.toPoints(geometry)

  t.deepEqual(obs.length, 4)
  t.deepEqual(obs, exp)
})

test('square (custom size, size object parameter)', t => {
  // NOTE: first point in geometry becomes last point in point array
  const exp = [[0, 0], [2, 0], [2, 3], [0, 3]]
  const geometry = square({size: [2, 3]})
  const obs = geom2.toPoints(geometry)

  t.deepEqual(obs.length, 4)
  t.deepEqual(obs, exp)
})

test('square (default size, centered)', t => {
  // NOTE: first point in geometry becomes last point in point array
  const exp = [[-0.5, -0.5], [0.5, -0.5], [0.5, 0.5], [-0.5, 0.5]]
  const geometry = square({center: true})
  const obs = geom2.toPoints(geometry)

  t.deepEqual(obs.length, 4)
  t.deepEqual(obs, exp)
})

test('square (custom size, centered)', t => {
  // NOTE: first point in geometry becomes last point in point array
  const exp = [[-1, -1.5], [1, -1.5], [1, 1.5], [-1, 1.5]]
  const geometry = square({size: [2, 3], center: true})
  const obs = geom2.toPoints(geometry)

  t.deepEqual(obs.length, 4)
  t.deepEqual(obs, exp)
})
