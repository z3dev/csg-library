const test = require('ava')

const path = require('../../path')
const geom2 = require('../../geom2')

const rotate = require('./rotate')

test('rotate: rotating of a path produces expected changes to points', t => {
  let geometry = path.fromPoints({}, [[0, 1, 0], [1, 0, 0]])

  geometry = rotate({angles: [0,0,90]}, geometry)
  let obs = path.toPoints(geometry)
  let exp = [ { _x: -1, _y: 0, _z: 0 }, { _x: 0, _y: 1, _z: 0 } ]

  t.deepEqual(obs, exp)
})

test('rotate: rotating of a geom2 produces expected changes to points', t => {
  let geometry = geom2.fromPoints([[0, 0], [1, 0], [0, 1]])

  geometry = rotate({angles: [0,0,-90]}, geometry)
  let obs = geom2.toPoints(geometry)
  let exp = [[0, 0], [0, -1], [1, 0]  ]

  t.deepEqual(obs, exp)
})
