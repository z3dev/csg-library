const test = require('ava')

const rectangle = require('./rectangle')

const geom2 = require('../../geom2')

test('rectangle (defaults)', t => {
  // NOTE: first point in geometry becomes last point in point array
  const exp = [
    [-1.00000, -1.00000],
    [1.00000, -1.00000],
    [1.00000, 1.00000],
    [-1.00000, 1.00000]
  ]
  const geometry = rectangle()
  const obs = geom2.toPoints(geometry)

  t.deepEqual(obs.length, 4)
  t.deepEqual(obs, exp)
})
