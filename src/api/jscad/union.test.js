const test = require('ava')

const path = require('../../path')
const geom2 = require('../../geom2')

const circle = require('./circle')
const rectangle = require('./rectangle')
const union = require('./union')

//test('union: union of a path produces expected changes to points', t => {
//  let geometry = path.fromPoints({}, [[0, 1, 0], [1, 0, 0]])
//
//  geometry = union({normal: [1, 0, 0]}, geometry)
//  let obs = path.toPoints(geometry)
//  let exp = [ { _x: 0, _y: 1, _z: 0 }, { _x: -1, _y: 0, _z: 0 } ]
//
//  t.deepEqual(obs, exp)
//})

test('union: union of one or more geom2 objects produces expected geometry', t => {
  let geometry1 = circle({radius: 2, resolution: 8})

  // union of one object
  let result1 = union(geometry1)
  let obs = geom2.toPoints(result1)
  let exp = [
    [ 2, 0 ], [ 1.41421, 1.41421 ], [ 0, 2 ], [ -1.41421, 1.41421 ],
    [ -2, 0 ], [ -1.41421, -1.41421 ], [ 0, -2 ], [ 1.41421, -1.41421 ]
  ]
  t.deepEqual(obs, exp)

  // union of two non-overlapping objects
  let geometry2 = rectangle({radius: [2,2], center: [10,10]})

  let result2 = union(geometry1, geometry2)
  obs = geom2.toPoints(result2)
  exp = [
    [ 2, 0 ],
    [ 1.41421, 1.41421 ],
    [ 0, 2 ],
    [ -1.41421, 1.41421 ],
    [ -2, 0 ],
    [ -1.41421, -1.41421 ],
    [ 0, -2 ],
    [ 8, 12 ],
    [ 8, 8 ],
    [ 12, 8 ],
    [ 12, 12 ],
    [ 1.41421, -1.41421 ]
  ]
  t.deepEqual(obs, exp)

  // union of two partially overlapping objects
  let geometry3 = rectangle({radius: [9,9]})

  let result3 = union(geometry2, geometry3)
  obs = geom2.toPoints(result3)
  exp = [
    [ 12, 12 ],
    [ 8, 12 ],
    [ 9, 8 ],
    [ -9, 9 ],
    [ -9, -9 ],
    [ 9, -9 ],
    [ 8, 9 ],
    [ 12, 8 ]
  ]
  t.deepEqual(obs, exp)

  // union of two completely overlapping objects
  let result4 = union(geometry1, geometry3)
  obs = geom2.toPoints(result4)
  exp = [
    [ -9, -9 ], [ 9, -9 ], [ 9, 9 ], [ -9, 9 ]
  ]
  t.deepEqual(obs, exp)
})
