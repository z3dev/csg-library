const createRectangle = require('../../primitives/rectangle')

const {parseOptionAs2DVector} = require('../optionParsers')

/** Construct a rectangle.
 * @param {Object} [options] - options for construction
 * @param {Vector2} [options.center=[0,0]] - center of rectangle
 * @param {Vector2} [options.radius=[1,1]] - radius of rectangle, width and height
 * @returns {geom2} new 2D geometry
 */
const rectangle = function (options) {
  let center = parseOptionAs2DVector(options, 'center', [0, 0])
  let radius = parseOptionAs2DVector(options, 'radius', [1, 1])

  center = [center.x, center.y]
  radius = [radius.x, radius.y]
  options = {center: center, radius: radius}
  return createRectangle(options)
}

module.exports = rectangle
