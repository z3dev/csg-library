const {defaultResolution2D} = require('../../constants')

const flatten = require('../../utils/flatten')

const path = require('../../path')
const pathexpand = require('../../ops-path/expand')

const geom2 = require('../../geom2')
const geom2expand = require('../../ops-geom2/expand')

const geom3 = require('../../geom3')
const geom3expand = require('../../ops-geom3/expand')

const {parseOptionAsFloat, parseOptionAsInt} = require('../optionParsers')

/**
 * Expand the given object(s) using the given options (if any)
 * @param {Object} options - options for expand
 * @param {Number} options.radius=1 - the radius of the expansion
 * @param {Number} [options.resolution=defaultResolution2D] - number of sides per 360 rotation
 * @param {Object|Array} objects - the objects(s) to expand
 * @return {Object|Array} the expanded object(s)
 *
 * @example
 * let newsphere = expand({radius: 2}, cube({center: [0,0,15], radius: [20, 25, 5]}))
 */
const expand = function (options, ...objects) {
  let radius = parseOptionAsFloat(options, 'radius', 1)
  let resolution = parseOptionAsInt(options, 'resolution', defaultResolution2D)

  // TODO alternative option format: array of [x, y, z]
  // TODO alternative option format: object with x, y, z attributes

  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  options = {radius: radius, resolution: resolution}

  const results = objects.map(function (object) {
    if (path.isA(object)) return pathexpand(options, object)
    if (geom2.isA(object)) return geom2expand(options, object)
    if (geom3.isA(object)) return geom3expand(options, object)
    return object
  })
  return results.length === 1 ? results[0] : results
}

module.exports = expand
