const {defaultResolution3D} = require('../../constants')

const createCylinder = require('../../primitives/cylinder')

const {parseOptionAs3DVector, parseOptionAsFloat, parseOptionAsInt} = require('../optionParsers')

/** Construct a solid cylinder.
 * @param {Object} [options] - options for construction
 * @param {Vector} [options.start=[0,-1,0]] - start point of cylinder
 * @param {Vector} [options.end=[0,1,0]] - end point of cylinder
 * @param {Number} [options.radius=1] - radius of cylinder, must be scalar
 * @param {Number} [options.resolution=defaultResolution3D] - number of polygons per 360 degree revolution
 * @returns {geom3} new 3D geometry
 *
 * @example
 * let cylinder = cylinder({
 *   start: [0, -10, 0],
 *   end: [0, 10, 0],
 *   radius: 10,
 *   resolution: 16
 * });
 */
const cylinder = function (options) {
  options = options || {}

  let start = parseOptionAs3DVector(options, 'start', [0, -1, 0])
  let end = parseOptionAs3DVector(options, 'end', [0, 1, 0])
  let radius = parseOptionAsFloat(options, 'radius', 1) // NOTE: this becomes the default start and end radius
  let endRadius = parseOptionAsFloat(options, 'endRadius', radius)
  let startRadius = parseOptionAsFloat(options, 'startRadius', radius)
  let resolution = parseOptionAsInt(options, 'resolution', defaultResolution3D)
  let endAngle = parseOptionAsFloat(options, 'endAngle', 360)

  endAngle = endAngle > 360 ? endAngle % 360 : endAngle

  if ((endRadius < 0) || (startRadius < 0)) {
    throw new Error('radius must be positive')
  }
  if ((endRadius === 0) && (startRadius === 0)) {
    throw new Error('either startRadius or endRadius must be positive')
  }

  start = [start.x, start.y, start.z]
  end = [end.x, end.y, end.z]
  options = {
    start: start,
    startRadius: startRadius,
    end: end,
    endRadius: endRadius,
    endAngle: endAngle,
    resolution: resolution
  }

  let result = createCylinder(options)

/*
  result.properties.cylinder = new Properties()
  result.properties.cylinder.start = new Connector(s, axisZ.negated(), axisX)
  result.properties.cylinder.end = new Connector(e, axisZ, axisX)
  let cylCenter = s.plus(ray.times(0.5))
  let fptVec = axisX.transform(Matrix4x4.rotationSCAD(s, axisZ, -endAngle / 2)).times((startRadius + endRadius) / 2)
  let fptVec90 = fptVec.cross(axisZ)
  // note this one is NOT a face normal for a cone. - It's horizontal from cyl perspective
  result.properties.cylinder.facepointH = new Connector(cylCenter.plus(fptVec), fptVec, axisZ)
  result.properties.cylinder.facepointH90 = new Connector(cylCenter.plus(fptVec90), fptVec90, axisZ)
*/
  return result
}

module.exports = cylinder
