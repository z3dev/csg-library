const flatten = require('../../utils/flatten')

const path = require('../../path')
const pathBounds = require('../../ops-path/calculateBounds')

const geom2 = require('../../geom2')
const geom2Bounds = require('../../ops-geom2/calculateBounds')

const geom3 = require('../../geom3')
const geom3Bounds = require('../../ops-geom3/calculateBounds')

/**
 * Calculate the min and max bounds of the given object(s),
 * where each bounds is an array of [x,y,z]
 * @param {Object|Array} objects - the objects(s) to center
 * @return {Object|Array} the bounds ([min, max]) for each object
 *
 * @example
 * let bounds = calculateBounds(sphere())
 */
const calculateBounds = function (...objects) {
  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  const results = objects.map(function (object) {
    if (path.isA(object)) return pathBounds(object)
    if (geom2.isA(object)) return geom2Bounds(object)
    if (geom3.isA(object)) return geom3Bounds(object)
    return [0, 0, 0]
  })
  // if there is more than one result, return them all , otherwise a single one
  return results.length === 1 ? results[0] : results
}

module.exports = calculateBounds
