const flatten = require('../../utils/flatten')
const areAllShapesTheSameType = require('../../utils/areAllShapesTheSameType')

const path = require('../../path')

const geom2 = require('../../geom2')
const geom2union = require('../../ops-geom2/union')

const geom3 = require('../../geom3')
//const geom3union = require('../../ops-geom3/union')

/**
 * Return a new geometry representing the total space in the given objects.
 * NOTE: None of the given objects are modified.
 * @param {...geometry} objects - list of objects to union
 * @returns {geometry} new geometry
 * @example
 * let C = union(A, B)
 * @example
 * +-------+            +-------+
 * |       |            |       |
 * |   A   |            |       |
 * |    +--+----+   =   |       +----+
 * +----+--+    |       +----+       |
 *      |   B   |            |       |
 *      |       |            |       |
 *      +-------+            +-------+
 */
const union = (...objects) => {
  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  if (!areAllShapesTheSameType(objects)) {
    throw new Error('only unions of the type are supported')
  }

  let object = objects[0]
  //if (path.isA(object)) return pathunion(matrix, objects)
  if (geom2.isA(object)) return geom2union(objects)
  //if (geom3.isA(object)) return geom3union(objects)
  return object
}

module.exports = union
