const test = require('ava')

const path = require('../../path')
const geom2 = require('../../geom2')
const geom3 = require('../../geom3')

const expand = require('./expand')

test('expand: expanding of a path produces an expected geom2', t => {
  let geometry = path.fromPoints({}, [[0, 10, 0], [10, 0, 0]])

  geometry = expand({radius: 2, resolution: 8}, geometry)
  let obs = geom2.toPoints(geometry)
  let exp = [
    [ 8.58579, -1.41421 ],
    [ 10, -2 ],
    [ 11.41421, -1.41421 ],
    [ -1.41421, 8.58579 ],
    [ 11.41421, 1.41421 ],
    [ 1.41421, 11.41421 ],
    [ 0, 12 ],
    [ -1.41421, 11.41421 ],
    [ -2, 10 ],
    [ 12, 0 ]
  ]

  t.deepEqual(obs, exp)
})

test('expand: expanding of a geom2 produces expected changes to points', t => {
  let geometry = geom2.fromPoints([[-8, -8], [8, -8], [8, 8], [-8, 8]])

  geometry = expand({radius: 2, resolution: 8}, geometry)
  let obs = geom2.toPoints(geometry)
  let exp = [
    [ -8, -10 ],
    [ 10, -8 ],
    [ 8, 10 ],
    [ -8, 10 ],
    [ -9.41421, 9.41421 ],
    [ -10, -8 ],
    [ -9.41421, -9.41421 ],
    [ 8, -10 ],
    [ 9.41421, -9.41421 ],
    [ 10, 8 ],
    [ 9.41421, 9.41421 ],
    [ -10, 8 ]
  ]

  t.deepEqual(obs, exp)
})

test('expand: expanding of a geom3 produces expected changes to polygons', t => {
  let polygonsAsPoints = [
    [[-5,-5,-5],[-5,-5,15],[-5,15,15],[-5,15,-5]],
    [[15,-5,-5],[15,15,-5],[15,15,15],[15,-5,15]],
    [[-5,-5,-5],[15,-5,-5],[15,-5,15],[-5,-5,15]],
    [[-5,15,-5],[-5,15,15],[15,15,15],[15,15,-5]],
    [[-5,-5,-5],[-5,15,-5],[15,15,-5],[15,-5,-5]],
    [[-5,-5,15],[15,-5,15],[15,15,15],[-5,15,15]]
  ]
  let geometry = geom3.fromPoints(polygonsAsPoints)

  geometry = expand({radius: 2, resolution: 8}, geometry)
  let obs = geom3.toPoints(geometry)
  let exp0 = [
    [ -7, -5, -5 ], [ -7, -5, 15 ], [ -7, 15, 15 ], [ -7, 15, -5 ]
  ]
  let exp93 = [
    [ 16.414213562373096, 16, 16 ],
    [ 16.414213562373096, 16.414213562373096, 15 ],
    [ 16.09383632135604, 16.093836321356058, 16.093836321356058 ]
  ]

  t.is(obs.length, 94)
  t.deepEqual(obs[0], exp0)
  t.deepEqual(obs[93], exp93)
})
