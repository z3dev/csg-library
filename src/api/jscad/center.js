const flatten = require('../../utils/flatten')

const calculateBounds = require('./calculateBounds')
const translate = require('./translate')

/**
 * Center the given object(s) using the given options (if any)
 * @param {Object} [options] - options for centering
 * @param {Array} [options.axes=[true,true,true]] - axis of which to center, true or false
 * @param {Array} [options.center=[0,0,0]] - point of which to center the object upon
 * @param {Object|Array} objects - the shape(s) to center
 * @return {Object|Array} objects
 *
 * @example
 * let csg = center({axes: [true,false,false]}, sphere()) // center about the X axis
 */
const center = function (options, ...objects) {
  const defaults = {
    axes: [true, true, true],
    center: [0, 0, 0]
  // TODO : Add addition 'methods' of centering; midpoint, centeriod
  }
  const {axes, center} = Object.assign({}, defaults, options)

  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  const results = objects.map(function (object) {
    let bounds = calculateBounds(object)
    let offsets = [0,0,0]
    if (axes[0]) offsets[0] = center[0] - (bounds[0][0] + ((bounds[1][0] - bounds[0][0]) / 2))
    if (axes[1]) offsets[1] = center[1] - (bounds[0][1] + ((bounds[1][1] - bounds[0][1]) / 2))
    if (axes[2]) offsets[2] = center[2] - (bounds[0][2] + ((bounds[1][2] - bounds[0][2]) / 2))
    return translate({offsets: offsets}, object)
  })
  return results.length === 1 ? results[0] : results
}

module.exports = center
