const Matrix4x4 = require('../../math/Matrix4x4')
const OrthoNormalBasis = require('../../math/OrthoNormalBasis')

const {parseOptionAsBool, parseOptionAs3DVector} = require('../optionParsers')

const extrude = require('./extrude')
const transform = require('./transform')


/**
 * Extrude the given objects upon a specific plane.
 * Giving just a plane is not enough, multiple different extrusions in the same plane would be possible
 * by rotating around the plane's origin. An additional right-hand vector should be specified as well,
 * and this is exactly a OrthoNormalBasis.
 * NOTE: See the extrude function for specific options used when extruding objects.
 * @param {Object} options - options for extrude (See extrude for additional options)
 * @param {Orthonormalbasis} options.orthonormalbasis - characterizes the plane in which to extrude
 * @param {Boolean} [options.symmetrical=true] - extrude symmetrically about the plane
 * @return {Object|Array} the extruded object(s)
 */
const extrudeInOrthonormalBasis = (options, ...objects) => {
  let offset = parseOptionAs3DVector(options, 'offset', [0, 0, 1])
  let symmetrical = parseOptionAsBool(options, 'symmetrical', false)

  if (!('orthonormalbasis' in options)) {
    throw new Error('options must include orthonormalbasis')
  }
  if (!(options.orthonormalbasis instanceof OrthoNormalBasis)) {
    throw new Error('options.orthonomalbasis must be an OrthoNormalBasis')
  }

  // extrude the objects
  // NOTE: Pass all options through for extruding
  let results = extrude(options, objects)

  // reposition symmetrically if necessary
  if (symmetrical === true) {
    let negated = offset.times(0.5).negated()
    results = transform(Matrix4x4.translation(negated), results)
  }

  // reposition the objects to the plane
  let matrix = options.orthonormalbasis.getInverseProjectionMatrix()
  results = transform(matrix, results)

  return results.length === 1 ? results[0] : results
}

module.exports = extrudeInOrthonormalBasis
