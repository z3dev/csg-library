const flatten = require('../../utils/flatten')

const Matrix4x4 = require('../../math/Matrix4x4')
const Plane = require('../../math/Plane')

const path = require('../../path')
const geom2 = require('../../geom2')
//const geom3 = require('../../geom3')

const {parseOptionAs3DVector} = require('../optionParsers')

/**
 * Mirror the given object(s) using the given options (if any)
 * Note: The normal should be given as 90 degrees from the plane origin.
 * @param {Object} options - options for mirror
 * @param {Array} options.origin=[0,0,0] - the origin of the plane
 * @param {Array} options.normal=[0,0,0] - the normal vector of the plane
 * @param {Object|Array} objects - the objects(s) to mirror
 * @return {Object|Array} the mirrored object(s)
 *
 * @example
 * let newsphere = mirror({normal: [0,0,10]}, cube({center: [0,0,15], radius: [20, 25, 5]}))
 */
const mirror = function (options, ...objects) {
  let origin = parseOptionAs3DVector(options, 'origin', [0, 0, 0])
  let normal = parseOptionAs3DVector(options, 'normal', [0, 0, 1]) // Z axis

  // TODO alternative option format: array of [x, y, z]
  // TODO alternative option format: object with x, y, z attributes

  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  const plane = Plane.fromNormalAndPoint(normal.unit(), origin)
  const matrix = Matrix4x4.mirroring(plane)

  const results = objects.map(function (object) {
    if (path.isA(object)) return path.transform(matrix, object)
    if (geom2.isA(object)) return geom2.transform(matrix, object)
    //if (geom3.isA(object)) return geom3.transform(matrix, object)
    return object
  })
  return results.length === 1 ? results[0] : results
}

module.exports = mirror
