const flatten = require('../../utils/flatten')

const Matrix4x4 = require('../../math/Matrix4x4')

const path = require('../../path')
const geom2 = require('../../geom2')
//const geom3 = require('../../geom3')

const {parseOptionAs3DVector} = require('../optionParsers')

/**
 * Scale the given object(s) using the given options (if any)
 * @param {Object} options - options for scale
 * @param {Array} options.factors=[0,0,0] - factors of which to scale the object
 * @param {Object|Array} objects - the objects(s) to scale
 * @return {Object|Array} the scaled object(s)
 *
 * @example
 * let newsphere = scale({factors: [5,0,10]}, sphere())
 */
const scale = function (options, ...objects) {
  let factors = parseOptionAs3DVector(options, 'factors', [1, 1, 1])

  // TODO alternative option format: array of [x, y, z]
  // TODO alternative option format: object with x, y, z attributes

  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  const matrix = Matrix4x4.scaling(factors)

  const results = objects.map(function (object) {
    if (path.isA(object)) return path.transform(matrix, object)
    if (geom2.isA(object)) return geom2.transform(matrix, object)
    //if (geom3.isA(object)) return geom3.transform(matrix, object)
    return object
  })
  return results.length === 1 ? results[0] : results
}

module.exports = scale
