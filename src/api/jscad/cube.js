const createCube = require('../../primitives/cube')

const {parseOptionAs3DVector} = require('../optionParsers')

/** Construct an axis-aligned solid cuboid.
 * @param {Object} [options] - options for construction
 * @param {Vector3} [options.center=[0,0,0]] - center of cube
 * @param {Vector3} [options.radius=[1,1,1]] - radius of cube, single scalar also possible
 * @returns {geom3} new geometry
 *
 * @example
 * let cube = cube({
 *   center: [5, 5, 5],
 *   radius: 5, // scalar radius
 * });
 */
const cube = function (options) {
  options = options || {}

  let center
  let radius
  if (('corner1' in options) || ('corner2' in options)) {
    if (('center' in options) || ('radius' in options)) {
      throw new Error('either give a radius and center parameter, or a corner1 and corner2 parameter')
    }
    const corner1 = parseOptionAs3DVector(options, 'corner1', [0, 0, 0])
    const corner2 = parseOptionAs3DVector(options, 'corner2', [1, 1, 1])
    center = corner1.plus(corner2).times(0.5)
    radius = corner2.minus(corner1).times(0.5)
  } else {
    center = parseOptionAs3DVector(options, 'center', [0, 0, 0])
    radius = parseOptionAs3DVector(options, 'radius', [1, 1, 1])
  }

  if (radius.x <= 0 || radius.y <= 0 || radius.z <= 0) {
    throw new Error('radius must be positive')
  }

  center = [center.x, center.y, center.z]
  radius = [radius.x, radius.y, radius.z]
  options = {
    center: center,
    radius: radius
  }
  const result = createCube(options)

  // add connectors, at center of cube, and center of each face
/*
  result.properties.cube = new Properties()
  result.properties.cube.center = c
  result.properties.cube.facecenters = [
    new Connector(new Vector3(r.x, 0, 0).plus(c), [1, 0, 0], [0, 0, 1]),
    new Connector(new Vector3(-r.x, 0, 0).plus(c), [-1, 0, 0], [0, 0, 1]),
    new Connector(new Vector3(0, r.y, 0).plus(c), [0, 1, 0], [0, 0, 1]),
    new Connector(new Vector3(0, -r.y, 0).plus(c), [0, -1, 0], [0, 0, 1]),
    new Connector(new Vector3(0, 0, r.z).plus(c), [0, 0, 1], [1, 0, 0]),
    new Connector(new Vector3(0, 0, -r.z).plus(c), [0, 0, -1], [1, 0, 0])
  ]
*/
  return result
}

module.exports = cube
