const flatten = require('../../utils/flatten')
const areAllShapesTheSameType = require('../../utils/areAllShapesTheSameType')

const path = require('../../path')

const geom2 = require('../../geom2')
const geom2subtract = require('../../ops-geom2/subtract')

const geom3 = require('../../geom3')
const geom3subtract = require('../../ops-geom3/subtract')

/**
   * Return a new geometry representing space in the first geometry but
   * not in the subsequent solids. None of the given geometries are modified.
   * @param {...geometries} objects - list of objects
   * @returns {geometry|[]} new geometries
   * @example
   * let C = subtract(A, B)
   * @example
   * +-------+            +-------+
   * |       |            |       |
   * |   A   |            |       |
   * |    +--+----+   =   |    +--+
   * +----+--+    |       +----+
   *      |   B   |
   *      |       |
   *      +-------+
 */
const subtract = (...objects) => {
  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  if (!areAllShapesTheSameType(objects)) {
    throw new Error('only subtract of the types are supported')
  }

  let object = objects[0]
  //if (path.isA(object)) return pathsubtract(matrix, objects)
  if (geom2.isA(object)) return geom2subtract(objects)
  if (geom3.isA(object)) return geom3subtract(objects)
  return object
}

module.exports = subtract
