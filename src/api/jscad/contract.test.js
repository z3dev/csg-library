const test = require('ava')

const path = require('../../path')
const geom2 = require('../../geom2')
const geom3 = require('../../geom3')

const contract = require('./contract')

//test('contract: contracting of a path produces an expected geom2', t => {
//  let geometry = path.fromPoints({}, [[0, 10, 0], [10, 0, 0]])
//
//  geometry = contract({radius: 3}, geometry)
//  let obs = geom2.toPoints(geometry)
//  let exp = [
//  ]
//
//  t.deepEqual(obs, exp)
//})

test('contract: contracting of a geom2 produces expected changes to points', t => {
  let geometry = geom2.fromPoints([[-8, -8], [8, -8], [8, 8], [-8, 8]])

  geometry = contract({radius: 2, resolution: 8}, geometry)
  let obs = geom2.toPoints(geometry)
  let exp = [ [ -6, -6 ], [ 6, -6 ], [ 6, 6 ], [ -6, 6 ] ]

  t.deepEqual(obs, exp)
})

test('contract: contracting of a geom3 produces expected changes to polygons', t => {
  let polygonsAsPoints = [
    [[-5,-5,-5],[-5,-5,15],[-5,15,15],[-5,15,-5]],
    [[15,-5,-5],[15,15,-5],[15,15,15],[15,-5,15]],
    [[-5,-5,-5],[15,-5,-5],[15,-5,15],[-5,-5,15]],
    [[-5,15,-5],[-5,15,15],[15,15,15],[15,15,-5]],
    [[-5,-5,-5],[-5,15,-5],[15,15,-5],[15,-5,-5]],
    [[-5,-5,15],[15,-5,15],[15,15,15],[-5,15,15]]
  ]
  let geometry = geom3.fromPoints(polygonsAsPoints)

  geometry = contract({radius: 2, resolution: 8}, geometry)
  let obs = geom3.toPoints(geometry)
  let exp0 = [
    [ 13, 13, -3 ], [ 13, -3, -3 ], [ -3, -3, -3 ], [ -3, 13, -3 ]
  ]
  let exp5 = [
    [ -3, 13, 13 ], [ -3, -3, 13 ], [ 13, -3, 13 ], [ 13, 13, 13 ]
  ]

  t.is(obs.length, 6)
  t.deepEqual(obs[0], exp0)
  t.deepEqual(obs[5], exp5)
})
