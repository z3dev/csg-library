
const {defaultResolution2D, defaultResolution3D} = require('../../constants')

const flatten = require('../../utils/flatten')

const path = require('../../path')
const pathextrude = require('../../ops-path/extrude')

const geom2 = require('../../geom2')
const geom2extrude = require('../../ops-geom2/extrude')

//const geom3 = require('../../geom3')

const {parseOptionAsFloat, parseOptionAsInt, parseOptionAs3DVector} = require('../optionParsers')

/**
 * Extrude the given object(s) using the given options (if any)
 * @param {Object} options - options for extrude
 * @param {Array} [options.offset=[0,0,1]] the direction of the extrusion as a 3D vector
 * @param {Number} [options.twistangle=0] the final rotatation (degrees) about the origin of the shape
 * @param {Integer} [options.twiststeps=defaultResolution3D] the resolution of the twist (if any) about the axis
 * @param {Number} [options.radius=1] - the radius of the expansion for paths
 * @param {Number} [options.resolution=defaultResolution2D] the number of segments of the expansion for paths
 * @param {Object|Array} objects - the objects(s) to extrude
 * @return {Object|Array} the extruded object(s)
 *
 * @example
 * let myshape = extrude({offset: [0,0,10]}, rectangle({radius: [20, 25]}))
 */
const extrude = function (options, ...objects) {
  let offset = parseOptionAs3DVector(options, 'offset', [0, 0, 1])
  let twistangle = parseOptionAsFloat(options, 'twistangle', 0)
  let twiststeps = parseOptionAsInt(options, 'twiststeps', defaultResolution3D)
  // for expanding paths
  let radius = parseOptionAsFloat(options, 'radius', 1)
  let resolution = parseOptionAsInt(options, 'resolution', defaultResolution2D)

  if (offset.z === 0) {
    throw new Error('offset cannot be orthogonal to the Z axis')
  }
  if (twistangle === 0 || twiststeps < 1) {
    twiststeps = 1
  }

  // TODO alternative option format: array of [x, y, z]
  // TODO alternative option format: object with x, y, z attributes

  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  offset = [offset.x, offset.y, offset.z]
  options = {offset: offset, radius: radius, twistangle: twistangle, twiststeps: twiststeps, resolution: resolution}

  const results = objects.map(function (object) {
    if (path.isA(object)) return pathextrude(options, object)
    if (geom2.isA(object)) return geom2extrude(options, object)
    //if (geom3.isA(object)) return geom3.extrude(options, object)
    return object
  })
  return results.length === 1 ? results[0] : results
}

module.exports = extrude
