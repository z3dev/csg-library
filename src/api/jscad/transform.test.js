const test = require('ava')

const Matrix4x4 = require('../../math/Matrix4x4')

const path = require('../../path')
const geom2 = require('../../geom2')

const transform = require('./transform')

test('transform: transforming of a path produces expected changes to points', t => {
  let matrix = Matrix4x4.translation([2, 2])
  let geometry = path.fromPoints({}, [[0, 0, 0], [1, 0, 0]])

  geometry = transform(matrix, geometry)
  let obs = path.toPoints(geometry)
  let exp = [ { _x: 2, _y: 2, _z: 0 }, { _x: 3, _y: 2, _z: 0 } ]

  t.deepEqual(obs, exp)
})

test('transform: transforming of a geom2 produces expected changes to points', t => {
  let matrix = Matrix4x4.scaling([5, 5])
  let geometry = geom2.fromPoints([[0, 0], [1, 0], [0, 1]])

  geometry = transform(matrix, geometry)
  let obs = geom2.toPoints(geometry)
  let exp = [[0, 0], [5, 0], [0, 5]  ]

  t.deepEqual(obs, exp)
})
