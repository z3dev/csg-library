const {defaultResolution3D} = require('../../constants')

const flatten = require('../../utils/flatten')

//const path = require('../../path')
//const pathextrude = require('../../ops-path/extrudeRotate')

const geom2 = require('../../geom2')
const geom2extrudeRotate = require('../../ops-geom2/extrudeRotate')

//const geom3 = require('../../geom3')

const {parseOptionAsFloat, parseOptionAsInt} = require('../optionParsers')

/**
 * Rotate extrude the given geometry using the given options.
 * @param {Object} [options] - options for extrusion
 * @param {Float} [options.angle=360] - angle of the extrusion, in degrees
 * @param {Float} [options.startangle=0] - start angle of the extrusion, in degrees
 * @param {Float} [options.overflow='cap'] - what to do with points outside of bounds (+ / - x) :
 * defaults to capping those points to 0 (only supported behaviour for now)
 * @param {Integer} [options.resolution=defaultResolution3D] - resolution/number of segments of the extrusion
 * @param {geom2} geometry the 2D geometry to extrude
 * @returns {geom3} new extruded 3D geometry
 */
const extrudeRotate = function (options, ...objects) {
  let angle = parseOptionAsFloat(options, 'angle', 360)
  let startangle = parseOptionAsFloat(options, 'startangle', 0)
  let overflow = 'cap'
  let resolution = parseOptionAsInt(options, 'resolution', defaultResolution3D)

  if (overflow !== 'cap') {
    throw new Error('only capping of overflowing points is supported')
  }
  if (resolution < 1) {
    throw new Error('resolution must be greater then one segment')
  }

  // TODO alternative option format: array of [x, y, z]
  // TODO alternative option format: object with x, y, z attributes

  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  options = {angle: angle, startangle: startangle, overflow: overflow, resolution: resolution}

  const results = objects.map(function (object) {
    //if (path.isA(object)) return pathextrude(options, object)
    if (geom2.isA(object)) return geom2extrudeRotate(options, object)
    //if (geom3.isA(object)) return geom3.extrude(options, object)
    return object
  })
  return results.length === 1 ? results[0] : results
}

module.exports = extrudeRotate
