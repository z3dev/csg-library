const flatten = require('../../utils/flatten')

const Matrix4x4 = require('../../math/Matrix4x4')

const path = require('../../path')
const geom2 = require('../../geom2')
//const geom3 = require('../../geom3')

const {parseOptionAs3DVector, parseOptionAsFloat} = require('../optionParsers')

// Tait-Bryan Euler angle convention using active, intrinsic rotations around the axes in the order z-y-x
// angle of rotations
//   yaw/Z rotation
//   pitch/Y rotation
//   roll/X rotation
const rotation = (yaw, pitch, roll) => {
  // convert to radians
  yaw = yaw * Math.PI * (1.0 / 180.0)
  pitch = pitch * Math.PI * (1.0 / 180.0)
  roll = roll * Math.PI * (1.0 / 180.0)

  // precompute sines and cosines of Euler angles
  const sy = Math.sin(yaw);
  const cy = Math.cos(yaw);
  const sp = Math.sin(pitch);
  const cp = Math.cos(pitch);
  const sr = Math.sin(roll);
  const cr = Math.cos(roll);

  // create and populate rotation matrix
  // clockwise rotation
  //var els = [
  //  cp*cy, sr*sp*cy - cr*sy, sr*sy + cr*sp*cy, 0,
  //  cp*sy, cr*cy + sr*sp*sy, cr*sp*sy - sr*cy, 0,
  //  -sp, sr*cp, cr*cp, 0,
  //  0, 0, 0, 1
  //]
  // counter clockwise rotation
  var els = [
    cp*cy, cp*sy, -sp, 0,
    sr*sp*cy - cr*sy, cr*cy + sr*sp*sy, sr*cp, 0,
    sr*sy + cr*sp*cy, cr*sp*sy - sr*cy, cr*cp, 0,
    0, 0, 0, 1
  ]
  return new Matrix4x4(els)
}

/**
 * Rotate the given object(s) using the given options (if any)
 * Based on a paper by D. Rose, Rotations in Three-Dimensions (Tait-Bryan euler angles)
 * @param {Object} options - options for rotate
 * @param {Array} [options.angles=[0,0,1]] - angle of rotations about X, Y, and X axis
 * @param {Object|Array} objects - the objects(s) to rotate
 * @return {Object|Array} the rotated object(s)
 *
 * @example
 * let newsphere = rotate({angles: [45,0,0]}, sphere())
 */
const rotate = function (options, ...objects) {
  let angles = parseOptionAs3DVector(options, 'angles', [0, 0, 0])

  // TODO alternative option format: array of [x, y, z]
  // TODO alternative option format: object with x, y, z attributes

  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  const matrix = rotation(angles.z, angles.y, angles.x)

  const results = objects.map(function (object) {
    if (path.isA(object)) return path.transform(matrix, object)
    if (geom2.isA(object)) return geom2.transform(matrix, object)
    //if (geom3.isA(object)) return geom3.transform(matrix, object)
    return object
  })
  return results.length === 1 ? results[0] : results
}

module.exports = rotate
