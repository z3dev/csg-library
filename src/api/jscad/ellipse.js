const {defaultResolution2D} = require('../../constants')

const createEllipse = require('../../primitives/ellipse')

const {parseOptionAs2DVector, parseOptionAsInt} = require('../optionParsers')

/** Construct an ellispe.
 * @param {Object} [options] - options for construction
 * @param {Vector2D} [options.center=[0,0]] - center of ellipse
 * @param {Vector2D} [options.radius=[1,1]] - radius of ellipse, width and height
 * @param {Number} [options.resolution=defaultResolution2D] - number of sides per 360 rotation
 * @returns {geom2} new 2D geometry
 */
const ellipse = function (options) {
  let center = parseOptionAs2DVector(options, 'center', [0, 0])
  let radius = parseOptionAs2DVector(options, 'radius', [1, 1])
  let resolution = parseOptionAsInt(options, 'resolution', defaultResolution2D)

  center = [center.x, center.y]
  radius = [radius.x, radius.y]
  return createEllipse({center: center, radius: radius, resolution: resolution})
}

module.exports = ellipse
