const flatten = require('../../utils/flatten')

const Matrix4x4 = require('../../math/Matrix4x4')

const path = require('../../path')
const geom2 = require('../../geom2')
const geom3 = require('../../geom3')

const {parseOptionAs3DVector} = require('../optionParsers')

/**
 * Translate the given object(s) using the given options (if any)
 * @param {Object} options - options for translate
 * @param {Array} options.offsets=[0,0,0] - offsets of which to translate the object
 * @param {Object|Array} objects - the objects(s) to translate
 * @return {Object|Array} the translated object(s)
 *
 * @example
 * let newsphere = translate({offsets: [5,0,10]}, sphere())
 */
const translate = function (options, ...objects) {
  let offsets = parseOptionAs3DVector(options, 'offsets', [0, 0, 0])

  // TODO alternative option format: array of [x, y, z]
  // TODO alternative option format: object with x, y, z attributes

  objects = flatten(objects)
  if (objects.length === 0) throw new Error('wrong number of arguments')

  const matrix = Matrix4x4.translation(offsets)

  const results = objects.map(function (object) {
    if (path.isA(object)) return path.transform(matrix, object)
    if (geom2.isA(object)) return geom2.transform(matrix, object)
    if (geom3.isA(object)) return geom3.transform(matrix, object)
    return object
  })
  return results.length === 1 ? results[0] : results
}

module.exports = translate
