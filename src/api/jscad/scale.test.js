const test = require('ava')

const path = require('../../path')
const geom2 = require('../../geom2')

const scale = require('./scale')

test('scale: scaling of a path produces expected changes to points', t => {
  let geometry = path.fromPoints({}, [[0, 1, 0], [1, 0, 0]])

  geometry = scale({factors: [2,3]}, geometry)
  let obs = path.toPoints(geometry)
  let exp = [ { _x: 0, _y: 3, _z: 0 }, { _x: 2, _y: 0, _z: 0 } ]

  t.deepEqual(obs, exp)
})

test('scale: scaling of a geom2 produces expected changes to points', t => {
  let geometry = geom2.fromPoints([[0, 0], [1, 0], [0, 1]])

  geometry = scale({factors: [3,6]}, geometry)
  let obs = geom2.toPoints(geometry)
  let exp = [[0, 0], [3, 0], [0, 6]  ]

  t.deepEqual(obs, exp)
})
