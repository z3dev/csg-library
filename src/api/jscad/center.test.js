const test = require('ava')

const path = require('../../path')
const geom2 = require('../../geom2')

const rectangle = require('./rectangle')

const center = require('./center')

test('center (defaults): centering of a path produces expected changes to points', t => {
  let line = path.fromPoints({}, [[10, 10, 10], [15, 15, 15]])

  line = center({}, line)
  let obs = path.toPoints(line)
  let exp = [ { _x: -2.5, _y: -2.5, _z: -2.5 }, { _x: 2.5, _y: 2.5, _z: 2.5 } ]

  t.deepEqual(obs, exp)
})

test('center (defaults) centering of a geom2 produces expected changes to points', t => {
  // NOTE: first point in geometry becomes last point in point array
  const exp = [
    [-1.00000, -1.00000],
    [1.00000, -1.00000],
    [1.00000, 1.00000],
    [-1.00000, 1.00000]
  ]
  let geometry = rectangle({center: [3, 6]}) // CENTER
  geometry = center({}, geometry)
  const obs = geom2.toPoints(geometry)

  t.deepEqual(obs.length, 4)
  t.deepEqual(obs, exp)
})
