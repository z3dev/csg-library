const Polygon3 = require('../math/Polygon3')

const {fromPolygons} = require('../core/CSGFactories')

// Extrude a polygon into the direction offsetvector
// Returns a CSG object
const extrudePolygon = function (offsetvector, polygon1) {
  let newpolygons = []

  let direction = polygon1.plane.normal.dot(offsetvector)
  if (direction > 0) {
    polygon1 = polygon1.flipped()
  }

  newpolygons.push(polygon1)

  let polygon2 = polygon1.translate(offsetvector)
  let numvertices = polygon1.vertices.length
  for (let i = 0; i < numvertices; i++) {
    let sidefacepoints = []
    let nexti = (i < (numvertices - 1)) ? i + 1 : 0
    sidefacepoints.push(polygon1.vertices[i].pos)
    sidefacepoints.push(polygon2.vertices[i].pos)
    sidefacepoints.push(polygon2.vertices[nexti].pos)
    sidefacepoints.push(polygon1.vertices[nexti].pos)
    let sidefacepolygon = Polygon3.fromPoints(sidefacepoints, polygon1.shared)
    newpolygons.push(sidefacepolygon)
  }
  polygon2 = polygon2.flipped()

  newpolygons.push(polygon2)

  return fromPolygons(newpolygons)
}

module.exports = extrudePolygon
