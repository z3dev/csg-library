const Matrix4x4 = require('../math/Matrix4x4')
const OrthoNormalBasis = require('../math/OrthoNormalBasis')
const Plane = require('../math/Plane')
const Polygon = require('../math/Polygon3')

const CAG = require('../api/CAG')
// FIXME: for some weird reason if CAG is imported AFTER frompolygons, a lot of things break???

const Properties = require('../core/Properties')
const Shared = require('../core/Shared')

const canonicalize = require('../core/utils/canonicalize')
const {projectToOrthoNormalBasis} = require('../core/utils/csgProjections')

const {sectionCut, cutByPlane} = require('../api/ops-cuts')

const flatten = require('../utils/flatten')

// the underlying geometry
const geom3 = require('../geom3')

// the extended operations of this object, beyond those of the geometry
const calculateBounds = require('../ops-geom3/calculateBounds')
const invert = require('../ops-geom3/invert')
const mayOverlap = require('../ops-geom3/mayOverlap')
const retesselate = require('../ops-geom3/retesselate')
const toBinaryDataBuffers = require('../ops-geom3/toBinaryDataBuffers')
const toTriangles = require('../ops-geom3/toTriangles')
const union = require('../ops-geom3/union')
const unionSub = require('../ops-geom3/unionSub')
const subtractSub = require('../ops-geom3/subtractSub')
const intersectSub = require('../ops-geom3/intersectSub')

const jscad = require('../api/jscad')

/** Class CSG
 * Holds a binary space partition tree representing a 3D solid. Two solids can
 * be combined using the `union()`, `subtract()`, and `intersect()` methods.
 * @constructor
 */
let CSG = function (geometry) {
  if (geometry === undefined) {
    this.geometry = geom3.create()
  } else {
    this.geometry = geometry
  }
  this.properties = new Properties()
}

CSG.prototype = {
  /**
   * Return a new CSG solid representing the space in either this solid or
   * in the given solids. Neither this solid nor the given solids are modified.
   * @param {CSG[]} csg - list of CSG objects
   * @returns {CSG} new CSG object
   * @example
   * let C = A.union(B)
   * @example
   * +-------+            +-------+
   * |       |            |       |
   * |   A   |            |       |
   * |    +--+----+   =   |       +----+
   * +----+--+    |       +----+       |
   *      |   B   |            |       |
   *      |       |            |       |
   *      +-------+            +-------+
   */
  union: function (...csgs) {
    csgs = flatten(csgs)
    let geometries = csgs.map((csg) => csg.geometry)
    geometries.push(this.geometry)

    let newgeometry = union(geometries)
    return new CSG(newgeometry)
  },

  unionSub: function (csg, doRetesselate, doCanonicalize) {
    let newgeometry = unionSub(this.geometry, csg.geometry)
    if (doRetesselate) newgeometry = retesselate(newgeometry)
    //if (doCanonicalize) newgeometry = canonicalized(newgeometry)

    let newcsg = new CSG(newgeometry)
    newcsg.properties = this.properties._merge(csg.properties)
    return newcsg
  },

  /**
   * Return a new CSG solid representing space in this solid but
   * not in the given solids. Neither this solid nor the given solids are modified.
   * @param {CSG[]} csg - list of CSG objects
   * @returns {CSG} new CSG object
   * @example
   * let C = A.subtract(B)
   * @example
   * +-------+            +-------+
   * |       |            |       |
   * |   A   |            |       |
   * |    +--+----+   =   |    +--+
   * +----+--+    |       +----+
   *      |   B   |
   *      |       |
   *      +-------+
   */
  subtract: function (...csgs) {
    csgs = flatten(csgs)
    let result = this
    for (let i = 0; i < csgs.length; i++) {
      let islast = (i === (csgs.length - 1))
      result = result.subtractSub(csgs[i], islast, islast)
    }
    return result
  },

  subtractSub: function (csg, doRetesselate, doCanonicalize) {
    let newgeometry = subtractSub(this.geometry, csg.geometry)
    if (doRetesselate) newgeometry = retesselate(newgeometry)
    //if (doCanonicalize) newgeometry = canonicalized(newgeometry)

    let newcsg = new CSG(newgeometry)
    return newcsg
  },

  /**
   * Return a new CSG solid representing space in both this solid and
   * in the given solids. Neither this solid nor the given solids are modified.
   * @param {CSG[]} csg - list of CSG objects
   * @returns {CSG} new CSG object
   * @example
   * let C = A.intersect(B)
   * @example
   * +-------+
   * |       |
   * |   A   |
   * |    +--+----+   =   +--+
   * +----+--+    |       +--+
   *      |   B   |
   *      |       |
   *      +-------+
   */
  intersect: function (...csgs) {
    csgs = flatten(csgs)
    let result = this
    for (let i = 0; i < csgs.length; i++) {
      let islast = (i === (csgs.length - 1))
      result = result.intersectSub(csgs[i], islast, islast)
    }
    return result
  },

  intersectSub: function (csg, doRetesselate, doCanonicalize) {
    let newgeometry = intersectSub(this.geometry, csg.geometry)
    if (doRetesselate) newgeometry = retesselate(newgeometry)
    //if (doCanonicalize) newgeometry = canonicalized(newgeometry)

    let newcsg = new CSG(newgeometry)
    return newcsg
  },

  // ALIAS !
  invert: function () {
    let newgeometry = invert(this.geometry)
    return new CSG(newgeometry)
  },

  // ALIAS !
  transform: function (matrix) {
    let newgeometry = geom3.transform(matrix, this.geometry)
    return new CSG(newgeometry)

/*
    let ismirror = matrix4x4.isMirroring()
    let transformedvertices = {}
    let transformedplanes = {}
    let newpolygons = this.polygons.map(function (p) {
      let newplane
      let plane = p.plane
      let planetag = plane.getTag()
      if (planetag in transformedplanes) {
        newplane = transformedplanes[planetag]
      } else {
        newplane = plane.transform(matrix4x4)
        transformedplanes[planetag] = newplane
      }
      let newvertices = p.vertices.map(function (v) {
        let newvertex
        let vertextag = v.getTag()
        if (vertextag in transformedvertices) {
          newvertex = transformedvertices[vertextag]
        } else {
          newvertex = v.transform(matrix4x4)
          transformedvertices[vertextag] = newvertex
        }
        return newvertex
      })
      if (ismirror) newvertices.reverse()
      return new Polygon(newvertices, p.shared, newplane)
    })
    let result = fromPolygons(newpolygons)
    result.properties = this.properties._transform(matrix4x4)
    return result
*/
  },

  // Use the JSCAD API in order to process options
  center: function (axes) {
    let newgeometry = jscad.center({axes: axes}, this.geometry)
    return new CSG(newgeometry)
  },

  expand: function (radius, resolution) {
    let newgeometry = jscad.expand({radius: radius, resolution: resolution}, this.geometry)
    return new CSG(newgeometry)
  },

  contract: function (radius, resolution) {
    let newgeometry = jscad.contract({radius: radius, resolution: resolution}, this.geometry)
    return new CSG(newgeometry)
  },

  // cut the solid at a plane, and stretch the cross-section found along plane normal
  // note: only used in roundedCube() internally
  stretchAtPlane: function (normal, point, length) {
    let plane = Plane.fromNormalAndPoint(normal, point)
    let onb = new OrthoNormalBasis(plane)
    let crosssect = this.sectionCut(onb)
    let midpiece = crosssect.extrudeInOrthonormalBasis(onb, length)
    let piece1 = this.cutByPlane(plane)
    let piece2 = this.cutByPlane(plane.flipped())
    let result = piece1.union([midpiece, piece2.translate(plane.normal.times(length))])
    return result
  },

  canonicalized: function () {
    return canonicalize(this)
  },

  reTesselated: function () {
    let newgeometry = retesselate(this.geometry)
    return new CSG(newgeometry)
  },

  // ALIAS !
  getBounds: function () {
    return calculateBounds(this.geometry)
  },

  // ALIAS !
  mayOverlap: function (csg) {
    return mayOverlap(this.geometry, csg.geometry)
  },

  cutByPlane: function (plane) {
    return cutByPlane(this, plane)
  },

  /**
   * Connect a solid to another solid, such that two Connectors become connected
   * @param  {Connector} myConnector a Connector of this solid
   * @param  {Connector} otherConnector a Connector to which myConnector should be connected
   * @param  {Boolean} mirror false: the 'axis' vectors of the connectors should point in the same direction
   * true: the 'axis' vectors of the connectors should point in opposite direction
   * @param  {Float} normalrotation degrees of rotation between the 'normal' vectors of the two
   * connectors
   * @returns {CSG} this csg, tranformed accordingly
   */
  connectTo: function (myConnector, otherConnector, mirror, normalrotation) {
    let matrix = myConnector.getTransformationTo(otherConnector, mirror, normalrotation)
    return this.transform(matrix)
  },

  /**
   * set the shared property of all polygons
   * @param  {Object} shared
   * @returns {CSG} Returns a new CSG solid, the original is unmodified!
   */
  setShared: function (shared) {
    const polygons = geom3.toPolygons(this.geometry)
    const newpolygons = polygons.map(function (polygon) {
      return new Polygon(polygon.vertices, shared, polygon.plane)
    })
    let newgeometry = geom3.create(newpolygons)
    let result = new CSG(newgeometry)

    result.properties = this.properties // keep original properties
    return result
  },

  // ALIAS !
  toPolygons: function () {
    return geom3.toPolygons(this.geometry)
  },

  // ALIAS !
  toString: function () {
    let result = 'CSG: ' + geom3.toString(this.geometry)
    return result
  },

  /** sets the color of this csg: non mutating, returns a new CSG
   * @param  {Object} args
   * @returns {CSG} a copy of this CSG, with the given color
   */
  setColor: function (args) {
    let newshared = Shared.fromColor.apply(this, arguments)
    return this.setShared(newshared)
  },

  // project the 3D CSG onto a plane
  // This returns a 2D CAG with the 'shadow' shape of the 3D solid when projected onto the
  // plane represented by the orthonormal basis
  projectToOrthoNormalBasis: function (orthobasis) {
    return projectToOrthoNormalBasis(this, orthobasis)
  },

  // FIXME: not finding any uses within our code ?
  sectionCut: function (orthobasis) {
    return sectionCut(this, orthobasis)
  },

  /**
   * Returns an array of values for the requested features of this solid.
   * Supported Features: 'volume', 'area'
   * @param {String[]} features - list of features to calculate
   * @returns {Float[]} values
   * @example
   * let volume = A.getFeatures('volume')
   * let values = A.getFeatures('area','volume')
   */
  getFeatures: function (features) {
    if (!(features instanceof Array)) {
      features = [features]
    }
    const polygons = geom3.toPolygons(this.geometry)
    let results = []
    features.forEach(function (feature) {
      let total = 0
      polygons.forEach(function (polygon) {
        if (feature === 'area') total += polygon.getArea()
        if (feature === 'volume') total += polygon.getSignedVolume()
      })
      results.push(total)
    })
    return (results.length === 1) ? results[0] : results
  },

  // ALIAS !
  toCompactBinary: function () {
    return toBinaryDataBuffers(this.geometry)
  },

  // ALIAS !
  toTriangles: function () {
    return toTriangles(this.geometry)
  },

  mirrored: function (plane) {
    return this.transform(Matrix4x4.mirroring(plane))
  },

  mirroredX: function () {
    let plane = Plane.fromNormalAndPoint([1, 0, 0], [0, 0, 0])
    return this.mirrored(plane)
  },

  mirroredY: function () {
    let plane = Plane.fromNormalAndPoint([0, 1, 0], [0, 0, 0])
    return this.mirrored(plane)
  },

  mirroredZ: function () {
    let plane = Plane.fromNormalAndPoint([0, 0, 1], [0, 0, 0])
    return this.mirrored(plane)
  },

  translate: function (v) {
    return this.transform(Matrix4x4.translation(v))
  },

  scale: function (f) {
    return this.transform(Matrix4x4.scaling(f))
  },

  rotateX: function (deg) {
    return this.transform(Matrix4x4.rotationX(deg))
  },

  rotateY: function (deg) {
    return this.transform(Matrix4x4.rotationY(deg))
  },

  rotateZ: function (deg) {
    return this.transform(Matrix4x4.rotationZ(deg))
  },

  rotate: function (rotationCenter, rotationAxis, degrees) {
    return this.transform(Matrix4x4.rotationSCAD(rotationCenter, rotationAxis, degrees))
  },

  rotateEulerAngles: function (alpha, beta, gamma, position) {
    return this.transform(Matrix4x4.rotateEulerAngles(alpha, beta, gamma, position))
  }
}

module.exports = CSG
