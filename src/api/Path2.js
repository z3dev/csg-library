const Matrix4x4 = require('../math/Matrix4x4')
const Plane = require('../math/Plane')

// the underlying geometry
const path = require('../path')

// the extended operations of this object, beyond those of the geometry
const appendArc = require('../ops-path/appendArc')
const appendBezier = require('../ops-path/appendBezier')
const appendPoints = require('../ops-path/appendPoints')

// high level functions from JSCAD API
const jscad = require('./jscad')

/** Class Path2
 * Represents infinitely thin lines, i.e. no measurable area or volume.
 * A path can be open or closed, i.e. the start and end are the same.
 * @constructor
 * @param {geom2} [geometry] a provided geometry
 *
 * @example
 * let path1 = Path2.fromPoints([[10,10], [-10,10], [-10,-10], [10,-10]], true) // closed
 * let path2 = Part2.arc({
 *   center: [5, 5],
 *   radius: 10,
 *   startangle: 90,
 *   endangle: 180,
 *   resolution: 36,
 * })
 * let path3 = path1.concat(path2)
 */
const Path2 = function (geometry) {
  if (geometry === undefined) {
    this.geometry = path.create()
  } else {
    this.geometry = geometry
  }
}

Path2.fromPoints = function (points, closed) {
  var newpath = path.fromPoints({closed: closed}, points)
  return new Path2(newpath)
}

// Use the JSCAD API in order to process options
Path2.arc = function (options) {
  var newpath = jscad.arc(options)
  return new Path2(newpath)
}

Path2.prototype = {

  concat: function (otherpath) {
    let newpath = path.concat(this.geometry, otherpath.geometry)
    return new Path2(newpath)
  },

  toPoints: function () {
    return path.toPoints(this.geometry)
  },

  appendPoint: function (point) {
    let newpath = appendPoints({}, [point], this.geometry)
    return new Path2(newpath)
  },

  appendPoints: function (points) {
    let newpath = appendPoints({close: false}, points, this.geometry)
    return new Path2(newpath)
  },

  close: function () {
    let newpath = path.close(this.geometry)
    return new Path2(newpath)
  },

  isClosed: function () {
    return this.geometry.isClosed
  },

  getTurn: function () {
    return path.direction(this.geometry)
  },

  transform: function (matrix) {
    let newpath = path.transform(matrix, this.geometry)
    return new Path2(newpath)
  },

  appendBezier: function (controlpoints, options) {
    let newpath = appendBezier(options, controlpoints, this.geometry)
    return new Path2(newpath)
  },

  appendArc: function (endpoint, options) {
    let newpath = appendArc(options, endpoint, this.geometry)
    return new Path2(newpath)
  },

  toString: function () {
    return path.toString(this.geometry)
  },

  // Use the JSCAD API in order to process options
  extrude: function (options) {
    let newgeom3 = jscad.extrude(options, this.geometry)
    const CSG = require('../api/CSG')
    return new CSG(newgeom3)
  },

  // Use the JSCAD API in order to process options
  expand: function (options) {
    let newgeom2 = jscad.expand(options, this.geometry)
    const CAG = require('../api/CAG')
    return new CAG(newgeom2)
  },

  mirrored: function (plane) {
    return this.transform(Matrix4x4.mirroring(plane))
  },

  mirroredX: function () {
    let plane = Plane.fromNormalAndPoint([1, 0, 0], [0, 0, 0])
    return this.mirrored(plane)
  },

  mirroredY: function () {
    let plane = Plane.fromNormalAndPoint([0, 1, 0], [0, 0, 0])
    return this.mirrored(plane)
  },

  mirroredZ: function () {
    let plane = Plane.fromNormalAndPoint([0, 0, 1], [0, 0, 0])
    return this.mirrored(plane)
  },

  translate: function (v) {
    return this.transform(Matrix4x4.translation(v))
  },

  scale: function (f) {
    return this.transform(Matrix4x4.scaling(f))
  },

  rotateX: function (deg) {
    return this.transform(Matrix4x4.rotationX(deg))
  },

  rotateY: function (deg) {
    return this.transform(Matrix4x4.rotationY(deg))
  },

  rotateZ: function (deg) {
    return this.transform(Matrix4x4.rotationZ(deg))
  },

  rotate: function (rotationCenter, rotationAxis, degrees) {
    return this.transform(Matrix4x4.rotationSCAD(rotationCenter, rotationAxis, degrees))
  },

  rotateEulerAngles: function (alpha, beta, gamma, position) {
    return this.transform(Matrix4x4.rotateEulerAngles(alpha, beta, gamma, position))
  }
}

module.exports = Path2
