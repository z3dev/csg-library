const Polygon3 = require('../math/Polygon3')
const Vector3 = require('../math/Vector3')

const geom3 = require('../geom3')

/** Construct a cylinder with rounded ends.
 * @param {Object} [options] - options for construction
 * @param {Vector3} [options.start=[0,-1,0]] - start point of cylinder
 * @param {Vector3} [options.end=[0,1,0]] - end point of cylinder
 * @param {Number} [options.radius=1] - radius of rounded ends, must be scalar
 * @param {Number} [options.resolution=defaultResolution3D] - number of polygons per 360 degree revolution
 * @returns {CSG} new 3D solid
 *
 * @example
 * let cylinder = roundedCylinder({
 *   start: [0, -10, 0],
 *   end: [0, 10, 0],
 *   radius: 2,
 *   resolution: 16
 * })
 */
const roundedCylinder = function (options) {
  const defaults = {
    start: [0, -1, 0],
    end: [0, 1, 0],
    radius: 1,
    resolution: defaultResolution3D
  }
  let {start, end, radius, resolution} = Object.assign({}, defaults, options)

  let p1 = start
  let p2 = end

  let direction = p2.minus(p1)
  let defaultnormal
  if (Math.abs(direction.x) > Math.abs(direction.y)) {
    defaultnormal = new Vector3(0, 1, 0)
  } else {
    defaultnormal = new Vector3(1, 0, 0)
  }

  if (resolution < 4) resolution = 4

  let qresolution = Math.floor(0.25 * resolution)

  let length = direction.length()
  if (length < EPS) { // FIXME this seems wrong
    return sphere({
      center: p1,
      radius: radius,
      resolution: resolution
    })
  }

  let zvector = direction.unit().times(radius)
  let xvector = zvector.cross(defaultnormal).unit().times(radius)
  let yvector = xvector.cross(zvector).unit().times(radius)

  let polygons = []
  let prevcylinderpoint
  for (let slice1 = 0; slice1 <= resolution; slice1++) {
    let angle = Math.PI * 2.0 * slice1 / resolution
    let cylinderpoint = xvector.times(Math.cos(angle)).plus(yvector.times(Math.sin(angle)))
    if (slice1 > 0) {
      // cylinder points:
      let points = []
      points.push(p1.plus(cylinderpoint))
      points.push(p1.plus(prevcylinderpoint))
      points.push(p2.plus(prevcylinderpoint))
      points.push(p2.plus(cylinderpoint))
      polygons.push(Polygon3.fromPoints(points))
      let prevcospitch, prevsinpitch
      for (let slice2 = 0; slice2 <= qresolution; slice2++) {
        let pitch = 0.5 * Math.PI * slice2 / qresolution
        let cospitch = Math.cos(pitch)
        let sinpitch = Math.sin(pitch)
        if (slice2 > 0) {
          points = []
          points.push(p1.plus(prevcylinderpoint.times(prevcospitch).minus(zvector.times(prevsinpitch))))
          points.push(p1.plus(cylinderpoint.times(prevcospitch).minus(zvector.times(prevsinpitch))))
          if (slice2 < qresolution) {
            points.push(p1.plus(cylinderpoint.times(cospitch).minus(zvector.times(sinpitch))))
          }
          points.push(p1.plus(prevcylinderpoint.times(cospitch).minus(zvector.times(sinpitch))))
          polygons.push(Polygon3.fromPoints(points))

          points = []
          points.push(p2.plus(prevcylinderpoint.times(prevcospitch).plus(zvector.times(prevsinpitch))))
          points.push(p2.plus(cylinderpoint.times(prevcospitch).plus(zvector.times(prevsinpitch))))
          if (slice2 < qresolution) {
            points.push(p2.plus(cylinderpoint.times(cospitch).plus(zvector.times(sinpitch))))
          }
          points.push(p2.plus(prevcylinderpoint.times(cospitch).plus(zvector.times(sinpitch))))
          points.reverse()
          polygons.push(Polygon3.fromPoints(points))
        }
        prevcospitch = cospitch
        prevsinpitch = sinpitch
      }
    }
    prevcylinderpoint = cylinderpoint
  }
  let result = geom3.create(polygons)
  return result
}

module.exports = roundedCylinder
