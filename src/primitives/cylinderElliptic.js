const Polygon3 = require('../math/Polygon3')

const geom3 = require('../geom3')

/** Construct an elliptic cylinder.
 * @param {Object} [options] - options for construction
 * @param {Vector3} [options.start=[0,-1,0]] - start point of cylinder
 * @param {Vector2D} [options.startRadius=[1,1]] - radius of rounded start, must be two dimensional array
 * @param {Vector3} [options.end=[0,1,0]] - end point of cylinder
 * @param {Vector2D} [options.endRadius=[1,1]] - radius of rounded end, must be two dimensional array
 * @param {Number} [options.resolution=defaultResolution3D] - number of polygons per 360 degree revolution
 * @returns {geom3} new geometry
 *
 * @example
 *     let cylinder = CSG.cylinderElliptic({
 *       start: [0, -10, 0],
 *       end: [0, 10, 0],
 *       radiusStart: [10,5],
 *       radiusEnd: [8,3],
 *       resolution: 16
 *     });
 */
const cylinderElliptic = function (options) {
  const defaults = {
    start: [0, -1, 0],
    startRadius: [1,1],
    end: [0, 1, 0],
    endRadius: [1,1],
    resolution: defaultResolution3D
  }
  let {start, startRadius, end, endRadius, resolution} = Object.assign({}, defaults, options)

  if ((endRadius._x <= 0) || (startRadius._x <= 0) || (endRadius._y <= 0) || (startRadius._y <= 0)) {
    throw new Error('Radius should be positive')
  }

  let slices = resolution

  let ray = e.minus(s)
  let axisZ = ray.unit()
  let axisX = axisZ.randomNonParallelVector().unit()
  let axisY = axisX.cross(axisZ).unit()

  function point (stack, slice, radius) {
    let angle = slice * Math.PI * 2
    let out = axisX.times(radius._x * Math.cos(angle)).plus(axisY.times(radius._y * Math.sin(angle)))
    let pos = s.plus(ray.times(stack)).plus(out)
    return pos
  }

  let polygons = []
  for (let i = 0; i < slices; i++) {
    let t0 = i / slices
    let t1 = (i + 1) / slices

    if (endRadius._x === startRadius._x && endRadius._y === startRadius._y) {
      polygons.push(Polygon3.fromPoints([start, point(0, t0, endRadius), point(0, t1, endRadius)]))
      polygons.push(Polygon3.fromPoints([point(0, t1, endRadius), point(0, t0, endRadius), point(1, t0, endRadius), point(1, t1, endRadius)]))
      polygons.push(Polygon3.fromPoints([end, point(1, t1, endRadius), point(1, t0, endRadius)]))
    } else {
      if (startRadius._x > 0) {
        polygons.push(Polygon3.fromPoints([start, point(0, t0, startRadius), point(0, t1, startRadius)]))
        polygons.push(Polygon3.fromPoints([point(0, t0, startRadius), point(1, t0, endRadius), point(0, t1, startRadius)]))
      }
      if (endRadius._x > 0) {
        polygons.push(Polygon3.fromPoints([end, point(1, t1, endRadius), point(1, t0, endRadius)]))
        polygons.push(Polygon3.fromPoints([point(1, t0, endRadius), point(1, t1, endRadius), point(0, t1, startRadius)]))
      }
    }
  }
  let result = geom3.create(polygons)
  return result
}

module.exports = cylinderElliptic
