const Vector2 = require('../math/Vector2')

const geom2 = require('../geom2')

/** Construct a rectangle.
 * @param {Object} [options] - options for construction
 * @param {Array} [options.center=[0,0]] - center of rectangle
 * @param {Array} [options.radius=[1,1]] - radius of rectangle, width and height
 * @returns {geom2} new 2D geometry
 */
const rectangle = function (options) {
  const defaults = {
    center: [0, 0],
    radius: [1, 1]
  }
  const {radius, center} = Object.assign({}, defaults, options)

  // convert to vectors in order to perform math
  const centerv = new Vector2(center[0], center[1])
  const radiusv = new Vector2(radius[0], radius[1])
  const rswap = new Vector2(radius[0], -radius[1])

  const points = [
    centerv.minus(radiusv), centerv.plus(rswap), centerv.plus(radiusv), centerv.minus(rswap)
  ]
  return geom2.fromPoints(points)
}

module.exports = rectangle
