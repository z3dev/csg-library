module.exports = {
  arc: require('./arc'),
  circle: require('./circle'),
  cube: require('./cube'),
  cylinder: require('./cylinder'),
  cylinderElliptic: require('./cylinderElliptic'),
  ellipse: require('./ellipse'),
  rectangle: require('./rectangle'),
  roundedCube: require('./roundedCube'),
  roundedCylinder: require('./roundedCylinder'),
  sphere: require('./sphere')
}
