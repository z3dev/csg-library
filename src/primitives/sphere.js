const Polygon3 = require('../math/Polygon3')
const Vector3 = require('../math/Vector3')

const geom3 = require('../geom3')

/**
 * Construct a solid sphere
 * @param {Object} [options] - options for construction
 * @param {Vector3} [options.center=[0,0,0]] - center of sphere
 * @param {Number} [options.radius=1] - radius of sphere
 * @param {Number} [options.resolution=defaultResolution3D] - number of polygons per 360 degree revolution
 * @param {Array} [options.axes] -  an array with 3 vectors for the x, y and z base vectors
 * @returns {geom3} new 3D geometry
*/
const sphere = (options) => {
  let center = options.center
  let radius = options.radius
  let resolution = options.resolution

  let xvector, yvector, zvector
  if ('axes' in options) {
    xvector = options.axes[0].unit().times(radius)
    yvector = options.axes[1].unit().times(radius)
    zvector = options.axes[2].unit().times(radius)
  } else {
    xvector = new Vector3(1, 0, 0).times(radius)
    yvector = new Vector3(0, -1, 0).times(radius)
    zvector = new Vector3(0, 0, 1).times(radius)
  }
  if (resolution < 4) resolution = 4

  let qresolution = Math.round(resolution / 4)
  let prevcylinderpoint
  let polygons = []
  for (let slice1 = 0; slice1 <= resolution; slice1++) {
    let angle = Math.PI * 2.0 * slice1 / resolution
    let cylinderpoint = xvector.times(Math.cos(angle)).plus(yvector.times(Math.sin(angle)))
    if (slice1 > 0) {
      // cylinder vertices:
      let vertices = []
      let prevcospitch, prevsinpitch
      for (let slice2 = 0; slice2 <= qresolution; slice2++) {
        let pitch = 0.5 * Math.PI * slice2 / qresolution
        let cospitch = Math.cos(pitch)
        let sinpitch = Math.sin(pitch)
        if (slice2 > 0) {
          points = []
          points.push(center.plus(prevcylinderpoint.times(prevcospitch).minus(zvector.times(prevsinpitch))))
          points.push(center.plus(cylinderpoint.times(prevcospitch).minus(zvector.times(prevsinpitch))))
          if (slice2 < qresolution) {
            points.push(center.plus(cylinderpoint.times(cospitch).minus(zvector.times(sinpitch))))
          }
          points.push(center.plus(prevcylinderpoint.times(cospitch).minus(zvector.times(sinpitch))))
          polygons.push(Polygon3.fromPoints(points))

          points = []
          points.push(center.plus(prevcylinderpoint.times(prevcospitch).plus(zvector.times(prevsinpitch))))
          points.push(center.plus(cylinderpoint.times(prevcospitch).plus(zvector.times(prevsinpitch))))
          if (slice2 < qresolution) {
            points.push(center.plus(cylinderpoint.times(cospitch).plus(zvector.times(sinpitch))))
          }
          points.push(center.plus(prevcylinderpoint.times(cospitch).plus(zvector.times(sinpitch))))
          points.reverse()
          polygons.push(Polygon3.fromPoints(points))
        }
        prevcospitch = cospitch
        prevsinpitch = sinpitch
      }
    }
    prevcylinderpoint = cylinderpoint
  }
  return geom3.create(polygons)
}

module.exports = sphere
