module.exports = {
  canonicalize: require('./canonicalize'),
  clone: require('./clone'),
  create: require('./create'),
  fromObject: require('./fromObject'),
  fromPoints: require('./fromPoints'),
  isA: require('./isA'),
  toPoints: require('./toPoints'),
  toPolygons: require('./toPolygons'),
  toString: require('./toString'),
  transform: require('./transform')
}
