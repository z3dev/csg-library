
const canonicalize = require('./canonicalize')

/*
 * Return the given geometry as a list of points, after applying transforms.
 * @return {Array[[points...]...]} list of polygons, represented as a list of points, each point containing 3 numbers
 */
const toPoints = function (geometry) {
  let polygons = canonicalize(geometry).polygons
  let listofpoints = polygons.map(function (polygon) {
    let points = polygon.vertices.map((vertex) => {
       let pos = vertex.pos
       return [pos.x, pos.y, pos.z]
    })
    return  points
  })
  return listofpoints
}

module.exports = toPoints
