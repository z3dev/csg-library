const Polygon3 = require('../math/Polygon3')

const create = require('./create')

const fromObject = (obj) => {
  let polygons = obj.basePolygons.map(function (p) {
    return Polygon3.fromObject(p)
  })
  return create(polygons)
}

module.exports = fromObject
