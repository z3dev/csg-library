/**
 * Create a string representing the contents of the given geometry.
 * @returns {String} a representive string
 * @example
 * console.out(toString(geometry))
 */
const toString = function (geometry) {
  if (geometry.isCanonicalized) {
    let result = 'geom3 (' + geometry.polygons.length + ' polygons):\n'
    geometry.polygons.forEach(function (polygon) {
      result += '  ' + polygon.toString() + '\n'
    })
    return result
  }

  let result = 'geom3 (' + geometry.basePolygons.length + ' basePolygons):\n'
  geometry.basePolygons.forEach(function (polygon) {
    result += '  ' + polygon.toString() + '\n'
  })
  return result
}

module.exports = toString
