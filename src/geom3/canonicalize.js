/**
 * Updates the given geometry by canonicalizing the polygons.
 * Must be called before exposing any polygon data.
 * @param {geom3} geometry - the geometry to canonicalize.
 * @returns {geom3} the given geometry
 * @example
 * let newgeometry = canonicalize(geometry)
 */
const canonicalize = (geometry) => {
  if (geometry.isCanonicalized) {
    return geometry
  }
  // apply transforms to each polygon, and canonicalize
  const isMirror = geometry.transforms.isMirroring()
  geometry.polygons = geometry.basePolygons.map((polygon) => {
    // TBD if (isMirror) newvertices.reverse()
    return polygon.transform(geometry.transforms) // .canonicalize()
  })
  geometry.isCanonicalized = true
  return geometry
}

module.exports = canonicalize
