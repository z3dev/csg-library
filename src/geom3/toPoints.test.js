const test = require('ava')

const {toPoints, fromPoints, toString} = require('./index')

test('toPoints: Creates an array of points from a populated geom3', (t) => {
  const points = [[[0, 0, 0], [1, 0, 0], [1, 0, 1]]]
  const geometry = fromPoints(points)

  const asstring = toString(geometry)
  // console.log(asstring)

  const pointarray = toPoints(geometry)
  t.deepEqual(pointarray, points)
})
