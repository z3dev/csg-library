const test = require('ava')

const fromPoints = require('./fromPoints')

test('fromPoints: Creates a populated geom3', (t) => {
  const points = [[[0, 0, 0], [1, 0, 0], [1, 0, 1]]]
  const expected = {
    basePolygons: [
      {
        plane: {normal: {_x: 0, _y: -1, _z: 0}, w: 0},
        shared: {},
        vertices: [
          {pos: {_x: 0, _y: 0, _z: 0}},
          {pos: {_x: 1, _y: 0, _z: 0}},
          {pos: {_x: 1, _y: 0, _z: 1}},
         ]
      }
    ],
    polygons: [], isCanonicalized: false, isRetesselated: false,
    transforms: {elements: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]}
  }
  t.deepEqual(fromPoints(points), expected)
})
