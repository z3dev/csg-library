const test = require('ava')

const Matrix4x4 = require('../math/Matrix4x4')

const {transform, fromPoints} = require('./index')

test('transform: Adjusts the transforms of a populated geom3', (t) => {
  const points = [[[0, 0, 0], [1, 0, 0], [1, 0, 1]]]
  const rotate90 = Matrix4x4.rotationZ(90)

  const expected = {
    basePolygons: [
      {
        plane: {normal: {_x: 0, _y: -1, _z: 0}, w: 0},
        shared: {},
        vertices: [
          {pos: {_x: 0, _y: 0, _z: 0}},
          {pos: {_x: 1, _y: 0, _z: 0}},
          {pos: {_x: 1, _y: 0, _z: 1}},
         ]
      }
    ],
    polygons: [], isCanonicalized: false, isRetesselated: false,
    transforms: {elements: [6.123233995736766e-17, 1, 0, 0, -1, 6.123233995736766e-17, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]}
  }
  const geometry = fromPoints(points)
  const another = transform(rotate90, geometry)
  t.not(geometry, another)
  t.deepEqual(another, expected)
})
