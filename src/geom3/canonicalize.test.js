const test = require('ava')

const {canonicalize, fromPoints, toString} = require('./index')

test('canonicalize: Updates a geom3 with canonalized polygons', (t) => {
  const points = [[[0, 0, 0], [1, 0, 0], [1, 0, 1]]]
  const expected = {
    basePolygons: [
      {
        plane: {normal: {_x: 0, _y: -1, _z: 0}, w: 0},
        shared: {},
        vertices: [
          {pos: {_x: 0, _y: 0, _z: 0}},
          {pos: {_x: 1, _y: 0, _z: 0}},
          {pos: {_x: 1, _y: 0, _z: 1}},
         ]
      }
    ],
    polygons: [
      {
        plane: {normal: {_x: 0, _y: -1, _z: 0}, w: 0},
        shared: {},
        vertices: [
          {pos: {_x: 0, _y: 0, _z: 0}},
          {pos: {_x: 1, _y: 0, _z: 0}},
          {pos: {_x: 1, _y: 0, _z: 1}},
         ]
      }
    ],
    isCanonicalized: true, isRetesselated: false,
    transforms: {elements: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]}
  }
  const geometry = fromPoints(points)
  const updated = canonicalize(geometry)
  t.is(geometry, updated)
  t.deepEqual(updated, expected)
})
