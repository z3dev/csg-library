const test = require('ava')

const {clone, create, fromPoints} = require('./index')

test('clone: Creates a clone on an empty geom3', t => {
  const expected = {
    basePolygons: [], polygons: [], isCanonicalized: false, isRetesselated: false,
    transforms: {elements: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]}
  }
  const geometry = create()
  const another = clone(geometry)
  t.not(another, geometry)
  t.deepEqual(another, expected)
})

test('clone: Creates a clone of a populated geom3', t => {
  const points = [[[0, 0, 0], [1, 0, 0], [1, 0, 1]]]
  const expected = {
    basePolygons: [
      {
        plane: {normal: {_x: 0, _y: -1, _z: 0}, w: 0},
        shared: {},
        vertices: [
          {pos: {_x: 0, _y: 0, _z: 0}},
          {pos: {_x: 1, _y: 0, _z: 0}},
          {pos: {_x: 1, _y: 0, _z: 1}},
         ]
      }
    ],
    polygons: [], isCanonicalized: false, isRetesselated: false,
    transforms: {elements: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]}
  }
  const geometry = fromPoints(points)
  const another = clone(geometry)
  t.not(another, geometry)
  t.deepEqual(another, expected)
})
