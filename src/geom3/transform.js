const create = require('./create')

/**
 * Transform the given geometry using the given matrix.
 * This is a lazy transform of the polygons, as this function only adjusts the transforms.
 * See canonicalize() for the actual application of the transfroms to the polygons.
 * @param {Matrix4x4} matrix - the matrix to transform with
 * @param {geom3} geometry - the geometry to transform
 * @returns {geom3} - the transformed geometry
 * @example
 * let newgeometry = transform(fromXRotation(degToRad(90)), geometry)
 */
const transform = function (matrix, geometry) {
  let newgeometry = create(geometry.basePolygons) // reuse the polygons
  newgeometry.transforms = geometry.transforms.multiply(matrix)
  return newgeometry
}

module.exports = transform
