const Matrix4x4 = require('../math/Matrix4x4')

/**
 * Create a new 3D geometry composed of polygons.
 * @returns {geom3} - a new geometry
 */
const create = function (polygons) {
  if (polygons === undefined) {
    polygons = [] // empty contents
  }
  return {
    basePolygons : polygons,
    polygons : [],
    isCanonicalized : false,
    isRetesselated : false,
    transforms : new Matrix4x4()
  }
}

module.exports = create
