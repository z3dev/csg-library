// list of supported geometries
const geom2 = require('../geom2')
const geom3 = require('../geom3')
const path = require('../path')

const areAllShapesTheSameType = (shapes) => {
  let previousType
  shapes.forEach((shape) => {
    let currentType = 0
    if (geom2.isA(shape)) currentType = 1
    if (geom3.isA(shape)) currentType = 2
    if (path.isA(shape)) currentType = 3

    if (previousType && currentType !== previousType) return false
    previousType = currentType
  })
  return true
}

module.exports = areAllShapesTheSameType
