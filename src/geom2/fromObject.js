const Side = require('../math/Side')

const create = require('./create')

const fromObject = (obj) => {
  let baseSides = obj.baseSides.map(function (side) {
    return Side.fromObject(side)
  })
  let geometry = create(baseSides)

  if (obj.isCanonicalized === true) {
    geometry.toSides() // initiate canonicalization
  }
  return geometry
}

module.exports = fromObject
