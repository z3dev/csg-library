const test = require('ava')

const Matrix4x4 = require('../math/Matrix4x4')

const {transform, fromPoints} = require('./index')

test('transform: adjusts the transforms of geom2', (t) => {
  const points = [[0, 0], [1, 0], [0, 1]]
  const rotate90 = Matrix4x4.rotationZ(90)

  const expected = {baseSides: [
                      { vertex0: {pos: {_x: 0, _y: 1}}, vertex1: {pos: {_x: 0, _y: 0}} },
                      { vertex0: {pos: {_x: 0, _y: 0}}, vertex1: {pos: {_x: 1, _y: 0}} },
                      { vertex0: {pos: {_x: 1, _y: 0}}, vertex1: {pos: {_x: 0, _y: 1}} }
                    ],
                    sides: [], isCanonicalized: false,
                    transforms: {elements: [6.123233995736766e-17, 1, 0, 0, -1, 6.123233995736766e-17, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]} }
  const geometry = fromPoints(points)
  const another = transform(rotate90, geometry)
  t.not(geometry, another)
  t.deepEqual(another, expected)
})
