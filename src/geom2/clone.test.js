const test = require('ava')

const {clone, create, fromPoints} = require('./index')

test('clone: Creates a clone on an empty geom2', t => {
  const expected = {baseSides: [], sides: [], isCanonicalized: false, transforms: {elements: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]} }
  const geometry = create()
  const another = clone(geometry)
  t.not(another, geometry)
  t.deepEqual(another, expected)
})

test('clone: Creates a clone of a complete geom2', t => {
  const points = [[0, 0], [1, 0], [0, 1]]
  const expected = {baseSides: [
                      { vertex0: {pos: {_x: 0, _y: 1}}, vertex1: {pos: {_x: 0, _y: 0}} },
                      { vertex0: {pos: {_x: 0, _y: 0}}, vertex1: {pos: {_x: 1, _y: 0}} },
                      { vertex0: {pos: {_x: 1, _y: 0}}, vertex1: {pos: {_x: 0, _y: 1}} }
                    ],
                    sides: [], isCanonicalized: false, transforms: {elements: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]} }
  const geometry = fromPoints(points)
  const another = clone(geometry)
  t.not(another, geometry)
  t.deepEqual(another, expected)
})
