/**
 * Produces a canonicalized geometry by canonicalizing the sides.
 * Must be called before exposing any side data.
 * @param {geom2} geometry - the geometry to canonicalize
 * @returns {geom2} the given geometry with transformed points
 * @example
 * let newgeometry = canonicalize(geometry)
 */
const canonicalize = (geometry) => {
  if (geometry.isCanonicalized) {
    return geometry
  }
  // apply transforms to each side, and canonicalize
  geometry.sides = geometry.baseSides.map((side) => {
    return side.transform(geometry.transforms).canonicalize()
  })
  geometry.isCanonicalized = true
  return geometry
}

module.exports = canonicalize
