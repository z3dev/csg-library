/**
 * Create a string representing the contents of the given geometry.
 * @returns {String} a representive string
 * @example
 * console.out(toString(geometry))
 */
const toString = function (geometry) {
  if (geometry.isCanonicalized) {
    let result = 'geom2 (' + geometry.sides.length + ' sides):\n'
    geometry.sides.forEach(function (side) {
      result += '  ' + side.toString() + '\n'
    })
    return result
  }

  let result = 'geom2 (' + geometry.baseSides.length + ' baseSides):\n'
  geometry.baseSides.forEach(function (side) {
    result += '  ' + side.toString() + '\n'
  })
  return result
}

module.exports = toString
