const {linesIntersect} = require('../math/lineUtils')

const isSelfIntersecting = function (geometry) {
  let numsides = geometry.sides.length
  for (let i = 0; i < numsides; i++) {
    let side0 = geometry.sides[i]
    for (let ii = i + 1; ii < numsides; ii++) {
      let side1 = geometry.sides[ii]
      if (linesIntersect(side0.vertex0.pos, side0.vertex1.pos, side1.vertex0.pos, side1.vertex1.pos)) {
        return true
      }
    }
  }
  return false
}

module.exports = isSelfIntersecting
