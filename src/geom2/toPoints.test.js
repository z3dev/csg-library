const test = require('ava')

const {toPoints, fromPoints, toString} = require('./index')

test('toPoints: Creates an array of points from a populated geom2', (t) => {
  const points = [[0, 0], [1, 0], [0, 1]]
  const expected = {baseSides: [
                      { vertex0: {pos: {_x: 0, _y: 1}}, vertex1: {pos: {_x: 0, _y: 0}} },
                      { vertex0: {pos: {_x: 0, _y: 0}}, vertex1: {pos: {_x: 1, _y: 0}} },
                      { vertex0: {pos: {_x: 1, _y: 0}}, vertex1: {pos: {_x: 0, _y: 1}} }
                    ],
                    sides: [], isCanonicalized: false, transforms: {elements: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]} }
  const geometry = fromPoints(points)
  t.deepEqual(geometry, expected)

  const asstring = toString(geometry)
  // console.log(asstring)

  const pointarray = toPoints(geometry)
  t.deepEqual(pointarray, points)

// TODO : test with mirrored geometry, see transform.js
})
