const {areaEPS} = require('../constants')

const Side = require('../math/Side')

const create = require('./create')
const isSelfIntersecting = require('./isSelfIntersecting')

/**
 * Create a new 2D geometry from the given points.
 * The direction (rotation) of the points is not relevant,
 * as the points can define a convex or a concave polygon.
 * The geometry must not self intersect, i.e. the sides cannot cross.
 * @param {Array} points - list of points in 2D space where each point is an array of two values
 * @returns {geom2} a new geometry
 */
const fromPoints = function (points) {
  if (!Array.isArray(points)) {
    throw new Error('the given points must be an array')
  }
  if (points.length < 3) {
    throw new Error('the given points must define a closed polygon with three or more points')
  }

  let sides = []
  let prevpoint = points[points.length - 1]
  points.forEach(function (point) {
    sides.push(Side.fromPoints(prevpoint, point))
    prevpoint = point
  })
  let result = create(sides)

  // FIXME enable via options?
  // if (isSelfIntersecting(result)) {
  //   throw new Error('the provided points define a self-intersecting 2D geometry')
  // }

  // FIXME enable via options?
  // let area = result.area()
  // if (Math.abs(area) < areaEPS) {
  //   throw new Error('the provided points define a degenerate 2D geometry')
  // }
  // if (area < 0) {
  //   result = result.flipped()
  // }

  return result
}

module.exports = fromPoints
