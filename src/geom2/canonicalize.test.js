const test = require('ava')

const {canonicalize, fromPoints, toString} = require('./index')

test('canonicalize: Updates a populated geom2 with canonalized sides', (t) => {
  const points = [[0, 0], [1, 0], [0, 1]]
  const expected = {baseSides: [
                      { vertex0: {pos: {_x: 0, _y: 1}}, vertex1: {pos: {_x: 0, _y: 0}} },
                      { vertex0: {pos: {_x: 0, _y: 0}}, vertex1: {pos: {_x: 1, _y: 0}} },
                      { vertex0: {pos: {_x: 1, _y: 0}}, vertex1: {pos: {_x: 0, _y: 1}} }
                    ],
                    sides: [
                      { vertex0: {pos: {_x: 0, _y: 1}}, vertex1: {pos: {_x: 0, _y: 0}} },
                      { vertex0: {pos: {_x: 0, _y: 0}}, vertex1: {pos: {_x: 1, _y: 0}} },
                      { vertex0: {pos: {_x: 1, _y: 0}}, vertex1: {pos: {_x: 0, _y: 1}} }
                    ],
                    isCanonicalized: true, transforms: {elements: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]} }
  const geometry = fromPoints(points)
  const updated = canonicalize(geometry)
  t.is(geometry, updated)
  t.deepEqual(updated, expected)

// TODO : test with mirrored geometry, see transform.js
})
