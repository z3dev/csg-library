module.exports = {
  canonicalize: require('./canonicalize'),
  clone: require('./clone'),
  create: require('./create'),
  //equals: require('./equals'),
  fromObject: require('./fromObject'),
  fromPoints: require('./fromPoints'),
  isA: require('./isA'),
  isSelfIntersecting: require('./isSelfIntersecting'),
  reverse: require('./reverse'),
  toPoints: require('./toPoints'),
  toSides: require('./toSides'),
  toString: require('./toString'),
  transform: require('./transform')
}
