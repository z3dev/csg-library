const test = require('ava')

const {fromPoints} = require('./index')

test('fromPoints: Creates populated geom2', (t) => {
  const points = [[0, 0], [1, 0], [0, 1]]
  const expected = {baseSides: [
                      { vertex0: {pos: {_x: 0, _y: 1}}, vertex1: {pos: {_x: 0, _y: 0}} },
                      { vertex0: {pos: {_x: 0, _y: 0}}, vertex1: {pos: {_x: 1, _y: 0}} },
                      { vertex0: {pos: {_x: 1, _y: 0}}, vertex1: {pos: {_x: 0, _y: 1}} }
                    ],
                    sides: [], isCanonicalized: false, transforms: {elements: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]} }
  t.deepEqual(fromPoints(points), expected)
})
