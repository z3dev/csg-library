const concat = require('../path/concat')
const fromPoints = require('../path/fromPoints')

/**
 * Appends a list of points to an open path.
 * @param {Object} options - options used when appending
 * @param {Array} points - the list of points to append to the path
 * @param {path} path - the path of which to appended to
 * @returns {path} a new path
 * @example
 * let newpath = appendPoints({}, [[1, 1, 0], [1, 0, 0]], path.fromPoints([[0, 0, 0]]))
 */
const appendPoints = (options, points, path) => {
  const newpath = fromPoints(options, points)
  return concat(path, newpath)
}

module.exports = appendPoints
