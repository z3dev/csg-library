const expand = require('./expand')

const geom2extrude = require('../ops-geom2/extrude')

/*
 * Extrude the path by expanding to a 2D geometry, and then extruding to 3D geometry.
 * See the above routines for required options.
 */
const extrude = (options, path) => {
  const geometry2 = expand(options, path)
  return geom2extrude(options, geometry2)
}

module.exports = extrude
