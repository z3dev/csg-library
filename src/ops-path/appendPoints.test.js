const test = require('ava')

const equals = require('../path/equals')
const fromPoints = require('../path/fromPoints')

const appendPoints = require('./appendPoints')

test('appendPoints: An empty path with a point appended is the same as a path created from that point', t => {
  const empty = fromPoints({}, [])
  const origin = fromPoints({}, [[0, 0, 0]])
  t.true(equals(appendPoints({}, [[0, 0, 0]], empty), origin))
})

test('appendPoints: Appending to a closed path fails', t => {
  t.throws(() => appendPoints({}, [[0, 0, 0]], fromPoints({closed: true}, [])),
           'Cannot concatenate to a closed path')
})
