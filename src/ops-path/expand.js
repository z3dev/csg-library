const {defaultResolution2D} = require('../constants')

const Side = require('../math/Side')
const Vertex2 = require('../math/Vertex2')
const Vector2 = require('../math/Vector2')

const path = require('../path')

const geom2 = require('../geom2')
const expandShell = require('../ops-geom2/expandShell')

/**
 * Convert the given geometry (path) to 2D geometry, then expand the geometry.
 * @param {Number} options.radius - radius of expansion
 * @param {Number} options.resolution - number of sides per 360 rotation
 * @returns {geom2} new 2D geometry (expanded)
 */
const expand = function (options, geometry) {
  const defaults = {
    radius: 1,
    resolution: defaultResolution2D
  }
  let {radius, resolution} = Object.assign({}, defaults, options)

  const oldpoints = path.toPoints(geometry) // Vector3[]
  let numpoints = oldpoints.length
  let startindex = 0
  if (geometry.isClosed && (numpoints > 2)) startindex = -1

  let sides = []
  let prevvertex
  for (let i = startindex; i < numpoints; i++) {
    let pointindex = i
    if (pointindex < 0) pointindex = numpoints - 1
    let point = oldpoints[pointindex] // Vector3
    let vertex = new Vertex2(new Vector2(point.x, point.y))
    if (i > startindex) {
      sides.push(new Side(prevvertex, vertex))
    }
    prevvertex = vertex
  }
  let newgeometry = geom2.create(sides)

  let expanded = expandShell({radius: radius, resolution: resolution}, newgeometry)
  return expanded
}

module.exports = expand
