const Vector3 = require('../math/Vector3')

const path = require('../path')

/*
 * Calculate the min and max bounds of the given geometry.
 * @return {[[x,y,z], [x,y,z]]} the bounds (min and max points) for the geometry
 */
const calculateBounds = function (geometry) {
  const points = path.toPoints(geometry)

  let minpoint
  if (points.length === 0) {
    minpoint = new Vector3(0, 0, 0)
  } else {
    minpoint = points[0]
  }
  let maxpoint = minpoint
  points.forEach(function (point) {
    minpoint = minpoint.min(point)
    minpoint = minpoint.min(point)
    maxpoint = maxpoint.max(point)
    maxpoint = maxpoint.max(point)
  })
  return [[minpoint.x, minpoint.y, minpoint.z], [maxpoint.x, maxpoint.y, maxpoint.z]]
}

module.exports = calculateBounds
