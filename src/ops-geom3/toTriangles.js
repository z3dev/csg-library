
const geom3 = require('../geom3')

/**
 * Convert the given geometry into triangles (polygons)
 * @returns {[Polygons]} the triangulated polygons
 */
const toTriangles = (geometry) => {
  let polygons = geom3.toPolygons(geometry)

  let triangles = []
  polygons.forEach(function (poly) {
    let firstVertex = poly.vertices[0]
    for (let i = poly.vertices.length - 3; i >= 0; i--) {
      triangles.push(new Polygon(
        [firstVertex, poly.vertices[i + 1], poly.vertices[i + 2]],
        poly.shared,
        poly.plane))
    }
  })
  return triangles
}

module.exports = toTriangles
