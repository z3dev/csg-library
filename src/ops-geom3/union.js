const flatten = require('../utils/flatten')

const geom3 = require('../geom3')

const retesselate = require('./retesselate')
const unionSub = require('./unionSub')

/**
 * Return a new geometry representing the space in the given solids.
 * @param {...objects} geometries - list of geometries to union
 * @returns {geom3} new geometry
 */
const union = (...geometries) => {
  geometries = flatten(geometries)

  // combine geometries in a way that forms a balanced binary tree pattern
  let i
  for (i = 1; i < geometries.length; i += 2) {
    geometries.push(unionSub(geometries[i - 1], geometries[i]))
  }
  let newgeomerty = retesselate(geometries[i - 1])
  return newgeomerty
}

module.exports = union
