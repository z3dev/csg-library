
const reTesselateCoplanarPolygons = require('../core/utils/reTesselateCoplanarPolygons')

const FuzzyFactory3d = require('../core/FuzzyFactory3d')

const geom3 = require('../geom3')

/*
 * Algorithm:
 * - group polygons by plane and color (shared)
 * - retesslelate the grouped polygons, combining small polygons into large polygons
 * - return a new geometry from the resulting polygons
 */
const retesselate = function (geometry) {
  // skip if already performed
  if (geometry.isRetesselated) {
    return geometry
  }

  let polygons = geom3.toPolygons(geometry)

  let fuzzyfactory = new FuzzyFactory3d()
  let polygonsPerPlane = {}
  polygons.forEach(function (polygon) {
    // in order to identify polygons having the same plane, we need to canonicalize the planes
    // to save time only do the planes and the shared data
    let plane = fuzzyfactory.getPlane(polygon.plane)
    let shared = fuzzyfactory.getPolygonShared(polygon.shared)
    let tag = plane.getTag() + '/' + shared.getTag()
    if (!(tag in polygonsPerPlane)) {
      polygonsPerPlane[tag] = [polygon]
    } else {
      polygonsPerPlane[tag].push(polygon)
    }
  })
  let destpolygons = []
  for (let planetag in polygonsPerPlane) {
    let sourcepolygons = polygonsPerPlane[planetag]
    if (sourcepolygons.length < 2) {
      Array.prototype.push.apply(destpolygons, sourcepolygons)
    } else {
      let retesselayedpolygons = []
      reTesselateCoplanarPolygons(sourcepolygons, retesselayedpolygons)
      Array.prototype.push.apply(destpolygons, retesselayedpolygons)
    }
  }
  let result = geom3.create(destpolygons)
  result.isRetesselated = true
  return result
}

module.exports = retesselate
