const Tree = require('../core/trees')

const geom3 = require('../geom3')

const mayOverlap = require('./mayOverlap')

/**
 * Return a new geometry representing the space in the given solids.
 * @param {...objects} csg - list of geometry
 * @returns {goem3} new geometry
 */
const unionSub = (geometry1, geometry2) => {
  if (!mayOverlap(geometry1, geometry2)) {
    return unionForNonIntersecting(geometry1, geometry2)
  }

  let a = new Tree(geom3.toPolygons(geometry1))
  let b = new Tree(geom3.toPolygons(geometry2))
  a.clipTo(b, false)
  // b.clipTo(a, true); // ERROR: doesn't work
  b.clipTo(a)
  b.invert()
  b.clipTo(a)
  b.invert()

  let newpolygons = a.allPolygons().concat(b.allPolygons())
  return geom3.create(newpolygons)
}

// Like union, but when we know that the two solids are not intersecting
// Do not use if you are not completely sure that the solids do not intersect!
const unionForNonIntersecting = (geometry1, geometry2) => {
  let newpolygons = geom3.toPolygons(geometry1)
  newpolygons = newpolygons.concat(geom3.toPolygons(geometry2))
  return geom3.create(newpolygons)
}

module.exports = unionSub
