const expandShell = require('./expandShell')
const subtractSub = require('./subtractSub')

/*
 * NOTE: There are no defaults. All options are required.
 */
const contract = (options, geometry) => {
  let expanded = expandShell(options, geometry)
  let result = subtractSub(geometry, expanded)
  return result
}

module.exports = contract
