const Tree = require('../core/trees')

const geom3 = require('../geom3')

const subtractSub = (geometry1, geometry2) => {
  let a = new Tree(geom3.toPolygons(geometry1))
  let b = new Tree(geom3.toPolygons(geometry2))
  a.invert()
  a.clipTo(b)
  b.clipTo(a, true)
  a.addPolygons(b.allPolygons())
  a.invert()

  let newpolygons = a.allPolygons()
  return geom3.create(newpolygons)
}

module.exports = subtractSub
