const expandShell = require('./expandShell')
const union = require('./union')

/*
 * NOTE: There are no defaults. All options are required.
 */
const expand = (options, geometry) => {
  let expanded = expandShell(options, geometry)
  let result = union(geometry, expanded)
  return result
}

module.exports = expand
