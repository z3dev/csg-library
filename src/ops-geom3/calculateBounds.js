const Vector3D = require('../math/Vector3')

const geom3 = require('../geom3')

/**
 * Calculate the min and max bounds of the given geometry.
 * @return {[[x,y,z], [x,y,z]]} the bounds (min and max points) for the geometry
 * @example
 * let bounds = calculateBounds(geometry)
 * let minX = bounds[0][0]
 */
const calculateBounds = (geometry) => {
  const polygons = geom3.toPolygons(geometry)

  let minpoint = new Vector3D(0, 0, 0)
  let maxpoint = minpoint

  polygons.forEach((polygon, i) => {
    let bounds = polygon.boundingBox()
    if (i === 0) {
      minpoint = bounds[0]
      maxpoint = bounds[1]
    } else {
      minpoint = minpoint.min(bounds[0])
      maxpoint = maxpoint.max(bounds[1])
    }
  })
  let min = [minpoint.x, minpoint.y, minpoint.z]
  let max = [maxpoint.x, maxpoint.y, maxpoint.z]
  return [min, max]
}

module.exports = calculateBounds
