
const geom3 = require('../geom3')

/**
 * Invert the given geometry, swapping solid and empty space.
 * @returns {geom3} new geometry
 * @example
 * let B = A.invert()
 */
const invert = (geometry) => {
  let polygons = geom3.toPolygons(geometry)

  let flippedpolygons = polygons.map(function (polygon) {
    return polygon.flipped()
  })
  // TODO: flip properties?

  return geom3.create(flippedpolygons)
}

module.exports = invert
