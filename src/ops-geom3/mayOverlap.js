const calculateBounds = require('./calculateBounds')

/**
 * Determine if the given geometries may overlap.
 * NOTE: This is critical as it is used in union for performace gains.
 * returns false if we can be sure that they do not overlap
 */
const mayOverlap = function (geometry1, geometry2) {
  if ((geometry1.basePolygons.length === 0) || (geometry2.basePolygons.length === 0)) {
    return false
  }

  let bounds1 = calculateBounds(geometry1)
  let bounds2 = calculateBounds(geometry2)
  if (bounds1[1][0] < bounds2[0][0]) return false
  if (bounds1[0][0] > bounds2[1][0]) return false
  if (bounds1[1][1] < bounds2[0][1]) return false
  if (bounds1[0][1] > bounds2[1][1]) return false
  if (bounds1[1][2] < bounds2[0][2]) return false
  if (bounds1[0][2] > bounds2[1][2]) return false
  return true
}

module.exports = mayOverlap
