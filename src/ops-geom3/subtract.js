const flatten = require('../utils/flatten')

const geom3 = require('../geom3')

const retesselate = require('./retesselate')
const subtractSub = require('./subtractSub')

/**
 * Return a new geometry representing space in this geometry but not in the given geometries.
 * Neither this geometry nor the given geometries are modified.
 * @param {...geom3} geometries - list of geometries
 * @returns {geom3} new geometry
 */
const subtract = (...geometries) => {
  geometries = flatten(geometries)

  let newgeometry = geometries.shift()
  geometries.forEach((geometry) => {
    newgeometry = subtractSub(newgeometry, geometry)
  })

  let newgeomerty = retesselate(newgeometry)
  return newgeometry
}

module.exports = subtract
